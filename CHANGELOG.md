# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.4.3] - UNRELEASED
### Added
- Allow users to delete their account or claim their data

## [0.4.2] - 2018-05-01
### Added (1 change)
- Add a Changelog
- Projects can be open, closed, and forked from.

### Security
- Tasks can only be deleted by their supervisors or the projects's managers
- Changed md5 to sha-256 for creating hashes
- Protect API resources
