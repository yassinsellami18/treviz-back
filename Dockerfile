FROM php:7.2-fpm

WORKDIR /var/www/api

ENV PARAMETERS_FILE ./app/config/parameters.yml

ARG MYSQL_HOST=db
ARG MYSQL_PORT=3306
ARG MYSQL_DATABASE
ARG MYSQL_PASSWORD
ARG SMTP_HOST
ARG SMTP_PORT
ARG SMTP_USERNAME
ARG SMTP_PASSWORD
ARG FRONTEND_URL
ARG BACKEND_URL
ARG WEBSOCKET_HOST
ARG WEBSOCKET_PORT
ARG RSA_PASSPHRASE

# Installing php extensions
RUN apt-get update && \
    apt-get install -y \
        zlib1g-dev \
        pwgen \
        openssl \
    && docker-php-ext-install zip pdo pdo_mysql

# Installing Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"

COPY . ./
RUN php composer.phar install

# Configuration file
## Database configuration
RUN sed -ri -e 's!db_host!'$MYSQL_HOST'!g' $PARAMETERS_FILE
RUN sed -ri -e 's!db_port!'$MYSQL_PORT'!g' $PARAMETERS_FILE
RUN sed -ri -e 's!db_name!'$MYSQL_DATABASE'!g' $PARAMETERS_FILE
RUN sed -ri -e 's!db_username!'$MYSQL_USER'!g' $PARAMETERS_FILE
RUN sed -ri -e 's!db_pwd!'$MYSQL_PASSWORD'!g' $PARAMETERS_FILE

## Secret
RUN sed -ri -e 's!changeit!'$(pwgen 48 1 -By)'!g' $PARAMETERS_FILE

## RSA keys for JWT
RUN mkdir -p var/jwt
RUN openssl genrsa -passout pass:$RSA_PASSPHRASE -out var/jwt/private.pem -aes256 4096
RUN openssl rsa -passin pass:$RSA_PASSPHRASE -pubout -in var/jwt/private.pem -out var/jwt/public.pem
RUN sed -ri -e 's!passphrase!'$RSA_PASSPHRASE'!g' $PARAMETERS_FILE

## SMTP Parameters
RUN sed -ri -e 's!smtp_host!'$SMTP_HOST'!g' $PARAMETERS_FILE
RUN sed -ri -e 's!smtp_port!'$SMTP_PORT'!g' $PARAMETERS_FILE
RUN sed -ri -e 's!smtp_user!'$SMTP_USERNAME'!g' $PARAMETERS_FILE
RUN sed -ri -e 's!smtp_password!'$SMTP_PASSWORD'!g' $PARAMETERS_FILE

## Websocket and Frontend
RUN sed -ri -e 's!app.treviz.xyz!'$FRONTEND_URL'!g' $PARAMETERS_FILE
RUN sed -ri -e 's!api.treviz.xyz!'$BACKEND_URL'!g' $PARAMETERS_FILE
RUN sed -ri -e 's!websocket.treviz.xyz!'$WEBSOCKET_HOST'!g' $PARAMETERS_FILE
RUN sed -ri -e 's!8080!'$WEBSOCKET_PORT'!g' $PARAMETERS_FILE

## Create upload folders
RUN mkdir -p web/upload/projects/logos \
         web/upload/communities/logos \
         web/upload/communities/backgrounds \
         web/upload/users/avatars \
         web/upload/users/backgrounds

RUN chmod 777 -R var/ web/upload/