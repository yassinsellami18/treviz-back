server {
    listen 80;
    listen [::]:80;

    # Specify the root directory of the project; in our case, the web folder of treviz-back
    root /var/www/api/web;

    # Index information
    location / {
        try_files $uri /app.php$is_args$args;
    }

    # Deny all access to unnecessary files and folders.
    location ~* /(\.git|cache|bin|logs|backup|tests)/.*$ { return 403; }
    location ~* /(LICENSE\.txt|README\.md|CONTRIBUTING\.md|composer\.lock|composer\.json|package\.json|\.env|\.gitignore|symfony\.lock) { return 403; }

    # PHP Configuration
    location ~ \.php(/|$) {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass api:9000;
        fastcgi_index app.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}