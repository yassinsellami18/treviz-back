imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }
    - { resource: "@TrevizNotificationBundle/Resources/config/services.yml" }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: en

framework:
    #esi:             ~
    translator:      { fallbacks: ["%locale%"] }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection:
        enabled: true
    validation:      { enable_annotations: true }
    serializer:
        enabled: false
    templating:
        engines: ['twig']
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # http://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id:  session.handler.native_file
        save_path:   "%kernel.root_dir%/../var/sessions/%kernel.environment%"
    fragments:       ~
    http_method_override: true
    assets: ~
    php_errors:
        log: true

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_mysql
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8


    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    port:      "%mailer_port%"
    encryption: "%mailer_encryption%"
    auth_mode:  login
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }
    logging: true

fos_user:
    db_driver: orm
    firewall_name: main
    user_class: Treviz\CoreBundle\Entity\User
    from_email:
        address: bastien@treviz.xyz
        sender_name: Treviz

fos_rest:
    routing_loader:
        default_format: json
    param_fetcher_listener: true
    body_listener: true
    format_listener: true
    view:
        view_response_listener: 'force'
        formats:
            xml: true
            json : true
    routing_loader:
        default_format: json

jms_serializer:
    handlers:
        datetime:
            default_format: "Y-m-d\\TH:i:sP"
            default_timezone: "UTC"
    metadata:
        directories:
            FOSUB:
                namespace_prefix: "FOS\\UserBundle"
                path: "%kernel.root_dir%/Resources/FOSUserBundle/serializer"
    property_naming:
      enable_cache: true

#Default configuration for Sensio Framework Extra Bundle. Enable @View Annotations.
sensio_framework_extra:
    view:        { annotations: true }

lexik_jwt_authentication:
    private_key_path: '%jwt_private_key_path%'
    public_key_path:  '%jwt_public_key_path%'
    pass_phrase:      '%jwt_key_pass_phrase%'
    token_ttl:        '%jwt_token_ttl%'
    token_extractors:
        authorization_header:      # look for a token as Authorization Header
            enabled: true
            prefix:  Bearer
            name:    Authorization
        cookie:                    # check token in a cookie
            enabled: false
            name:    BEARER
        query_parameter:           # check token in query string parameter
            enabled: true
            name:    bearer

sonata_block:
    default_contexts: [cms]
    blocks:
        # enable the SonataAdminBundle block
        sonata.admin.block.admin_list:
            contexts: [admin]
        # ...

nelmio_cors:
    defaults:
        allow_credentials: false
        allow_origin: []
        allow_headers: []
        allow_methods: []
        expose_headers: []
        max_age: 0
        hosts: []
        origin_regex: false
        forced_allow_origin_value: ~
    paths:
        '^/v1/':
            allow_origin: ['*']
            allow_headers: ['*']
            allow_methods: ['OPTIONS', 'GET', 'PUT', 'POST', 'DELETE']
            max_age: 3600

nelmio_api_doc:
    routes:
        path_patterns: # an array of regexps
            - ^/v1
    documentation:
        info:
            title: Treviz API
            description: First version of the Treviz API
            version: 1.0.0