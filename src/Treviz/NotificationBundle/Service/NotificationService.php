<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 03/08/2017
 * Time: 22:50
 */

namespace Treviz\NotificationBundle\Service;

use Doctrine\ORM\EntityManager;
use Treviz\NotificationBundle\Sockets\NotificationSocket;
use Symfony\Component\Console\Output\OutputInterface;
use Ratchet\App;

class NotificationService
{

    private $em;

    private $websocketUrl;
    private $websocketPort;

    /** @var  NotificationSocket */
    private $notifications;

    public function __construct(EntityManager $entityManager, $websocketUrl, $websocketPort)
    {
        $this->em = $entityManager;
        $this->websocketUrl = $websocketUrl;
        $this->websocketPort = $websocketPort;
    }

    public function startChat(OutputInterface $output)
    {

        $this->notifications = new NotificationSocket($this->em);

        $output->writeln([
            'Notifications socket',
        ]);

        $app =  new App($this->websocketUrl, $this->websocketPort, '0.0.0.0');
        $app->route('', $this->notifications, array('*'));

        $output->writeln([
            '=======',
            'Bootstrapping Websocket server at url ws://' . $this->websocketUrl . ':' . $this->websocketPort
        ]);

        $app->run();

    }


}