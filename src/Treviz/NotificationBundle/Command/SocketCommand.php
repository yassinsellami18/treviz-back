<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 29/07/2017
 * Time: 18:07
 */

namespace Treviz\NotificationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SocketCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('sockets:start-notification')
            // the short description shown while running "php bin/console list"
            ->setHelp("Starts the notifications socket")
            // the full command description shown when running the command with
            ->setDescription('Starts the notifications socket')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $notificationService = $this->getContainer()->get("treviz_notification.service.websocket");
        $notificationService->startChat($output);

    }

}