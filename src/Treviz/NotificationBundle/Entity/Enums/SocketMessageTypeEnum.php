<?php

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 18/03/2017
 * Time: 14:28
 */

namespace Treviz\NotificationBundle\Entity\Enums;

abstract class SocketMessageTypeEnum
{

    CONST CHAT_POST_MESSAGE = "CHAT_POST_MESSAGE";
    CONST CHAT_UPDATE_MESSAGE = "CHAT_UPDATE_MESSAGE";
    CONST CHAT_DELETE_MESSAGE = "CHAT_DELETE_MESSAGE";
    CONST CHAT_INVITE_USER = "CHAT_INVITE_USER";
    CONST CHAT_POST_ROOM = "CHAT_POST_ROOM";
    CONST PROJECT_INVITE = "PROJECT_INVITE";
    CONST POST_POST_COMMENT = "POST_POST_COMMENT";

    /** @var array  */
    protected static $typeName = [
        self::CHAT_POST_MESSAGE => "Message Posted",
        self::CHAT_UPDATE_MESSAGE => "Message update",
        self::CHAT_DELETE_MESSAGE => "Message deleted",
        self::CHAT_POST_ROOM => "Room created",
        self::CHAT_INVITE_USER => "Invite user to chat",
        self::PROJECT_INVITE => "Invitation sent",
        self::POST_POST_COMMENT => "Comment posted",

    ];

    /**
     * @param $roleShortName
     * @return mixed|string
     * @internal param $visibilityShortName
     * @internal param $roleShortName
     */
    public static function getTypeName($roleShortName){
        if(!isset(static::$typeName[$roleShortName])){
            return "Unknown role ($roleShortName)";
        }

        return static::$typeName[$roleShortName];
    }

    public static function getAvailableTypes(){
        return [
            self::CHAT_POST_MESSAGE,
            self::CHAT_UPDATE_MESSAGE,
            self::CHAT_DELETE_MESSAGE,
            self::CHAT_INVITE_USER,
            self::CHAT_POST_ROOM,
            self::PROJECT_INVITE,
            self::POST_POST_COMMENT,
        ];
    }

}