<?php

namespace Treviz\NotificationBundle\Sockets;

use Doctrine\ORM\EntityManager;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Treviz\NotificationBundle\Entity\Notification;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 29/07/2017
 * Time: 16:57
 */
class NotificationSocket implements MessageComponentInterface
{
    private $em;

    /**
     *  Associative array: takes username as key, connection id as value.
     * [ "jdoe" => "0ger6g0herg", "jsmith" => "zeg06056ezgz" ]
     * @var array  */
    private $clients;

    public function __construct(EntityManager $em) {
        //$this->clients = new \SplObjectStorage;
        $this->em = $em;
        $this->clients = [];
    }

    public function onOpen(ConnectionInterface $conn) {

        echo("New connection opened \n");

        // Retrieve the ticket sent in the handshake request query param.
        $ticket = $conn->WebSocket->request->getQuery()->get('ticket');

        echo("Fetching user with ticket " . $ticket . " \n");

        // Retrieve the user who owns this ticket
        $user = $this->getUserFromTicket($ticket);

        // Add the user in the client registry.
        $this->clients[$user->getUsername()] = $conn;

        echo("Connection established \n");

    }

    public function onMessage(ConnectionInterface $from, $msg) {

        echo "Message received from connection " . $from->resourceId . "\n";

        //Decoding $msg as array, to retrieve chatroom
        $msg = json_decode($msg, true);

        /*
         * Send the message to its recipients.
         * TODO: Handle each type of message differently, and checks the access rights.
         */
        $userNames = $msg['recipients'];
        foreach ($userNames as $recipient) {
            if (isset($this->clients[$recipient])) {
                echo "Sending message to " . $recipient . "\n";
                $this->clients[$recipient]->send(json_encode($msg));
            } else {
                echo $recipient . ' is not connected, saving notification for later \n';

                $notification = new Notification();
                $notification->setType($msg['type']);
                $notification->setContent(json_encode($msg['content']));
                $notification->setRecipient($recipient);
                $this->em->persist($notification);
                $this->em->flush();
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {

        echo("Removing connection " . $conn->resourceId . " from clients \n");

        $ticket = $conn->WebSocket->request->getQuery()->get('ticket');
        $user= $this->getUserFromTicket($ticket);
        unset($this->clients[$user->getUsername()]);

    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred : {$e->getMessage()}\n";

        $conn->close();
    }


    // Helper functions

    /**
     * This function returns the User who owns the ticket
     *
     * @param $ticket
     * @return \Treviz\CoreBundle\Entity\User|null|object
     */
    private function getUserFromTicket($ticket) {

        if($ticket == null) {
            throw new UnexpectedValueException("Request ticket cannot be null");
        }

        $user = $this->em->getRepository("TrevizCoreBundle:User")->findOneBy(array('webSocketTicket' => $ticket));

        if($user == null) {
            throw new UnexpectedValueException("No user found for this ticket");
        }

        return $user;
    }

}