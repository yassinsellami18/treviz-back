<?php

namespace Treviz\NotificationBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Treviz\CoreBundle\Entity\User;
use Swagger\Annotations as SWG;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 05/08/2017
 * Time: 16:58
 */
class NotificationController extends FOSRestController
{

    /**
     * Returns the notifications of a user
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when the notifications could successfully be fetched"
     * )
     *
     * @SWG\Tag(name="notifications")
     *
     * @Rest\Get("/notifications")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     */
    public function getNotificationsAction(){
        $ticket = md5(random_bytes(10));
        /** @var User $user */
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $user->setWebSocketTicket($ticket);

        $notifications = $em->getRepository('TrevizNotificationBundle:Notification')
            ->findBy(array('recipient' => $user->getUsername()));

        $data = array(
            "notifications" => $notifications,
            "ticket" => $ticket
        );

        if ($notifications) {
            foreach ($notifications as $notification) {
                $em->remove($notification);
            }
        }

        $em->flush();

        $view = $this->view($data, 200);

        return $this->handleView($view);
    }
}