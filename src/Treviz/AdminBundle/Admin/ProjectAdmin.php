<?php

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 12/02/2017
 * Time: 16:47
 */

namespace Treviz\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class ProjectAdmin
 * @package Treviz\AdminBundle\Admin
 *
 * Displays, filter and administrates the various projects of the organization.
 */
class ProjectAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General Information', array('class' => 'col-md-9'))
                ->add('name', 'text')
                ->add('description', 'text')
                ->add('shortDescription', 'text')
            ->end()

            ->with('Tags', array('class' => 'col-md-3'))
                ->add('tags', 'sonata_type_model', array('required' => false, 'expanded' => true, 'multiple' => true, 'property' => 'name'))
                ->add('skills', 'sonata_type_model', array('required' => false, 'expanded' => true, 'multiple' => true, 'property' => 'name'))
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('hash');
        $listMapper->add('name');
        $listMapper->add('description');
    }

}