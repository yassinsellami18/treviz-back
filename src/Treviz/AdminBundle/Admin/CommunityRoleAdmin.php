<?php

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 12/02/2017
 * Time: 16:47
 */

namespace Treviz\AdminBundle\Admin;

use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class CommunityRoleAdmin
 * @package Treviz\AdminBundle\Admin
 *
 * Displays, filter and administrates the available roles for all the communities.
 */
class CommunityRoleAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text')
            ->add('permissions', 'choice', array(
                'multiple' => true,
                'choices' => CommunityPermissions::getAvailablePermissions(),
                'choice_label' => function($choice) {
                   return CommunityPermissions::getPermissionName($choice);
                }))
            ->add('global')
            ->add('defaultMember')
            ->add('defaultCreator');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('hash')
            ->add('name')
            ->add('global')
            ->add('defaultCreator')
            ->add('defaultMember')
            ->add('community.name', null, array('label'=>'Community'));
    }

}