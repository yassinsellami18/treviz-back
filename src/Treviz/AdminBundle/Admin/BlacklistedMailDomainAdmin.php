<?php

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 12/02/2017
 * Time: 16:47
 */

namespace Treviz\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class BlacklistedMailDomainAdmin
 * @package Treviz\AdminBundle\Admin
 *
 * Displays, filter and administrates the mail domains that should be blacklisted at user registration.
 */
class BlacklistedMailDomainAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('domain', 'text');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('domain');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('hash');
        $listMapper->add('domain');
    }

}