<?php

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 12/02/2017
 * Time: 16:47
 */

namespace Treviz\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class CommentAdmin
 * @package Treviz\AdminBundle\Admin
 *
 * Displays, filter and administrates the comments made on any post.
 */
class CommentAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('message', 'text');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('message');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('hash');
        $listMapper->add('message');
    }

}