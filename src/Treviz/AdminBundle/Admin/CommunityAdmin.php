<?php

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 12/03/2017
 * Time: 16:47
 */

namespace Treviz\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class CommunityAdmin
 * @package Treviz\AdminBundle\Admin
 *
 * Displays, filter and administrates the communities of the platform.
 */
class CommunityAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text');
        $formMapper->add('description', 'text');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('hash');
        $listMapper->add('name');
        $listMapper->add('description');
    }

}