<?php

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 12/02/2017
 * Time: 16:47
 */

namespace Treviz\AdminBundle\Admin;

use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class ProjectRoleAdmin
 * @package Treviz\AdminBundle\Admin
 *
 * Displays, filter and administrates the available roles for projects.
 */
class ProjectRoleAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text')
            ->add('permissions', 'choice', array(
                'multiple' => true,
                'choices' => ProjectPermissions::getAvailablePermissions(),
                'choice_label' => function($choice) {
                   return ProjectPermissions::getPermissionName($choice);
                }))
            ->add('global')
            ->add('defaultMember')
            ->add('defaultCreator');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('hash')
            ->add('name')
            ->add('global')
            ->add('defaultCreator')
            ->add('defaultMember')
            ->add('project.name', null, array('label'=>'Project'));
    }

}