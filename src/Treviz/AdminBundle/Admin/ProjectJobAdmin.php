<?php

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 12/02/2017
 * Time: 16:47
 */

namespace Treviz\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class ProjectJobAdmin
 * @package Treviz\AdminBundle\Admin
 *
 * Displays, filter and administrates the jobs of the organization.
 */
class ProjectJobAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General Information', array('class' => 'col-md-9'))
                ->add('name', 'text')
                ->add('description', 'text')
                ->add('monthlyReward', 'integer')
            ->end()
            ->with('Tags and Skills', array('class' => 'col-md-3'))
                ->add('tags', 'sonata_type_model', array('required' => false, 'expanded' => true, 'multiple' => true, 'property' => 'name'))
                ->add('skills', 'sonata_type_model', array('required' => false, 'expanded' => true, 'multiple' => true, 'property' => 'name'))
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name')
            ->add('project.name', null, array('label'=>'Project'))
            ->add('holder.username', null, array('label'=>'Job Holder'))
            ->add('contact.username', null, array('label'=>'Contact'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('hash')
            ->addIdentifier('name')
            ->add('description')
            ->add('project.name', null, array('label'=>'Project'))
            ->add('holder.username', null, array('label'=>'Job Holder'))
            ->add('contact.username', null, array('label'=>'Contact'));
    }

}