<?php

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 12/02/2017
 * Time: 16:47
 */

namespace Treviz\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class UserAdmin
 * @package Treviz\AdminBundle\Admin
 *
 * Displays, filter and administrates the members of the organization.
 */
class UserAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('username', 'text');
        $formMapper->add('firstName', 'text');
        $formMapper->add('lastName', 'text');
        $formMapper->add('email', 'text');
        $formMapper->add('description', 'text');
        $formMapper->add('enabled', 'sonata_type_boolean');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('username');
        $datagridMapper->add('firstName');
        $datagridMapper->add('lastName');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('username');
        $listMapper->add('firstName');
        $listMapper->add('lastName');
        $listMapper->add('email');
        $listMapper->add('enabled');
    }

}