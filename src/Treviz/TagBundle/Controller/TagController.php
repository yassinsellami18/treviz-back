<?php

namespace Treviz\TagBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\TagBundle\Entity\Tag;
use Treviz\TagBundle\Form\TagType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class TagController
 * @package Treviz\TagBundle\Controller
 *
 * @Security("has_role('ROLE_USER')")
 */
class TagController extends FOSRestController
{
    /**
     * Fetches all existing tags, or the ones matching the query string.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of tags, matching the query string",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Tag::class)
     *     )
     * )
     *
     * @SWG\Tag(name="tags")
     *
     * @QueryParam(name="name", description="Name, or part of the name of the tags to fetch", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getTagsAction(ParamFetcher $paramFetcher){

        $name = $paramFetcher->get("name");

        $qb = $this->getDoctrine()->getRepository("TrevizTagBundle:Tag")->createQueryBuilder("t");

        if ($name !== null) {

            $qb->andWhere("t.name LIKE :name")
                ->setParameter("name", "%" . $name . "%");

        }

        $tags = $qb->getQuery()->getResult();

        $view = $this->view($tags, 200);
        return $this->handleView($view);
    }

    /**
     * Creates a new tag
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created tag",
     *     @Model(type=Tag::class)
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="Tag already exists"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid Data"
     * )
     *
     * @SWG\Parameter(
     *     name="skill",
     *     in="body",
     *     description="Tag to create",
     *     required=true,
     *     @Model(type=Tag::class)
     * )
     *
     * @SWG\Tag(name="tags")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postTagsAction(Request $request){

        $tag = new Tag();
        $form = $this->createForm(TagType::class, $tag);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            if(!$this->getDoctrine()->getRepository("TrevizTagBundle:Tag")->findOneBy(array("name" => $tag->getName()))){

                $em = $this->getDoctrine()->getManager();
                $em->persist($tag);
                $em->flush();

                $view = $this->view($tag, 201);
                return $this->handleView($view);

            }

            $view = $this->view("Tag with same name already exists", 409);
            return $this->handleView($view);

        }

        $view = $this->view("Invalid data", 422);
        return $this->handleView($view);

    }

    /**
     * Updates an existing tag
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated tag",
     *     @Model(type=Tag::class)
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Tag does not exist"
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="Tag already exists"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid Data"
     * )
     *
     * @SWG\Parameter(
     *     name="skill",
     *     in="body",
     *     description="Updated Tag",
     *     required=true,
     *     @Model(type=Tag::class)
     * )
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="path",
     *     type="string",
     *     description="Name of the tag to update"
     * )
     *
     * @SWG\Tag(name="tags")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putTagAction(Request $request, $name)
    {

        $tag = $this->getDoctrine()->getRepository("TrevizTagBundle:Tag")->findOneBy(array("name" => $name));

        if ($tag !== null) {
            $form = $this->createForm(TagType::class, $tag);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                if(!$this->getDoctrine()->getRepository("TrevizTagBundle:Tag")->findOneBy(array("name" => $tag->getName()))){

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    $view = $this->view($tag, 201);
                    return $this->handleView($view);

                }

                $view = $this->view("Tag with same name already exists", 409);
                return $this->handleView($view);

            }

            $view = $this->view("Invalid data", 422);
            return $this->handleView($view);
        }

        $view = $this->view("No tag exists with name " . $name, 404);
        return $this->handleView($view);

    }

    /**
     * Creates a new tag
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the tag was correctly deleted"
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Tag does not exist"
     * )
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="path",
     *     type="string",
     *     description="Name of the tag to delete"
     * )
     *
     * @SWG\Tag(name="tags")
     *
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteTagAction($name)
    {

        $tag = $this->getDoctrine()->getRepository("TrevizTagBundle:Tag")->findOneBy(array("name" => $name));

        if ($tag !== null) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($tag);
            $em->flush();

            $view = $this->view(null, 204);
            return $this->handleView($view);
        }

        $view = $this->view("No tag exists with name " . $name, 404);
        return $this->handleView($view);

    }

}
