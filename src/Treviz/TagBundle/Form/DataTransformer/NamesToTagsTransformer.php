<?php

namespace Treviz\TagBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Treviz\TagBundle\Entity\Tag;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class NamesToTagsTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * NamesToTagsTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param ArrayCollection $tags
     * @return array
     */
    public function transform($tags)
    {
        $tagsNames = [];
        /** @var Tag $tag */
        foreach ($tags as $tag) {
            $tagsNames[] = $tag->getName();
        }
        return $tagsNames;
    }

    /**
     * @param array $tagsNames
     * @return ArrayCollection
     */
    public function reverseTransform($tagsNames)
    {
        $tags = new ArrayCollection();

        foreach ($tagsNames as $tagName) {
            if ($tagName !== null) {
                $tag = $this->em->getRepository("TrevizTagBundle:Tag")
                    ->findOneByName($tagName);

                if ($tag == null) {
                    $tag = new Tag();
                    $tag->setName($tagName);
                }

                $tags->add($tag);
            }
        }

        return $tags;

    }
}