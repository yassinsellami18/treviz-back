<?php

namespace Treviz\BrainstormingBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Treviz\BrainstormingBundle\Entity\Idea;
use Treviz\BrainstormingBundle\Entity\Session;
use Treviz\BrainstormingBundle\Form\IdeaType;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;

/**
 *
 * @Security("has_role('ROLE_USER')")
 *
 * Class IdeaController
 * @package Treviz\BrainstormingBundle\Controller
 */
class IdeaController extends FOSRestController
{

    /**
     * Fetches the ideas of a community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of the ideas of a specific brainstorming session",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Get("/brainstorming-sessions/{hash}/ideas")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getIdeasAction($hash)
    {

        /** @var Session $session */
        $session = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Session")
            ->findOneBy(array("hash" => $hash));

        if ($session) {

            /*
             * Check if the user has access to it.
             */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $this->getUser(),
                    "community" => $session->getCommunity()
                ));

            if ($membership !== null) {

                $view = $this->view($session->getIdeas(), 200);
                return $view;

            }

            $view = $this->view("You can't see the ideas of this session", 403);
            return $view;

        }

        $view = $this->view("No brainstorming session was found for this hash", 400);
        return $view;

    }

    /**
     * Fetch a specific idea.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when an idea was successfully fetched",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Get("/brainstorming-sessions/ideas/{hash}")
     *
     * @View(serializerGroups={"Default", "idea"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getIdeaAction($hash)
    {

        /** @var Idea $idea */
        $idea = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Idea")
            ->findOneBy(array("hash" => $hash));

        if ($idea) {

            /*
             * Check if the user has access to it.
             */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $this->getUser(),
                    "community" => $idea->getSession()->getCommunity()
                ));

            if ($membership !== null) {

                $view = $this->view($idea, 200);
                return $view;

            }

            $view = $this->view("You can't see the ideas of this session", 403);
            return $view;

        }

        $view = $this->view("No idea session was found for this hash", 400);
        return $view;

    }

    /**
     * Adds a new idea to a brainstorming session.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when an idea was successfully created",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Post("/brainstorming-sessions/{hash}/ideas")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postIdeaAction(Request $request, $hash)
    {

        /** @var Session $session */
        $session = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Session")
            ->findOneBy(array("hash" => $hash));

        if ($session) {

            /*
             * Check if the user has access to it.
             */

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $this->getUser(),
                    "community" => $session->getCommunity()
                ));

            if ($membership !== null && ($session->isOpen())) {

                $idea = new Idea();
                $form = $this->createForm(IdeaType::class, $idea);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $hash = hash("sha256", random_bytes(256));
                    while($this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Idea")->findOneBy(array("hash" => $hash))) {
                        $hash = hash("sha256", random_bytes(256));
                    }

                    $idea->setSession($session);
                    $idea->setHash($hash);
                    $idea->setAuthor($this->getUser());

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($idea);
                    $em->flush();

                    $view = $this->view($idea, 201);
                    return $view;

                }

                $view = $this->view($form, 422);
                return $view;

            }


            $view = $this->view("You can't post an idea for this session", 403);
            return $view;

        }

        $view = $this->view("No brainstorming session was found for this hash", 400);
        return $view;

    }

    /**
     * Updates an existing idea.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when an idea was successfully updated",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Put("/brainstorming-sessions/ideas/{hash}")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putIdeaAction(Request $request, $hash)
    {
        /** @var Session $session */
        $idea = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Idea")
            ->findOneBy(array("hash" => $hash));

        if ($idea) {

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $this->getUser(),
                    "community" => $idea->getSession()->getCommunity()
                ));

            /*
             * Check if the user is its author or is an admin of the community.
             */
            if (($this->getUser() === $idea->getAuthor()
                || ($membership !== null
                    && in_array(CommunityPermissions::MANAGE_BRAINSTORMING_IDEAS, $membership->getRole()->getCommunityPermissions())))
                && ($session->isOpen())) {

                $form = $this->createForm(IdeaType::class, $idea);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    $view = $this->view($idea, 201);
                    return $view;

                }

                $view = $this->view($form, 422);
                return $view;

            }

            $view = $this->view("You can't update this idea", 403);
            return $view;

        }

        $view = $this->view("No idea was found for this hash", 400);
        return $view;

    }

    /**
     * Deletes an existing idea.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when an idea was successfully deleted",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Delete("/brainstorming-sessions/ideas/{hash}")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteIdeaAction($hash)
    {
        /** @var Session $session */
        $idea = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Idea")
            ->findOneBy(array("hash" => $hash));

        if ($idea) {

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $this->getUser(),
                    "community" => $idea->getSession()->getCommunity()
                ));

            /*
             * Check if the user is its author or is an admin of the community.
             */
            if ($this->getUser() === $idea->getAuthor()
            || (($membership !== null
                && in_array(CommunityPermissions::MANAGE_BRAINSTORMING_IDEAS, $membership->getRole()->getCommunityPermissions())))
                && ($session->isOpen())) {

                $em = $this->getDoctrine()->getManager();
                $em->remove($idea);
                $em->flush();

                $view = $this->view($idea, 201);
                return $view;

            }

            $view = $this->view("You can't update this idea", 403);
            return $view;

        }

        $view = $this->view("No idea was found for this hash", 400);
        return $view;
    }
    
}
