<?php

namespace Treviz\BrainstormingBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Treviz\BrainstormingBundle\Entity\Idea;
use Treviz\BrainstormingBundle\Entity\IdeaEnhancement;
use Treviz\BrainstormingBundle\Form\IdeaEnhancementType;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\View;
use Swagger\Annotations as SWG;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class IdeaEnhancementController
 * @package Treviz\BrainstormingBundle\Controller
 */
class IdeaEnhancementController extends FOSRestController
{

    /**
     * Fetches the enhancements of an existing idea.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the array containing the enhancements of an idea",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Get("/brainstorming-sessions/ideas/{hash}/enhancements")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getIdeaEnhancementsAction($hash)
    {

        /** @var Idea $idea */
        $idea = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Idea")
            ->findOneBy(array("hash" => $hash));

        if ($idea) {

            /*
             * Check if the user has access to it.
             */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $this->getUser(),
                    "community" => $idea->getSession()->getCommunity()
                ));

            if ($membership !== null) {

                $view = $this->view($idea->getEnhancements(), 200);
                return $view;

            }

            $view = $this->view("You can't see the enhancements of this idea", 403);
            return $view;

        }

        $view = $this->view("No idea was found for this hash", 400);
        return $view;

    }

    /**
     * Fetch a specific idea enhancement.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a enhancement was successfully fetched",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Get("/brainstorming-sessions/ideas/enhancements/{hash}")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getIdeaEnhancementAction($hash)
    {

        /** @var IdeaEnhancement $enhancement */
        $enhancement = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:IdeaEnhancement")
            ->findOneBy(array("hash" => $hash));

        if ($enhancement) {

            /*
             * Check if the user has access to it.
             */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $this->getUser(),
                    "community" => $enhancement->getIdea()->getSession()->getCommunity()
                ));

            if ($membership !== null) {

                $view = $this->view($enhancement, 200);
                return $view;

            }

            $view = $this->view("You can't see this enhancement", 403);
            return $view;

        }

        $view = $this->view("No idea enhancement was found for this hash", 400);
        return $view;

    }

    /**
     * Adds a enhancement to an existing idea.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a enhancement was successfully created",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Post("/brainstorming-sessions/ideas/{hash}/enhancements")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postIdeaEnhancementAction(Request $request, $hash)
    {

        /** @var Idea $idea */
        $idea = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Idea")
            ->findOneBy(array("hash" => $hash));

        if ($idea) {

            /*
             * Check if the user has access to it.
             */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $this->getUser(),
                    "community" => $idea->getSession()->getCommunity()
                ));

            if ($membership !== null && ($idea->getSession()->isOpen()) ) {

                $enhancement = new IdeaEnhancement();
                $form = $this->createForm(IdeaEnhancementType::class, $enhancement);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $hash = hash("sha256", random_bytes(256));
                    while($this->getDoctrine()->getRepository("TrevizBrainstormingBundle:IdeaEnhancement")->findOneBy(array("hash" => $hash))) {
                        $hash = hash("sha256", random_bytes(256));
                    }

                    $enhancement->setIdea($idea);
                    $enhancement->setHash($hash);
                    $enhancement->setAuthor($this->getUser());

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($enhancement);
                    $em->flush();

                    $view = $this->view($enhancement, 201);
                    return $view;

                }

                $view = $this->view("Invalid data", 422);
                return $view;

            }

            $view = $this->view("You can't post a enhancement for this idea", 403);
            return $view;

        }

        $view = $this->view("No idea was found for this hash", 400);
        return $view;

    }

    /**
     * Updates an existing idea enhancement.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a enhancement was successfully updated",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Put("/brainstorming-sessions/ideas/enhancements/{hash}")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putIdeaEnhancementAction(Request $request, $hash)
    {

        /** @var Idea $idea */
        $enhancement = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:IdeaEnhancement")
            ->findOneBy(array("hash" => $hash));

        if ($enhancement) {

            /*
             * Check if the user has access to it.
             */
            /** @var CommunityMembership $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $this->getUser(),
                    "community" => $idea->getSession()->getCommunity()
                ));

            if ($membership !== null
            && (($enhancement->getAuthor() == $this->getUser()
             || in_array(CommunityPermissions::MANAGE_BRAINSTORMING_IDEAS, $membership->getRole()->getPermissions())))
                && ($idea->getSession()->isOpen())) {

                $form = $this->createForm(IdeaEnhancementType::class, $enhancement);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    $view = $this->view($idea, 200);
                    return $view;

                }

                $view = $this->view("Invalid data", 422);
                return $view;

            }

            $view = $this->view("You can't update this enhancement", 403);
            return $view;

        }

        $view = $this->view("No enhancement was found for this hash", 400);
        return $view;

    }

    /**
     * Deletes an existing idea enhancement.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when a enhancement was successfully deleted",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Delete("/brainstorming-sessions/ideas/enhancements/{hash}")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteIdeaEnhancementAction($hash)
    {

        /** @var Idea $idea */
        $enhancement = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:IdeaEnhancement")
            ->findOneBy(array("hash" => $hash));

        if ($enhancement) {

            /*
             * Check if the user has access to it.
             */
            /** @var CommunityMembership $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $this->getUser(),
                    "community" => $idea->getSession()->getCommunity()
                ));

            if (($membership !== null
                && ($enhancement->getAuthor() == $this->getUser()
                    || in_array(CommunityPermissions::MANAGE_BRAINSTORMING_IDEAS, $membership->getRole()->getPermissions())))
                && ($idea->getSession()->isOpen())) {

                $em = $this->getDoctrine()->getManager();
                $em->remove($enhancement);
                $em->flush();

                $view = $this->view(null, 204);
                return $view;

            }

            $view = $this->view("You can't update this enhancement", 403);
            return $view;

        }

        $view = $this->view("No enhancement was found for this hash", 400);
        return $view;

    }
}
