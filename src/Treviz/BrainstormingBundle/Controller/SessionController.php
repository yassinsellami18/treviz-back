<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 01/11/2017
 * Time: 20:08
 */

namespace Treviz\BrainstormingBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\BrainstormingBundle\Entity\Session;
use Treviz\BrainstormingBundle\Form\SessionType;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Treviz\BrainstormingBundle\Events\BrainstormingCreatedEvent;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Swagger\Annotations as SWG;
use Treviz\ProjectBundle\Entity\ProjectMembership;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class SessionController
 * @package Treviz\BrainstormingBundle\Controller
 */
class SessionController extends FOSRestController
{

    /**
     * Fetches the brainstorming sessions of a project or community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of brainstorming sessiongs for the selected community",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Get("/brainstorming-sessions")
     *
     * @QueryParam(name="community", description="hash of the community from which fetch the brainstorming sessions")
     * @QueryParam(name="project", description="hash of the project from which fetch the brainstorming sessions")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getSessionsAction(ParamFetcher $paramFetcher)
    {

        $qb = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Session")->createQueryBuilder('session');

        if($community = $paramFetcher->get('community')) {
            $qb->join('session.community', 'community');
            $qb->andWhere('community.hash = :communityHash')
               ->andWhere('EXISTS (
                            SELECT cm
                            FROM TrevizCommunityBundle:CommunityMembership cm
                            WHERE cm.community = community
                            AND cm.user = :user
                          )');
            $qb->setParameter('communityHash', $community);
            $qb->setParameter('user', $this->getUser());
        } else {
            $qb->andWhere('session.community IS NULL');
        }

        if($project = $paramFetcher->get('project')) {
            $qb->join('session.project', 'project');
            $qb->andWhere('project.hash = :projectHash')
                ->andWhere('EXISTS (
                            SELECT pm
                            FROM TrevizProjectBundle:ProjectMembership pm
                            WHERE pm.project = project
                            AND pm.user = :user
                          )');
            $qb->setParameter('projectHash', $project);
            $qb->setParameter('user', $this->getUser());
        } else {
            $qb->andWhere('session.project IS NULL');
        }

        $view = $this->view($qb->getQuery()->getResult(), 200);
        return $view;

    }

    /**
     * Fetches a specific brainstorming session.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a session was successfully fetched",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Get("/brainstorming-sessions/{hash}")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getSessionAction($hash)
    {

        $session = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Session")
            ->findOneBy(array("hash" => $hash));

        if ($session) {

            /*
             * Check if the user has access to it.
             */

            if ($session->getCommunity()) {
                $communityMembership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                    ->findOneBy(array(
                        "user" => $this->getUser(),
                        "community" => $session->getCommunity()
                    ));
                if ($communityMembership == null) {
                    return $this->view('You cannot view sessions from this community', 403);
                }

            }

            if ($session->getProject()) {
                $projectMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "user" => $this->getUser(),
                        "project" => $session->getProject()
                    ));
                if ($projectMembership == null) {
                    return $this->view('You cannot view sessions from this community', 403);
                }
            }

            $view = $this->view($session, 200);
            return $view;

        }

        $view = $this->view("No brainstorming session was found for this hash", 400);
        return $view;

    }

    /**
     * Creates a new session, for a community or project.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a session was successfully created",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Post("/brainstorming-sessions")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postSessionAction(Request $request)
    {

        $session = new Session();
        $form = $this->createForm(SessionType::class, $session);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            if ($session->getCommunity() !== null) {
                /** @var CommunityMembership $membership */
                $membership = $this->getDoctrine()->getRepository('TrevizCommunityBundle:CommunityMembership')
                    ->findOneBy(array(
                        'user' => $this->getUser(),
                        'community' => $session->getCommunity()
                    ));
                if ($membership == null
                    || !in_array(CommunityPermissions::MANAGE_BRAINSTORMING_SESSION, $membership->getRole()->getPermissions())
                ) {
                    return $this->view('You are not authorized to create brainstorming sessions in this community', 403);
                }
            }

            if ($session->getProject() !== null) {
                /** @var ProjectMembership $membership */
                $membership = $this->getDoctrine()->getRepository('TrevizProjectBundle:ProjectMembership')
                    ->findOneBy(array(
                        'user' => $this->getUser(),
                        'project' => $session->getProject()
                    ));
                if ($membership == null
                    || !in_array(ProjectPermissions::MANAGE_BRAINSTORMING_SESSION, $membership->getRole()->getPermissions())
                ) {
                    return $this->view('You are not authorized to create brainstorming sessions in this community', 403);
                }
            }

            $hash = hash("sha256", random_bytes(256));
            while($this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Session")
                    ->findOneBy(array("hash" => $hash)) !== null) {
                $hash = hash("sha256", random_bytes(256));
            }

            $session->setHash($hash);
            $em = $this->getDoctrine()->getManager();
            $em->persist($session);
            $em->flush();

            $event = new BrainstormingCreatedEvent($session);
            $dispatcher = $this->get('treviz_core.event_dispatcher.dispatcher');
            $dispatcher->dispatch(BrainstormingCreatedEvent::NAME, $event);

            $view = $this->view($session, 201);
            return $view;

        }

        $view = $this->view($form, 422);
        return $view;

    }

    /**
     * Updates an existing session, for instance to close it.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a session was successfully updated",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Put("/communities/brainstorming/{hash}")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putSessionAction(Request $request, $hash)
    {

        $session = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Session")
            ->findOneBy(array("hash" => $hash));

        if ($session) {

            $form = $this->createForm(SessionType::class, $session);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                if ($session->getCommunity() !== null) {
                    /** @var CommunityMembership $membership */
                    $membership = $this->getDoctrine()->getRepository('TrevizCommunityBundle:CommunityMembership')
                        ->findOneBy(array(
                            'user' => $this->getUser(),
                            'community' => $session->getCommunity()
                        ));
                    if ($membership == null
                        || !in_array(CommunityPermissions::MANAGE_BRAINSTORMING_SESSION, $membership->getRole()->getPermissions())
                    ) {
                        return $this->view('You are not authorized to update brainstorming sessions in this community', 403);
                    }
                }

                if ($session->getProject() !== null) {
                    /** @var ProjectMembership $membership */
                    $membership = $this->getDoctrine()->getRepository('TrevizProjectBundle:ProjectMembership')
                        ->findOneBy(array(
                            'user' => $this->getUser(),
                            'project' => $session->getProject()
                        ));
                    if ($membership == null
                        || !in_array(ProjectPermissions::MANAGE_BRAINSTORMING_SESSION, $membership->getRole()->getPermissions())
                    ) {
                        return $this->view('You are not authorized to update brainstorming sessions in this community', 403);
                    }
                }

                $em = $this->getDoctrine()->getManager();
                $em->flush();

                $view = $this->view($session, 200);
                return $view;

            }

            $view = $this->view("Invalid data", 422);
            return $view;

        }

        $view = $this->view("No brainstorming session was found for this hash", 400);
        return $view;
    }

    /**
     * Removes an existing session.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when a session was successfully deleted",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Delete("/communities/brainstorming/{hash}")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteSessionAction($hash)
    {

        $session = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Session")
            ->findOneBy(array("hash" => $hash));

        if ($session) {

            /*
             * Check if the user has access to it and can launch brainstorming sessions.
             */

            if ($session->getCommunity() !== null) {
                /** @var CommunityMembership $membership */
                $membership = $this->getDoctrine()->getRepository('TrevizCommunityBundle:CommunityMembership')
                    ->findOneBy(array(
                        'user' => $this->getUser(),
                        'community' => $session->getCommunity()
                    ));
                if ($membership == null
                    || !in_array(CommunityPermissions::MANAGE_BRAINSTORMING_SESSION, $membership->getRole()->getPermissions())
                ) {
                    return $this->view('You are not authorized to delete brainstorming sessions in this community', 403);
                }
            }

            if ($session->getProject() !== null) {
                /** @var ProjectMembership $membership */
                $membership = $this->getDoctrine()->getRepository('TrevizProjectBundle:ProjectMembership')
                    ->findOneBy(array(
                        'user' => $this->getUser(),
                        'project' => $session->getProject()
                    ));
                if ($membership == null
                    || !in_array(ProjectPermissions::MANAGE_BRAINSTORMING_SESSION, $membership->getRole()->getPermissions())
                ) {
                    return $this->view('You are not authorized to delete brainstorming sessions in this community', 403);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->remove($session);
            $em->flush();

            $view = $this->view(null, 204);
            return $view;

        }

        $view = $this->view("No brainstorming session was found for this hash", 400);
        return $view;

    }

}