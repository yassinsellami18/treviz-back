<?php

namespace Treviz\BrainstormingBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Treviz\BrainstormingBundle\Entity\Idea;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class LikeController
 * @package Treviz\BrainstormingBundle\Controller
 */
class LikeController extends FOSRestController
{
    /**
     * Like an idea as a user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when an idea was successfully liked by the user authenticated by the auth. token",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Get("/brainstorming-sessions/ideas/{hash}/like")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function likeIdeaAction($hash)
    {

        /** @var Idea $idea */
        $idea = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Idea")
            ->findOneBy(array("hash" => $hash));

        if ($idea) {

            $user = $this->getUser();

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $user,
                    "community" => $idea->getSession()->getCommunity()
                ));


            if ($membership != null && ($idea->getSession()->isOpen())) {

                if(!$idea->getLiked()->contains($user)){

                    $idea->addLiked($user);
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->view($idea, 200);

                }

                return $this->view("You have already liked this idea", 409);

            }

            return $this->view("You cannot like this idea", 403);
        }

        return $this->view("No idea was found for hash " . $hash, 404);

    }

    /**
     *
     * Stop liking an idea as a user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user stopped liking an idea",
     * )
     *
     * @SWG\Tag(name="brainstorming")
     *
     * @Get("/brainstorming-sessions/ideas/{hash}/unlike")
     *
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function unlikeIdeaAction($hash)
    {
        /** @var Idea $idea */
        $idea = $this->getDoctrine()->getRepository("TrevizBrainstormingBundle:Idea")
            ->findOneBy(array("hash" => $hash));

        if ($idea) {

            $user = $this->getUser();

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $user,
                    "community" => $idea->getSession()->getCommunity()
                ));


            if ($membership != null && ($idea->getSession()->isOpen())) {

                if($idea->getLiked()->contains($user)){

                    $idea->removeLiked($user);
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->view($idea, 200);

                }

                return $this->view("You already do not like this idea", 409);

            }

            return $this->view("You cannot unlike this idea", 403);
        }

        return $this->view("No idea was found for hash " . $hash, 404);

    }
}
