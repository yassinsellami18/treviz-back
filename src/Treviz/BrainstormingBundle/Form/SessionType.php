<?php

namespace Treviz\BrainstormingBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\CommunityBundle\Form\DataTransformer\HashToCommunityTransformer;
use Treviz\ProjectBundle\Form\DataTransformer\HashToProjectTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SessionType extends AbstractType
{

    private $em;

    /**
     * SessionType constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('open', TextType::class, array("required" => false))
            ->add('project', TextType::class, array("required" => false))
            ->add('community', TextType::class, array("required" => false));

        $builder->get('project')->addModelTransformer(new HashToProjectTransformer($this->em), true);
        $builder->get('community')->addModelTransformer(new HashToCommunityTransformer($this->em), true);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\BrainstormingBundle\Entity\Session',
            'csrf_protection' => false
        ));
    }

}
