<?php

namespace Treviz\BrainstormingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Treviz\BrainstormingBundle\Entity\Superclass\Hashable;
use Treviz\ProjectBundle\Entity\Project;
use Treviz\CoreBundle\Entity\User;
use JMS\Serializer\Annotation as JMS;
use Treviz\DocumentBundle\Entity\Document;

/**
 * Idea
 *
 * @ORM\Table(name="idea")
 * @ORM\Entity(repositoryClass="Treviz\BrainstormingBundle\Repository\IdeaRepository")
 */
class Idea extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var Session
     *
     * @ORM\ManyToOne(targetEntity="Treviz\BrainstormingBundle\Entity\Session", inversedBy="ideas", cascade={"persist"})
     * @JMS\Groups({"idea"})
     */
    private $session;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Treviz\BrainstormingBundle\Entity\IdeaEnhancement", mappedBy="idea", cascade={"persist"})
     * @JMS\Exclude()
     */
    private $enhancements;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Treviz\CoreBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinTable(name="idea_liked")
     * @JMS\MaxDepth(3)
     */
    private $liked;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Treviz\ProjectBundle\Entity\Project", mappedBy="idea")
     * @JMS\MaxDepth(3)
     */
    private $forked;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Treviz\CoreBundle\Entity\User", inversedBy="ideas", cascade={"persist"})
     * @JMS\MaxDepth(3)
     */
    private $author;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Treviz\DocumentBundle\Entity\Document", mappedBy="idea", cascade={"persist", "remove"})
     */
    private $documents;

    public function __construct()
    {
        $this->enhancements = new ArrayCollection();
        $this->liked = new ArrayCollection();
        $this->forked = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Idea
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @return Session
     */
    public function getSession(): ?Session
    {
        return $this->session;
    }

    /**
     * @param Session $session
     */
    public function setSession(Session $session)
    {
        $this->session = $session;
        if (!$session->getIdeas()->contains($this)) {
            $session->addIdea($this);
        }
    }

    /**
     * @return Collection
     */
    public function getEnhancements(): Collection
    {
        return $this->enhancements;
    }

    /**
     * @param ArrayCollection $enhancements
     */
    public function setEnhancements($enhancements)
    {
        $this->enhancements = $enhancements;
    }

    /**
     * @param IdeaEnhancement $enhancement
     */
    public function addEnhancement(IdeaEnhancement $enhancement)
    {
        $this->enhancements->add($enhancement);

        if ($enhancement->getIdea() == null) {
            $enhancement->setIdea($this);
        }
    }

    public function removeEnhancement(IdeaEnhancement $enhancement)
    {
        $this->enhancements->removeElement($enhancement);
    }

    /**
     * @return Collection
     */
    public function getLiked(): Collection
    {
        return $this->liked;
    }

    /**
     * @param mixed $liked
     */
    public function setLiked($liked)
    {
        $this->liked = $liked;
    }

    public function addLiked(User $user)
    {
        $this->liked->add($user);
    }

    public function removeLiked(User $user)
    {
        $this->liked->removeElement($user);
    }

    /**
     * @return Collection
     */
    public function getForked(): Collection
    {
        return $this->forked;
    }

    /**
     * @param mixed $forked
     */
    public function setForked($forked)
    {
        $this->forked = $forked;
    }

    public function addForked(Project $project)
    {
        $this->forked->add($project);
        if ($project->getIdea() == null) {
            $project->setIdea($this);
        }
    }

    /**
     * @return User
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        if(!$author->getIdeas()->contains($this)){
            $author->addIdea($this);
        }
        $this->author = $author;
    }

    /**
     * @return Collection
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    /**
     * @param ArrayCollection $documents
     */
    public function setDocuments($documents)
    {
        $this->documents = $documents;
    }

    /**
     * @param Document $document
     */
    public function addDocument(Document $document)
    {
        $this->documents->add($document);
    }

    /**
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        $this->documents->removeElement($document);
    }

}
