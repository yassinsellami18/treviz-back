<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 28/10/2017
 * Time: 15:28
 */

namespace Treviz\BrainstormingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Treviz\BrainstormingBundle\Entity\Enums\SessionStatusEnum;
use Treviz\BrainstormingBundle\Entity\Superclass\Hashable;
use Treviz\CommunityBundle\Entity\Community;
use Treviz\ProjectBundle\Entity\Project;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Brainstorming session
 *
 * @ORM\Table(name="brainstorming_session")
 * @ORM\Entity(repositoryClass="Treviz\BrainstormingBundle\Repository\SessionRepository")
 */
class Session extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var boolean $open
     *
     * @ORM\Column(name="open", type="boolean")
     */
    protected $open = true;

    /**
     * @ORM\ManyToOne(targetEntity="Treviz\CommunityBundle\Entity\Community", inversedBy="brainstormingSessions", cascade={"persist"})
     * @JMS\Groups({"idea"})
     */
    protected $community;

    /**
     * @ORM\ManyToOne(targetEntity="Treviz\ProjectBundle\Entity\Project", inversedBy="brainstormingSessions", cascade={"persist"})
     * @JMS\Exclude()
     */
    protected $project;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Idea", mappedBy="session", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    protected $ideas;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    protected $startDate;

    /**
     * Session constructor.
     */
    public function __construct()
    {
        $this->startDate = new \DateTime();
        $this->ideas = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return boolean
     */
    public function isOpen(): ?bool
    {
        return $this->open;
    }

    /**
     * @param boolean $open
     */
    public function setOpen(bool $open)
    {
        $this->open = $open;
    }

    /**
     * @return Community
     */
    public function getCommunity(): ?Community
    {
        return $this->community;
    }

    /**
     * @param Community $community
     */
    public function setCommunity(Community $community)
    {
        $this->community = $community;
        if (!$community->getBrainstormingSessions()->contains($this)) {
            $community->addBrainstormingSession($this);
        }
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
        if (!$project->getBrainstormingSessions()->contains($this)) {
            $project->addBrainstormingSession($this);
        }
    }

    /**
     * @return Collection
     */
    public function getIdeas(): Collection
    {
        return $this->ideas;
    }

    /**
     * @param ArrayCollection $ideas
     */
    public function setIdeas(ArrayCollection $ideas)
    {
        $this->ideas = $ideas;
    }

    /**
     * @param Idea $idea
     */
    public function addIdea(Idea $idea)
    {
        $this->ideas->add($idea);
        if ($idea->getSession() == null) {
            $idea->setSession($this);
        }
    }

    /**
     * @param Idea $idea
     */
    public function removeIdea(Idea $idea)
    {
        $this->ideas->removeElement($idea);
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    /**
     * @JMS\VirtualProperty()
     */
    public function getNbIdeas()
    {
        return $this->getIdeas()->count();
    }


}