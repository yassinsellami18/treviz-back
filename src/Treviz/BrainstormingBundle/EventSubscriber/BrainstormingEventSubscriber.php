<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 23:27
 */

namespace Treviz\BrainstormingBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\BrainstormingBundle\Events\BrainstormingCreatedEvent;
use Treviz\CoreBundle\Services\MailerService;

/**
 * Class BrainstormingEventSubscriber
 *
 * Listens to brainstorming creations and update, to trigger the appropriate
 * notifications to be sent.
 *
 * @package Treviz\CommunityBundle\EventSubscriber
 */
class BrainstormingEventSubscriber implements EventSubscriberInterface
{

    private $mailerService;
    private $frontendUrl;

    public function __construct(MailerService $service, string $frontUrl)
    {
        $this->mailerService = $service;
        $this->frontendUrl = $frontUrl;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            BrainstormingCreatedEvent::NAME      => 'onBrainstormingCreated',
        );
    }

    public function onBrainstormingCreated(BrainstormingCreatedEvent $event)
    {
        $brainstorming = $event->getBrainstorming();
        $community = $brainstorming->getCommunity();
        $message = "A new brainstorming session " . $brainstorming->getName() . " has just been created in your community " . $community->getName();
        $url = $this->frontendUrl . "/communities/" . $community->getHash();
        /** @var CommunityMembership $membership */
        if ($community !== null) {
            foreach ($community->getMemberships() as $membership) {
                if ($membership->getPreferences()->isOnNewBrainstorming()) {
                    $this->mailerService->sendUpdateMail(
                        $membership->getUser(),
                        $message,
                        $url
                    );
                }
            }
        }

    }
}