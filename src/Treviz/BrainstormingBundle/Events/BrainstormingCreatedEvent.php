<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 23:41
 */

namespace Treviz\BrainstormingBundle\Events;


use Symfony\Component\EventDispatcher\Event;
use Treviz\BrainstormingBundle\Entity\Session;

class BrainstormingCreatedEvent extends Event
{

    const NAME = 'brainstorming.created';

    protected $brainstorming;

    public function __construct(Session $brainstorming)
    {
        $this->brainstorming = $brainstorming;
    }

    /**
     * @return Session
     */
    public function getBrainstorming(): Session
    {
        return $this->brainstorming;
    }

}