<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 11/07/2017
 * Time: 22:47
 */

namespace Treviz\ChatBundle\Controller;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\ChatBundle\Entity\Room;
use FOS\RestBundle\Controller\Annotations as Rest;
use Treviz\ChatBundle\Form\RoomType;
use Treviz\CoreBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class RoomController
 * @package Treviz\ChatBundle\Controller
 */
class RoomController extends FOSRestController
{
    /**
     * Fetches the rooms according to the query parameters.
     * A user can see all the rooms of a project or community he is member of; but he can only
     * post messages in rooms he is member of.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of rooms matching the query",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Get("/rooms")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @QueryParam(name="project", description="hash of the project from which the rooms should be fetched", nullable=true)
     * @QueryParam(name="community", description="hash of the community from which the rooms should be fetched", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getRoomsAction(ParamFetcher $paramFetcher) {

        $s_project = $paramFetcher->get("project");
        $s_community = $paramFetcher->get("community");

        /** @var User $user */
        $user = $this->getUser();
        $qb = $this->getDoctrine()->getRepository("TrevizChatBundle:Room")->createQueryBuilder("room");


        if ($s_project) {
            $qb->join("room.project", "project");
            $qb->andWhere("EXISTS(
                            SELECT pm
                            FROM TrevizProjectBundle:ProjectMembership pm
                            WHERE pm.project = project
                            AND pm.user = :user
                           )");
            $qb->andWhere("project.hash =  :project_hash");
            $qb->setParameter("project_hash", $s_project);
        }

        if ($s_community) {
            $qb->join("room.community", "community");
            $qb->andWhere("EXISTS(
                            SELECT cm
                            FROM TrevizCommunityBundle:CommunityMembership cm
                            WHERE cm.community = community
                            AND cm.user = :user
                           )");
            $qb->andWhere("community.hash =  :community_hash");
            $qb->setParameter("community_hash", $s_community);
        }

        if ($s_project == null && $s_community == null) {
            $qb->andWhere(":user MEMBER OF room.users");
        }

        $qb->setParameter("user", $user);

        return $this->view($qb->getQuery()->getResult(), 200);

    }

    /**
     * Creates a new room for a project or community.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a chat room was successfully created",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Post("/rooms")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @return Room
     * @internal param ParamFetcher $paramFetcher
     */
    public function postRoomsAction(Request $request) {

        $room = new Room();
        $form = $this->createForm(RoomType::class, $room);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            /** @var User $user */
            $user = $this->getUser();
            $room->addUser($user);

            $roomHash = hash("sha256", random_bytes(256));
            while ($this->getDoctrine()->getRepository("TrevizChatBundle:Room")->findOneBy(array("hash" => $roomHash))) {
                $roomHash = hash("sha256", random_bytes(256));
            }

            $room->setHash($roomHash);

            $em = $this->getDoctrine()->getManager();

            if ($project = $room->getProject() && $room->getCommunity() == null) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $project,
                        "user" => $this->getUser()
                    ));

                if($membership !== null) {

                    $em->persist($room);
                    $em->flush();

                    $view = $this->view($room, 200);
                    return $view;

                }

                return $this->view("You do not have the rights to do this", 403);

            } elseif ($community = $room->getCommunity() && $room->getProject() == null) {

                $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                    ->findOneBy(array(
                        "community" => $community,
                        "user" => $this->getUser()
                    ));

                if($membership !== null) {

                    $em->persist($room);
                    $em->flush();

                    $view = $this->view($room, 201);
                    return $view;

                }

                return $this->view("You do not have the rights to do this", 403);

            }

            $em->persist($room);
            $em->flush();

            return $room;

        }

        throw new HttpException(400, $form);

    }

    /**
     * Updates an existing chat room.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a chat room was successfully updated",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Put("/rooms/{hash}")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param string $hash Hash of the room to update
     * @param Request $request JSON Body containing the updated room
     * @return Room
     */
    public function putRoomAction($hash, Request $request)
    {
        /** @var Room $room */
        $room = $this->getDoctrine()->getRepository('TrevizChatBundle:Room')->findOneByHash($hash);
        if ($room !== null) {
            $form = $this->createForm(RoomType::class, $room);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                if ($room->getUsers()->contains($this->getUser())) {
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                    return $this->view($room, 200);
                }

                throw new HttpException(403, 'Only members of this room can update it');

            }

            throw new HttpException(400, $form);

        }

        throw new HttpException(404, 'No room was found with hash ' . $hash);

    }

    /**
     * Add users to a chat room.
     *
     * We expect a JSON array of usernames : ['johndoe', 'janesmith']
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a user was successfully added to a room",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Post("/rooms/{hash}/invite")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @param Request $request
     * @return Room
     */
    public function addUserToRoomAction($hash, Request $request)
    {

        $room = $this->getDoctrine()->getRepository("TrevizChatBundle:Room")->findOneBy(array("hash" => $hash));

        if ($room) {

            if($room->getUsers()->contains($this->getUser())){

                $usernames = json_decode($request->getContent());

                foreach ($usernames as $username) {
                    $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(array("username" => $username));

                    if ($user && !$room->getUsers()->contains($user)) {

                        $room->addUser($user);

                    }

                }

                $this->getDoctrine()->getManager()->flush();
                return $room;

            }

            throw new HttpException(403, "You are not authorized to do this");

        }

        throw new HttpException(404, "No room was found for hash " . $hash);
    }

    /**
     * Removes a user from a chat room.
     * If the chat rooms has no more users, deletes it.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user was successfully removed from a room",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Post("/rooms/{hash}/remove")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function removeUserFromRoomAction($hash, Request $request)
    {
        $room = $this->getDoctrine()->getRepository("TrevizChatBundle:Room")->findOneBy(array("hash" => $hash));

        if ($room) {

            if($room->getUsers()->contains($this->getUser())){

                $usernames = json_decode($request->getContent());

                foreach ($usernames as $username) {
                    $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(array("username" => $username));

                    if ($user && $room->getUsers()->contains($user)) {

                        $room->removeUser($user);

                    }

                }

                $em = $this->getDoctrine()->getManager();

                if ($room->getUsers()->count() === 0) {
                    $em->remove($room);
                }

                $em->flush();
                return $this->view($room, 200);

            }

            throw new HttpException(403, "You are not authorized to do this");

        }

        throw new HttpException(404, "No room was found for hash " . $hash);
    }

}