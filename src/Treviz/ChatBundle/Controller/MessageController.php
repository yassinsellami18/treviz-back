<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 11/07/2017
 * Time: 22:47
 */

namespace Treviz\ChatBundle\Controller;

use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\ChatBundle\Entity\Message;
use Treviz\ChatBundle\Entity\Room;
use Treviz\ChatBundle\Form\MessageType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class MessageController
 * @package Treviz\ChatBundle\Controller
 */
class MessageController extends FOSRestController
{

    /**
     * Fetches the messages of a chatroom, according to some query parameters.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of messages matching the query",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Get("/rooms/{hash}/messages")
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @Rest\QueryParam(name="limit", description="Number of messages to fetch. Defaults to 20", nullable=true)
     * @Rest\QueryParam(name="offset", description="Offset of the query. Defaults to 0", nullable=true)
     * @Rest\QueryParam(name="since", description="Timestamp representing the date from which retrieve messages", nullable=true)
     *
     * @param $hash
     * @param ParamFetcher $paramFetcher
     * @return mixed
     */
    public function getMessageAction($hash, ParamFetcher $paramFetcher) {

        $limit = $paramFetcher->get("limit") ? $paramFetcher->get("limit") : 20;
        $offset = $paramFetcher->get("offset") ? $paramFetcher->get("offset") : 0;
        $since = $paramFetcher->get("since") ? $paramFetcher->get("since") : 0;

        $repository = $this->getDoctrine()->getRepository('TrevizChatBundle:Message');

        /** @var Room $room */
        $room = $this->getDoctrine()->getRepository("TrevizChatBundle:Room")->findOneBy(array("hash" => $hash));

        if($room) {

            if ($room->getUsers()->contains($this->getUser())) {

                /** @var QueryBuilder $query */
                $qb = $repository->createQueryBuilder('message');
                $qb->where('message.room = :room');
                $qb->setParameter('room', $room);
                $qb->orderBy('message.posted', 'DESC');

                if($since) {
                    $qb->andWhere('message.posted > :date');
                    $qb->setParameter('date', $since);
                }

                $qb->setMaxResults($limit);
                $qb->setFirstResult($offset);

                return $this->view($qb->getQuery()->getResult(), 200);

            }

            throw new HttpException(403, "You are not authorized to see this room's messages");

        }

        throw new HttpException(404, "No room found for hash " . $hash);

    }

    /**
     * Adds a new message in a room.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a message was successfully posted",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Post("/rooms/{hash}/messages")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return mixed
     */
    public function postMessageAction(Request $request, $hash) {

        /** @var Room $room */
        $room = $this->getDoctrine()->getRepository("TrevizChatBundle:Room")->findOneBy(array("hash" => $hash));

        if ($room) {

            $message = new Message();
            $form = $this->createForm(MessageType::class, $message);
            $form->submit($request->request->all());

            if($form->isValid()) {

                $user = $this->getUser();

                if ($room->getUsers()->contains($user)) {

                    $em = $this->getDoctrine()->getManager();

                    $messageHash = md5($message->getText() . $message->getPosted()->getTimestamp() . $message->getHash());
                    while ($this->getDoctrine()->getRepository("TrevizChatBundle:Room")->findOneBy(array("hash" => $messageHash))) {
                        $messageHash = hash("sha256", random_bytes(256));
                    }

                    $message->setHash($messageHash);
                    $message->setOwner($this->getUser());

                    $message->setRoom($room);
                    $em->persist($message);
                    $em->flush();

                    return $this->view($message, 201);

                }

                throw new HttpException(403, "You are not authorized to see this room's messages");

            }

            throw new HttpException(400, $form);
        }

        throw new HttpException(404, "No chat room was found for hash " . $hash);

    }

    /**
     * Updates an existing message in a room.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a message was successfully updated",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Post("/rooms/messages/{hash}")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return mixed
     */
    public function putMessageAction(Request $request, $hash) {

        /** @var Message $message */
        $message = $this->getDoctrine()->getRepository("TrevizChatBundle:Message")->findOneBy(array("hash" => $hash));

        if ($message) {

            $form = $this->createForm(MessageType::class, $message);
            $form->submit($request->request->all());

            if($form->isValid()) {

                if ($message->getOwner() === $this->getUser()) {

                    $em = $this->getDoctrine()->getManager();

                    $em->flush();

                    return $this->view($message, 200);

                }

                throw new HttpException(403, "You are not authorized to update this message");

            }

            throw new HttpException(400, $form);
        }

        throw new HttpException(404, "No message was found for hash " . $hash);

    }

    /**
     * Deletes an existing message in a room.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a message was successfully deleted",
     * )
     *
     * @SWG\Tag(name="chat")
     *
     * @Rest\Delete("/rooms/messages/{hash}")
     *
     * @Rest\View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return mixed
     */
    public function deleteMessageAction($hash) {

        /** @var Message $message */
        $message = $this->getDoctrine()->getRepository("TrevizChatBundle:Message")->findOneBy(array("hash" => $hash));

        if ($message) {

            if ($message->getOwner() === $this->getUser()) {

                $em = $this->getDoctrine()->getManager();

                $em->remove($message);
                $em->flush();

                return $this->view($message, 201);

            }

            throw new HttpException(403, "You are not authorized to update this message");

        }

        throw new HttpException(404, "No message was found for hash " . $hash);

    }

}