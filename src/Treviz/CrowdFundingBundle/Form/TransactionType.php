<?php

namespace Treviz\CrowdFundingBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\CrowdFundingBundle\Form\DataTransformers\HashToCampaignTransformer;
use Treviz\CrowdFundingBundle\Form\DataTransformers\HashToTierTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransactionType extends AbstractType
{

    private $em;

    /**
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('description', TextType::class, array("required" => false))
            ->add('amount')
            ->add('from', TextType::class, array("required" => true))
            ->add('to', TextType::class, array("required" => true))
            ->add('transactionHash', TextType::class, array("required" => true))
            ->add('tier', TextType::class, array("required" => false))
            ->add('campaign', TextType::class, array("required" => false));

        $builder->get('tier')->addModelTransformer(new HashToTierTransformer($this->em), true);
        $builder->get('campaign')->addModelTransformer(new HashToCampaignTransformer($this->em), true);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\CrowdFundingBundle\Entity\Transaction',
            'csrf_protection' => false
        ));
    }

}
