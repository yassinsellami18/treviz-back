<?php

namespace Treviz\CrowdFundingBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\CrowdFundingBundle\Form\DataTransformers\ISO8601ToDateTimeTransformer;
use Treviz\ProjectBundle\Form\DataTransformer\HashToProjectTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CampaignType extends AbstractType
{

    /** @var  EntityManager */
    private $em;

    /**
     * CampaignType constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('description')
            ->add('objective')
            ->add('max')
            ->add('deadline', TextType::class, array("required" => true))
            ->add('project', TextType::class, array("required" => false));

        $builder->get('project')->addModelTransformer(new HashToProjectTransformer($this->em), true);
        $builder->get('deadline')->addModelTransformer(new ISO8601ToDateTimeTransformer(), true);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\CrowdFundingBundle\Entity\Campaign',
            'csrf_protection' => false
        ));
    }

}
