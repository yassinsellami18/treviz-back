<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 07/11/2017
 * Time: 21:54
 */

namespace Treviz\CrowdFundingBundle\Form\DataTransformers;

use Doctrine\ORM\EntityManager;
use Treviz\CrowdFundingBundle\Entity\Campaign;
use Treviz\CrowdFundingBundle\Entity\Tier;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class HashToTierTransformer implements DataTransformerInterface
{

    private $em;

    /**
     * HashToTierTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Tier $tier
     * @return string
     */
    public function transform($tier)
    {
        if($tier == null) {
            return '';
        }

        return $tier->getHash();
    }

    /**
     * @param string $hash
     * @return mixed|Tier|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $tier = $this->em->getRepository("TrevizCrowdFundingBundle:Tier")->findOneByHash($hash);

        if (!$tier) {
            return;
        }

        return $tier;

    }

}