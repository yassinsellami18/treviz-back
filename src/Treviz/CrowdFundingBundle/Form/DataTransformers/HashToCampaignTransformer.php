<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 07/11/2017
 * Time: 21:54
 */

namespace Treviz\CrowdFundingBundle\Form\DataTransformers;

use Doctrine\ORM\EntityManager;
use Treviz\CrowdFundingBundle\Entity\Campaign;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Validator\Constraints\DateTime;

class HashToCampaignTransformer implements DataTransformerInterface
{

    private $em;

    /**
     * HashToProjectTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Campaign $campaign
     * @return string
     */
    public function transform($campaign)
    {
        if($campaign == null) {
            return '';
        }

        return $campaign->getHash();
    }

    /**
     * @param string $hash
     * @return mixed|Campaign|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $campaign = $this->em->getRepository("TrevizCrowdFundingBundle:Campaign")->findOneByHash($hash);

        if (!$campaign) {
            throw new TransformationFailedException("No campaign was found for hash " . $hash);
        }

        return $campaign;

    }

}