<?php

namespace Treviz\CrowdFundingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Treviz\CrowdFundingBundle\Entity\Traits\Hashable;
use JMS\Serializer\Annotation as JMS;

/**
 * Tier
 *
 * @ORM\Table(name="tier")
 * @ORM\Entity(repositoryClass="Treviz\CrowdFundingBundle\Repository\TierRepository")
 */
class Tier
{

    use Hashable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var Campaign
     *
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="tiers")
     * @JMS\Exclude()
     */
    private $campaign;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Tier
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Tier
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Campaign
     */
    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     * @return Tier
     */
    public function setCampaign(Campaign $campaign): Tier
    {
        $this->campaign = $campaign;
        if (!$campaign->getTiers()->contains($this)) {
            $campaign->addTier($this);
        }

        return $this;
    }
}

