<?php

namespace Treviz\CrowdFundingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Treviz\CoreBundle\Entity\User;
use Treviz\CrowdFundingBundle\Entity\Traits\Hashable;
use JMS\Serializer\Annotation as JMS;
use Treviz\ProjectBundle\Entity\Project;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Campaign
 *
 * @ORM\Table(name="campaign")
 * @ORM\Entity(repositoryClass="Treviz\CrowdFundingBundle\Repository\CampaignRepository")
 */
class Campaign
{

    use Hashable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="objective", type="integer")
     */
    private $objective;

    /**
     * @var int
     *
     * @ORM\Column(name="max", type="integer")
     */
    private $max;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime")
     */
    private $deadline;

    /**
     * Ethereum address used for the crowdfunding
     *
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="campaign")
     * @JMS\Exclude()
     */
    private $transactions;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Tier", mappedBy="campaign", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $tiers;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Treviz\CoreBundle\Entity\User")
     * @JMS\Groups({"crowdfund"})
     */
    private $author;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Treviz\ProjectBundle\Entity\Project")
     * @JMS\Groups({"crowdfund"})
     */
    private $project;

    /**
     * Campaign constructor.
     */
    public function __construct()
    {
        $this->transactions = new ArrayCollection();
        $this->tiers = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Campaign
     */
    public function setName($name): Campaign
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Campaign
     */
    public function setDescription($description): Campaign
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set objective
     *
     * @param integer $objective
     *
     * @return Campaign
     */
    public function setObjective($objective): Campaign
    {
        $this->objective = $objective;

        return $this;
    }

    /**
     * Get objective
     *
     * @return int
     */
    public function getObjective(): ?int
    {
        return $this->objective;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return Campaign
     */
    public function setDeadline($deadline): Campaign
    {
        if ($deadline > new \DateTime()) {
            $this->deadline = $deadline;
            return $this;
        } else {
            throw new Exception("Deadline has already passed");
        }
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline(): ?\DateTime
    {
        return $this->deadline;
    }

    /**
     * @return int
     */
    public function getMax(): ?int
    {
        return $this->max;
    }

    /**
     * @param int $max
     * @return Campaign
     */
    public function setMax(int $max): Campaign
    {
        $this->max = $max;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Campaign
     */
    public function setAddress(string $address): Campaign
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<Transaction>
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    /**
     * You can only add a transaction if it does not make the collected amount exceed the maximum.
     *
     * @param Transaction $transaction
     * @return Campaign
     */
    public function addTransaction(Transaction $transaction): Campaign
    {
        if ($this->max == null
            || $this->getCollected() < $this->max) {
            $this->transactions->add($transaction);
        }

        return $this;
    }

    /**
     * @param Transaction $transaction
     * @return Campaign
     */
    public function removeTransaction(Transaction $transaction): Campaign
    {
        $this->transactions->removeElement($transaction);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getTiers(): Collection
    {
        return $this->tiers;
    }

    /**
     * @param Tier $tier
     * @return Campaign
     */
    public function addTier(Tier $tier): Campaign
    {
        $this->tiers->add($tier);

        return $this;
    }

    /**
     * @param Tier $tier
     * @return Campaign
     */
    public function removeTier(Tier $tier): Campaign
    {
        $this->tiers->removeElement($tier);

        return $this;
    }

    /**
     * @JMS\VirtualProperty(name="collected")
     */
    public function getCollected(): ?int
    {
        $amount = 0;
        if ($this->transactions !== null) {
            foreach ($this->transactions as $transaction) {
                $amount += $transaction->getAmount();
            }
        }

        return $amount;
    }

    /**
     * @return User
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @param User $author
     * @return Campaign
     */
    public function setAuthor(User $author): Campaign
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return Campaign|null
     */
    public function setProject(Project $project): ?Campaign
    {
        $this->project = $project;
        return $this;
    }

}

