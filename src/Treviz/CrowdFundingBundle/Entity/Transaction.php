<?php

namespace Treviz\CrowdFundingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Transaction
 *
 * @ORM\Table(name="treviz_transaction")
 * @ORM\Entity(repositoryClass="Treviz\CrowdFundingBundle\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * Address from which the transaction was made
     *
     * @var string
     *
     * @ORM\Column(name="from_address", type="string", length=255, nullable=false)
     */
    private $from;

    /**
     * Address to which the transaction was made
     *
     * @var string
     *
     * @ORM\Column(name="to_address", type="string", length=255, nullable=false)
     */
    private $to;

    /**
     * Hash of the transaction
     *
     * @var string
     *
     * @ORM\Column(name="transaction_hash", type="string", length=255, nullable=false, unique=true)
     */
    private $transactionHash;

    /**
     * @var Campaign
     *
     * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="transactions")
     * @JMS\Exclude()
     */
    private $campaign;

    /**
     * @var Tier
     *
     * @ORM\ManyToOne(targetEntity="Tier")
     * @JMS\Exclude()
     */
    private $tier;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Transaction
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getFrom(): ?string
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo(): ?string
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to)
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getTransactionHash(): ?string
    {
        return $this->transactionHash;
    }

    /**
     * @param string $transactionHash
     */
    public function setTransactionHash(string $transactionHash)
    {
        $this->transactionHash = $transactionHash;
    }

    /**
     * @return Campaign
     */
    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function setCampaign(Campaign $campaign)
    {
        $this->campaign = $campaign;
        if (!$campaign->getTransactions()->contains($this)) {
            $campaign->addTransaction($this);
        }
    }

    /**
     * @return Tier
     */
    public function getTier(): ?Tier
    {
        return $this->tier;
    }

    /**
     * @param Tier $tier
     * @return Transaction
     */
    public function setTier(Tier $tier): Transaction
    {
        $this->tier = $tier;

        return $this;
    }

}

