<?php

namespace Treviz\CrowdFundingBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 06/11/2017
 * Time: 12:06
 */
trait Hashable
{

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, unique=true)
     */
    protected $hash;

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     * @return $this
     */
    public function setHash(string $hash)
    {
        $this->hash = $hash;

        return $this;
    }

}