<?php

namespace Treviz\CrowdFundingBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CoreBundle\Entity\User;
use Treviz\CrowdFundingBundle\Entity\Campaign;
use Treviz\CrowdFundingBundle\Entity\Transaction;
use Treviz\CrowdFundingBundle\Form\TransactionType;
use Treviz\ProjectBundle\Entity\Project;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\QueryParam;


/**
 * Campaign controller.
 *
 * @Security("has_role('ROLE_USER')")
 */
class TransactionController extends FOSRestController
{

    /**
     * Retrieves transactions according to query parameters.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of transactions matching the query",
     * )
     *
     * @SWG\Tag(name="crowdfunding")
     *
     * @QueryParam(name="user", description="username of the user who made the transactions")
     * @QueryParam(name="project", description="hash of the campaigns from which the transactions are made")
     * @QueryParam(name="campaign", description="hash of the campaigns from which the transactions are made")
     * @QueryParam(name="from", description="hash of the campaigns from which the transactions are made")
     * @QueryParam(name="to", description="hash of the campaigns from which the transactions are made")
     * @QueryParam(name="hash", description="part of the hash of the transaction")
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getTransactionsAction(ParamFetcher $paramFetcher)
    {
        $qb = $this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Transaction")->createQueryBuilder("transaction");

        if ($username = $paramFetcher->get("user")) {
            /** @var User $user */
            $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneByUsername($username);
            if ($user !== null) {
                $qb->andWhere("transaction.from = :user
                            OR transaction.to = :user");
                $qb->setParameter("user", $user->getAddress());
            }
        }

        if ($projectHash = $paramFetcher->get("project")) {
            /** @var Project $project */
            $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")->findOneByHash($projectHash);
            if ($project !== null) {
                $qb->andWhere("transaction.from = :project
                            OR transaction.to = :project");
                $qb->setParameter("project", $project->getAddress());
            }
        }

        if ($campaign = $paramFetcher->get("campaign")) {
            $qb->join("transaction.campaign", "campaign");
            $qb->andWhere("campaign.hash = :campaign")
                ->setParameter("campaign", $campaign);
        }

        if ($from = $paramFetcher->get("from")) {
            $qb->andWhere("transaction.from = :from")
                ->setParameter("from", $from);
        }

        if ($to = $paramFetcher->get("to")) {
            $qb->andWhere("transaction.to = :to")
                ->setParameter("to", $to);
        }

        if ($hash = $paramFetcher->get("hash")) {
            $qb->andWhere("transaction.transactionHash LIKE :hash")
                ->setParameter("hash", '%'.$hash.'%');
        }

        return $this->view($qb->getQuery()->getResult(), 200);

    }

    /**
     * Retrieves a specific transaction tiers.
     *
     * @SWG\Response(
     *     response=200,
     *     description="The transaction is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The transaction does not exist",
     * )
     *
     * @SWG\Tag(name="crowdfunding")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getTransactionAction($hash)
    {
        /** @var Campaign $campaign */
        $transaction = $this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Transaction")->findOneByHash($hash);

        if ($transaction) {
            return $this->view($transaction, 200);
        }

        return $this->view("No transaction was found for hash " . $hash, 404);
    }

    /**
     * Creates a new transaction.
     *
     * @SWG\Response(
     *     response=201,
     *     description="The transaction tier is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="A transaction already exists with the hash",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="The input could not be processed due to incorrect parameters",
     * )
     *
     * @SWG\Tag(name="crowdfunding")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postTransactionAction(Request $request)
    {

        $transaction = new Transaction();
        $form = $this->createForm(TransactionType::class, $transaction);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            /** @var User $user */
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();

            if ($transaction->getFrom() == $user->getAddress()) {

                if ($transaction->getTier() !== null
                && $transaction->getAmount() < $transaction->getTier()->getAmount()) {
                    return $this->view("You did not pledge enough to hit this tier", 400);
                }

                if ($transaction->getCampaign() !== null
                    && $transaction->getTo() !== $transaction->getCampaign()->getAddress()
                ) {
                    return $this->view("The recipient of the transaction does not match the campaign", 400);
                }

                $em->persist($transaction);
                $em->flush();
                return $this->view($transaction, 201);

            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view($form, 422);

    }

}
