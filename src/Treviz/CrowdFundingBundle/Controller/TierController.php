<?php

namespace Treviz\CrowdFundingBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Treviz\CrowdFundingBundle\Entity\Campaign;
use Treviz\CrowdFundingBundle\Entity\Tier;
use Treviz\CrowdFundingBundle\Form\TierType;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Delete;


/**
 * Campaign controller.
 *
 * @Security("has_role('ROLE_USER')")
 */
class TierController extends FOSRestController
{

    /**
     * Retrieves a specific crowdfuning campaign tiers.
     *
     * @SWG\Response(
     *     response=200,
     *     description="The campaign tiers is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user has no rights to fetch this campaign tiers",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The campaign does not exist",
     * )
     *
     * @SWG\Tag(name="crowdfunding")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getCampaignTiersAction($hash)
    {
        /** @var Campaign $campaign */
        $campaign = $this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Campaign")->findOneByHash($hash);

        if ($campaign) {
            return $this->view($campaign->getTiers(), 200);
        }

        return $this->view("No campaign was found for hash " . $hash, 404);
    }

    /**
     * Creates a new campaign tier.
     * A campaign can either be created for a project, or in order to fund the organization.
     * In the latter case, the user must be an admin of the organization.
     *
     * @SWG\Response(
     *     response=201,
     *     description="The campaign tier is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user has no rights to create tiers for this campaign",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No campaign was found",
     * )
     *
     *
     * @SWG\Response(
     *     response=422,
     *     description="The input could not be processed due to incorrect parameters",
     * )
     *
     * @SWG\Tag(name="crowdfunding")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postCampaignTierAction(Request $request, $hash)
    {

        /** @var Campaign $campaign */
        $campaign = $this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Campaign")->findOneByHash($hash);

        if ($campaign !== null) {
            $tier = new Tier();
            $form = $this->createForm(TierType::class, $tier);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                $user = $this->getUser();
                $em = $this->getDoctrine()->getManager();
                $tier->setCampaign($campaign);

                if ($campaign->getProject() !== null) {

                    $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                        ->findOneBy(array(
                            "project" => $campaign->getProject(),
                            "user" => $user
                        ));

                    if($membership !== null
                        && in_array(ProjectPermissions::MANAGE_CROWD_FUND, $membership->getRole()->getPermissions())) {

                        $hash = hash("sha256", random_bytes(256));
                        while($this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Tier")->findOneByHash($hash) !== null){
                            $hash = hash("sha256", random_bytes(256));
                        }
                        $tier->setHash($hash);

                        $em->persist($tier);
                        $em->flush();
                        return $this->view($tier, 201);

                    }

                } elseif ($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {

                    $em->persist($tier);
                    $em->flush();
                    return $this->view($campaign, 201);
                }

                return $this->view("You are not authorized to do this", 403);

            }

            return $this->view($form, 422);

        }

        return $this->view("No campaign was found for hash " . $hash, 404);

    }

    /**
     *
     * @SWG\Response(
     *     response=204,
     *     description="The tier is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user has no rights to delete this tier",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The tier could not be fetched",
     * )
     *
     * @SWG\Tag(name="crowdfunding")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Delete("/campaigns/tiers/{hash}")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteCampaignTierAction($hash)
    {

        /** @var Tier $tier */
        $tier = $this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Tier")->findOneByHash($hash);

        if ($tier != null) {
            $project = $tier->getCampaign()->getProject();
            if ($project !== null) {
                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $project,
                        "user" => $this->getUser()
                    ));

                if ($membership !== null
                    && in_array(ProjectPermissions::MANAGE_CROWD_FUND, $membership->getRole()->getPermissions())
                ) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($tier);
                    $em->flush();
                    return $this->view(null, 204);
                }
            } elseif($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($tier);
                $em->flush();
                return $this->view(null, 204);
            }

            return $this->view("You do not have the rights to do this", 403);

        }

        return $this->view("No campaign was found for hash " . $hash, 404);

    }

}
