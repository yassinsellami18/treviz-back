<?php

namespace Treviz\CrowdFundingBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CrowdFundingBundle\Entity\Campaign;
use Treviz\CrowdFundingBundle\Form\CampaignType;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * Campaign controller.
 *
 * All campaigns are public.
 *
 * @Security("has_role('ROLE_USER')")
 */
class CampaignController extends FOSRestController
{

    /**
     * Lists all campaigns matching the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns a list of crowdfunding campaigns",
     * )
     *
     * @SWG\Tag(name="crowdfunding")
     *
     * @QueryParam(name="project", description="hash of the project from which the campaigns must be fetched")
     * @QueryParam(name="active", description="The campaign is still live. Defaults to 1.")
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getCampaignsAction(ParamFetcher $paramFetcher) {

        $qb = $this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Campaign")->createQueryBuilder("campaign");

        if ($paramFetcher->get("project")) {
            $qb->join("campaign.project", "project");
            $qb->andWhere("project.hash = :hash");
            $qb->setParameter("hash", $paramFetcher->get("project"));
        }

        $active = $paramFetcher->get('active');

        if ($active == null || $active) {
            $qb->andWhere("campaign.deadline > CURRENT_DATE()");
        } else {
            $qb->andWhere("campaign.deadline < CURRENT_DATE()");
        }

        return $this->view($qb->getQuery()->getResult(), 200);

    }

    /**
     * Retrieves a specific crowdfuning campaign.
     *
     * @SWG\Response(
     *     response=200,
     *     description="The campaign is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user has no rights to fetch this campaign",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The campaign does not exist",
     * )
     *
     * @SWG\Tag(name="crowdfunding")
     *
     * @View(serializerGroups={"Default", "crowdfund"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getCampaignAction($hash)
    {
        $campaign = $this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Campaign")->findOneByHash($hash);

        if ($campaign) {
            return $this->view($campaign, 200);
        }

        return $this->view("No campaign was found for hash " . $hash, 404);
    }

    /**
     * Creates a new campaign.
     * A campaign can either be created for a project, or in order to fund the organization.
     * In the latter case, the user must be an admin of the organization.
     *
     * @SWG\Response(
     *     response=201,
     *     description="The campaign is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user has no rights to create this campaign",
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="A campaign has already be launched.",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="The input could not be processed due to incorrect parameters",
     * )
     *
     * @SWG\Tag(name="crowdfunding")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @View(serializerGroups={"Default", "crowdfund"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postCampaignAction(Request $request)
    {

        $campaign = new Campaign();
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();

            $hash = hash("sha256", random_bytes(256));
            while($this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Campaign")->findOneByHash($hash) !== null){
                $hash = hash("sha256", random_bytes(256));
            }
            $campaign->setHash($hash);

            if ($campaign->getProject() !== null) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $campaign->getProject(),
                        "user" => $user
                    ));

                if($membership !== null
                    && in_array(ProjectPermissions::MANAGE_CROWD_FUND, $membership->getRole()->getPermissions())) {

                    /*
                     * Checks if no brainstorming campaign already exist for this project.
                     */
                    $campaigns = $this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Campaign")
                        ->findBy(array(
                            "project" => $campaign->getProject()
                        ));
                    if ($campaigns !== null) {
                        foreach ($campaigns as $campaign) {
                            if ($campaign->getDeadline() > new \DateTime()) {
                                return $this->view("An active campaign already exists for this project", 409);
                            }
                        }
                    }

                    /**
                     * TODO: generate the address of the campaign with an ethereum client so that it is valid.
                     */
                    $campaign->setAddress($hash);

                    $em->persist($campaign);
                    $em->flush();
                    return $this->view($campaign, 201);
                }

            } elseif ($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {

                /*
                 * Checks if no brainstorming campaign already exist for this project.
                 */
                $campaigns = $this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Campaign")->createQueryBuilder("c")
                    ->andWhere("c.project IS NULL")->getQuery()->getResult();
                if ($campaigns !== null) {
                    foreach ($campaigns as $campaign) {
                        if ($campaign->getDeadline() > new \DateTime()) {
                            return $this->view("An active campaign already exists for this project", 409);
                        }
                    }
                }

                $campaign->setAuthor($user);
                $em->persist($campaign);
                $em->flush();
                return $this->view($campaign, 201);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view($form, 422);

    }

    /**
     *
     * @SWG\Response(
     *     response=204,
     *     description="The campaign is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user has no rights to delete this campaign",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The campaign could not be fetched",
     * )
     *
     * @SWG\Tag(name="crowdfunding")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteCampaignAction($hash)
    {

        /** @var Campaign $campaign */
        $campaign = $this->getDoctrine()->getRepository("TrevizCrowdFundingBundle:Campaign")->findOneByHash($hash);

        if ($campaign != null) {
            $project = $campaign->getProject();
            if ($project !== null) {
                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $project,
                        "user" => $this->getUser()
                    ));

                if ($membership !== null
                    && in_array(ProjectPermissions::MANAGE_CROWD_FUND, $membership->getRole()->getPermissions())
                ) {
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($campaign);
                    $em->flush();
                    return $this->view(null, 204);
                }
            } elseif($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($campaign);
                $em->flush();
                return $this->view(null, 204);
            }

            return $this->view("You do not have the rights to do this", 403);

        }

        return $this->view("No campaign was found for hash " . $hash, 404);

    }

}
