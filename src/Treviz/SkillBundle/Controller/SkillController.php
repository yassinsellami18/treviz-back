<?php

namespace Treviz\SkillBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\SkillBundle\Entity\Skill;
use Treviz\SkillBundle\Form\SkillType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class SkillController
 * @package Treviz\SkillBundle\Controller
 *
 * @Security("has_role('ROLE_USER')")
 */
class SkillController extends FOSRestController
{
    /**
     * Fetches all existing skills, or the ones matching the query string.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of skills, matching the query string",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Skill::class)
     *     )
     * )
     *
     * @SWG\Tag(name="skills")
     *
     * @QueryParam(name="name", description="Name, or part of the name of the skills to fetch", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSkillsAction(ParamFetcher $paramFetcher){

        $name = $paramFetcher->get("name");

        $qb = $this->getDoctrine()->getRepository("TrevizSkillBundle:Skill")->createQueryBuilder("t");

        if ($name !== null) {

            $qb->andWhere("t.name LIKE :name")
                ->setParameter("name", "%" . $name . "%");

        }

        $skills = $qb->getQuery()->getResult();

        $view = $this->view($skills, 200);
        return $this->handleView($view);
    }

    /**
     * Creates a new skill
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created skill",
     *     @Model(type=Skill::class)
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="Skill already exists"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid Data"
     * )
     *
     * @SWG\Parameter(
     *     name="skill",
     *     in="body",
     *     description="Skill to create",
     *     required=true,
     *     @Model(type=Skill::class)
     * )
     *
     * @SWG\Tag(name="skills")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postSkillsAction(Request $request){

        $skill = new Skill();
        $form = $this->createForm(SkillType::class, $skill);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            if(!$this->getDoctrine()->getRepository("TrevizSkillBundle:Skill")->findOneBy(array("name" => $skill->getName()))){

                $em = $this->getDoctrine()->getManager();
                $em->persist($skill);
                $em->flush();

                $view = $this->view($skill, 201);
                return $this->handleView($view);

            }

            $view = $this->view("Skill with same name already exists", 409);
            return $this->handleView($view);

        }

        $view = $this->view("Invalid data", 422);
        return $this->handleView($view);

    }

    /**
     * Updates an existing skill
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated skill",
     *     @Model(type=Skill::class)
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Skill does not exist"
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="Skill already exists"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid Data"
     * )
     *
     * @SWG\Parameter(
     *     name="skill",
     *     in="body",
     *     description="Updated Skill",
     *     required=true,
     *     @Model(type=Skill::class)
     * )
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="path",
     *     type="string",
     *     description="Name of the skill to update"
     * )
     *
     * @SWG\Tag(name="skills")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putSkillAction(Request $request, $name)
    {

        $skill = $this->getDoctrine()->getRepository("TrevizSkillBundle:Skill")->findOneBy(array("name" => $name));

        if ($skill !== null) {
            $form = $this->createForm(SkillType::class, $skill);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                if(!$this->getDoctrine()->getRepository("TrevizSkillBundle:Skill")->findOneBy(array("name" => $skill->getName()))){

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    $view = $this->view($skill, 201);
                    return $this->handleView($view);

                }

                $view = $this->view("Skill with same name already exists", 409);
                return $this->handleView($view);

            }

            $view = $this->view("Invalid data", 422);
            return $this->handleView($view);
        }

        $view = $this->view("Skill does not exists with name " .$name, 404);
        return $this->handleView($view);

    }

    /**
     * Deletes a skill.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the skill was correctly deleted"
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Skill does not exist"
     * )
     *
     * @SWG\Parameter(
     *     name="name",
     *     in="path",
     *     type="string",
     *     description="Name of the skill to delete"
     * )
     *
     * @SWG\Tag(name="skills")
     *
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteSkillAction($name)
    {

        $skill = $this->getDoctrine()->getRepository("TrevizSkillBundle:Skill")->findOneBy(array("name" => $name));

        if ($skill !== null) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($skill);
            $em->flush();

            $view = $this->view(null, 204);
            return $this->handleView($view);
        }

        $view = $this->view("Skill does not exists with name " .$name, 404);
        return $this->handleView($view);

    }

}
