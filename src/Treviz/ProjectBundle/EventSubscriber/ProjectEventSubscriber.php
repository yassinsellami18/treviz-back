<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 23:27
 */

namespace Treviz\ProjectBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\ProjectBundle\Events\ProjectCandidacyCreatedEvent;
use Treviz\ProjectBundle\Events\ProjectCreatedEvent;
use Treviz\CoreBundle\Services\MailerService;

/**
 * Class BrainstormingEventSubscriber
 *
 * Listens to project creations and update, to trigger the appropriate
 * notifications to be sent.
 *
 * @package Treviz\CommunityBundle\EventSubscriber
 */
class ProjectEventSubscriber implements EventSubscriberInterface
{

    private $mailerService;
    private $frontendUrl;

    public function __construct(MailerService $service, string $frontUrl)
    {
        $this->mailerService = $service;
        $this->frontendUrl = $frontUrl;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            ProjectCreatedEvent::NAME      => 'onProjectCreated',
            ProjectCandidacyCreatedEvent::NAME => 'onProjectCandidacyCreated'
        );
    }

    public function onProjectCreated(ProjectCreatedEvent $event)
    {
        $project = $event->getProject();
        $communities = $project->getCommunities();
        if(sizeof($communities->toArray()) > 0) {
            foreach ($communities as $community) {
                $message = "Your community " . $community->getName() . " hosts a new project: " . $project->getName();
                $url = $this->frontendUrl . "/projects/" . $project->getHash();
                /** @var CommunityMembership $membership */
                foreach ($community->getMemberships() as $membership) {
                    if ($membership->getPreferences()->isOnNewProject()) {
                        $this->mailerService->sendUpdateMail(
                            $membership->getUser(),
                            $message,
                            $url
                        );
                    }
                }
            }
        }
    }

    public function onProjectCandidacyCreated(ProjectCandidacyCreatedEvent $event)
    {
        $candidacy = $event->getCandidacy();
        $project = $candidacy->getProject();
        $message = $candidacy->getUser()->getUsername() . " just candidated to your project " . $project->getName();
        $url = $this->frontendUrl . "/communities/" . $project->getHash();
        /** @var CommunityMembership $membership */
        foreach ($project->getMemberships() as $membership) {
            if ($membership->getPreferences()->isOnCandidacy()) {
                $this->mailerService->sendUpdateMail(
                    $membership->getUser(),
                    $message,
                    $url
                );
            }
        }
    }
}