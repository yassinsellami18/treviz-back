<?php

namespace Treviz\ProjectBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\ProjectBundle\Form\DataTransformer\HashToJobTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectCandidacyType extends AbstractType
{

    private $em;

    /**
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message')
                ->add('job', TextType::class, array('required' => false));

        $builder->get('job')->addModelTransformer(new HashToJobTransformer($this->em));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\ProjectBundle\Entity\ProjectCandidacy',
            'csrf_protection' => false
        ));
    }

}
