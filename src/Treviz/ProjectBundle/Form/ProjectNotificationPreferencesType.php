<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 22:58
 */

namespace Treviz\ProjectBundle\Form;


use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectNotificationPreferencesType extends AbstractType
{

    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('onNewTask')
            ->add('onNewDocument')
            ->add('onAssignedTask')
            ->add('onCandidacy')
            ->add('onFork')
            ->add('onPost')
            ->add('onTaskApprovalOrRefusal')
            ->add('onTaskSubmission');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\ProjectBundle\Entity\ProjectNotificationPreferences',
            'csrf_protection' => false
        ));
    }


}