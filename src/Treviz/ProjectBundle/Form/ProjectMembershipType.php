<?php

namespace Treviz\ProjectBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\ProjectBundle\Form\DataTransformer\HashToRoleTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectMembershipType extends AbstractType
{

    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('role', TextType::class, array("required" => true));

        $builder->get('role')->addModelTransformer(new HashToRoleTransformer($this->em));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\ProjectBundle\Entity\ProjectMembership',
            'csrf_protection' => false
        ));
    }

}
