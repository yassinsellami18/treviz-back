<?php

namespace Treviz\ProjectBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\ProjectBundle\Entity\ProjectRole;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToRoleTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param ProjectRole $role
     * @return string
     */
    public function transform($role)
    {
        if($role == null) {
            return '';
        }

        return $role->getHash();
    }

    /**
     * @param string $hash
     * @return ProjectRole|null
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $role = $this->em->getRepository("TrevizProjectBundle:ProjectRole")->findOneBy(array("hash" => $hash));

        if ($role == null) {
            throw new TransformationFailedException(sprintf('No role with hash ' . $hash . ' exists.'));
        }

        return $role;

    }
}