<?php

namespace Treviz\ProjectBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\CoreBundle\Entity\User;
use Treviz\ProjectBundle\Entity\Project;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToProjectTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * HashToProjectTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Project $project
     * @return string
     */
    public function transform($project)
    {
        if($project == null) {
            return '';
        }

        return $project->getHash();
    }

    /**
     * @param string $hash
     * @return mixed|Project|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $project = $this->em->getRepository("TrevizProjectBundle:Project")->findOneByHash($hash);

        if ($project == null) {
            throw new TransformationFailedException(sprintf('No project with hash ' . $hash . ' exists.'));
        }

        return $project;

    }

}