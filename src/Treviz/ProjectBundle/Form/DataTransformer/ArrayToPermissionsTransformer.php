<?php

namespace Treviz\ProjectBundle\Form\DataTransformer;

use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class ArrayToPermissionsTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $permissions
     * @return string
     */
    public function transform($permissions)
    {
        if($permissions == null) {
            return '';
        }

        return $permissions;
    }

    /**
     * Checks if all the permissions given are actual permissions.
     *
     * @param array $permissions
     * @return array|null
     */
    public function reverseTransform($permissions)
    {
        if (!$permissions) {
            return;
        }

        $actualPermissions = [];
        foreach ($permissions as $permission) {
            if (in_array($permission, ProjectPermissions::getAvailablePermissions())) {
                $actualPermissions[] = $permission;
            } else {
                throw new Exception($permission . "is not an actual permission");
            }
        }

        return $permissions;

    }
}