<?php

namespace Treviz\ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectRoleType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array("required" => true))
            ->add('permissions', CollectionType::class, array(
                'allow_add' => true,
                'required' => true,
                'entry_type' => TextType::class
            ))
            ->add('defaultCreator')
            ->add('defaultMember');;

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\ProjectBundle\Entity\ProjectRole',
            'csrf_protection' => false
        ));
    }
}
