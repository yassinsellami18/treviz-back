<?php

namespace Treviz\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Treviz\CoreBundle\Entity\Superclass\Hashable;
use Treviz\CoreBundle\Entity\User;

/**
 * ProjectCandidacy
 *
 * @ORM\Table(name="project_candidacy")
 * @ORM\Entity(repositoryClass="Treviz\ProjectBundle\Repository\ProjectCandidacyRepository")
 */
class ProjectCandidacy extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Treviz\ProjectBundle\Entity\Project", inversedBy="candidacies", cascade={"persist"})
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"user"})
     */
    private $project;

    /**
     * @var ProjectJob
     *
     * @ORM\ManyToOne(targetEntity="ProjectJob", inversedBy="applications")
     */
    private $job;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Treviz\CoreBundle\Entity\User", inversedBy="projectsCandidacies", cascade={"persist"})
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"project"})
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return ProjectJob
     */
    public function getJob(): ?ProjectJob
    {
        return $this->job;
    }

    /**
     * @param ProjectJob $job
     */
    public function setJob(ProjectJob $job)
    {
        $this->job = $job;
    }

}

