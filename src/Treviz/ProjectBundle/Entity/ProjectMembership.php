<?php

namespace Treviz\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Treviz\CoreBundle\Entity\Superclass\Hashable;
use Treviz\CoreBundle\Entity\User;

/**
 * ProjectMember
 *
 * @ORM\Table(name="project_membership")
 * @ORM\Entity(repositoryClass="Treviz\ProjectBundle\Repository\ProjectMemberRepository")
 */
class ProjectMembership extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var ProjectRole $role
     *
     * @ORM\ManyToOne(targetEntity="Treviz\ProjectBundle\Entity\ProjectRole", cascade={"persist"})
     */
    private $role;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Treviz\ProjectBundle\Entity\Project", inversedBy="memberships", cascade={"persist"})
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"user"})
     */
    private $project;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Treviz\CoreBundle\Entity\User", inversedBy="projectsMemberships", cascade={"persist"})
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"project"})
     */
    private $user;

    /**
     * @var ProjectNotificationPreferences
     *
     * @ORM\OneToOne(targetEntity="Treviz\ProjectBundle\Entity\ProjectNotificationPreferences", cascade={"persist"})
     * @JMS\Exclude()
     */
    private $preferences;

    public function __construct()
    {
        $this->preferences = new ProjectNotificationPreferences();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ProjectRole
     */
    public function getRole(): ?ProjectRole
    {
        return $this->role;
    }

    /**
     * @param ProjectRole $role
     */
    public function setRole(ProjectRole $role)
    {
        $this->role = $role;
    }

    /**
     * @return Project
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return ProjectNotificationPreferences
     */
    public function getPreferences(): ProjectNotificationPreferences
    {
        return $this->preferences;
    }

    /**
     * @param ProjectNotificationPreferences $preferences
     */
    public function setPreferences(ProjectNotificationPreferences $preferences
    ): void {
        $this->preferences = $preferences;
    }

}

