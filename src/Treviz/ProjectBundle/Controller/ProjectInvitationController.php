<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 30/10/2017
 * Time: 15:25
 */

namespace Treviz\ProjectBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CoreBundle\Entity\User;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Treviz\ProjectBundle\Entity\ProjectInvitation;
use Treviz\ProjectBundle\Entity\ProjectMembership;
use Treviz\ProjectBundle\Entity\ProjectRole;
use Treviz\ProjectBundle\Form\ProjectInvitationType;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;


/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class ProjectInvitationController
 * @package Treviz\ProjectBundle\Controller
 */
class ProjectInvitationController extends FOSRestController
{

    /**
     * Fetches the invitations of a project
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project from which the invitations should be fetched"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the invitations of a specified project",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectInvitation::class, groups={"Default", "project"})
     *     )
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to fetch invitations for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Project does not exist",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectInvitationsAction($hash)
    {
        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")
            ->findOneBy(array("hash" => $hash));

        if ($project) {
            $user = $this->getUser();

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $user
                ));

            if($membership != null
                && in_array(ProjectPermissions::MANAGE_INVITATIONS, $membership->getRole()->getPermissions())) {
                return $this->view($project->getInvitations(), 200);
            }

            $invitation = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectInvitation")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $user
                ));

            if ($invitation != null) {
                return $this->view([$invitation], 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No project was found with hash " . $hash, 404);

    }

    /**
     * Fetches the projects of a user specified in the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the invitations specified by the query",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectInvitation::class, groups={"Default", "user"})
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @QueryParam(name="user", description="username of the user from whom fetch invitations", nullable=false)
     *
     * @View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectsInvitationsAction(ParamFetcher $paramFetcher)
    {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")
            ->findOneBy(array("username" => $paramFetcher->get("user")));

        if ($user ==! null) {

            $currentUser = $this->getUser();

            if ($user == $currentUser) {
                $invitations = $user->getProjectsInvitations();
            } else {
                $repository = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectInvitation");
                $qb = $repository->createQueryBuilder("pc");

                $qb->andWhere("pc.user = :user");
                $qb->setParameter("user", $user);

                $qb->andWhere("EXISTS (
                                SELECT pm
                                FROM TrevizProjectBundle:ProjectMembership pm
                                JOIN pm.role r
                                WHERE pm.project = pc.project
                                AND pm.user = :current_user
                                AND r.permissions LIKE :read_invitations
                              )");
                $qb->setParameter("current_user", $currentUser);
                $qb->setParameter("read_invitations", '%'.ProjectPermissions::MANAGE_INVITATIONS.'%');

                $invitations = $qb->getQuery()->getResult();

            }

            return $this->view($invitations, 200);
        }

        $view = $this->view(null, 200);
        return $view;

    }

    /**
     * Creates a new invitation for a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project for which the invitation should be created"
     * )
     *
     * @SWG\Parameter(
     *     name="invitation",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="user",
     *             type="string",
     *             description="Name of the user to invite"
     *         ),
     *         @SWG\Property(
     *             property="message",
     *             type="string",
     *             description="Message to justify the invitation"
     *         )
     *     ),
     *     required=true,
     *     description="Invitation that should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created invitation",
     *     @Model(type=ProjectInvitation::class, groups={"Default"})
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to create invitations for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Project does not exist",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data was submitted",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postProjectInvitationAction(Request $request, $hash)
    {
        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")
            ->findOneBy(array("hash" => $hash));

        if ($project) {
            $user = $this->getUser();

            /** @var  $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $user
                ));

            if($membership !== null
                && in_array(ProjectPermissions::MANAGE_INVITATIONS, $membership->getRole()->getPermissions())) {

                $invitation = new ProjectInvitation();
                $form = $this->createForm(ProjectInvitationType::class, $invitation);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $invitation->setProject($project);

                    $invitationHash = hash("sha256", random_bytes(256));

                    while ($this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectInvitation")
                            ->findOneBy(array("hash" => $invitationHash)) != null) {
                        $invitationHash = hash("sha256", random_bytes(256));
                    }

                    $invitation->setHash($invitationHash);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($invitation);
                    $em->flush();

                    return $this->view($invitation, 200);

                }

                return $this->view($form, 422);

            }

            return $this->view("You cannot candidate to a project you are part of, candidate or invited to.", 403);

        }

        return $this->view("No project was found with hash " . $hash, 404);
    }

    /**
     * Removes an invitation from a project.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the invitation is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to delete invitations for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Invitation does not exist",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Delete("/projects/invitations/{hash}")
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteProjectInvitationAction($hash)
    {
        $invitation = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectInvitation")
            ->findOneBy(array("hash" => $hash));

        if ($invitation) {
            $user = $this->getUser();

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $invitation->getProject(),
                    "user" => $user
                ));

            if($invitation->getUser() == $user
                || ($membership ==! null
                    && in_array(ProjectPermissions::MANAGE_INVITATIONS, $membership->getRole()->getPermissions()))){

                $em = $this->getDoctrine()->getManager();
                $em->remove($invitation);
                $em->flush();
                return $this->view(null, 204);

            }

            return $this->view("You do not have the rights to do this", 403);

        }

        return $this->view("No invitation was found with hash " . $hash, 404);
    }

    /**
     * Accept a invitation for a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the invitation to accept"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The invitation is successfully accepted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to accept the invitation",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The invitation could not be fetched",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Post("/projects/invitations/{hash}/accept")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function acceptProjectInvitationAction($hash)
    {
        /** @var ProjectInvitation $invitation */
        $invitation = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectInvitation")
            ->findOneByHash($hash);

        $currentUser = $this->getUser();

        if ($invitation !== null
            && $invitation->getUser() == $currentUser
        ) {

            $project = $invitation->getProject();

            $currentUserMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $currentUser
                ));

            if ($currentUserMembership == null) {

                $membership = new ProjectMembership();

                /*
                 * Create the hash for this membership.
                 */
                $membershipHash = hash("sha256", random_bytes(256));

                while ($this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                        ->findOneBy(array("hash" => $membershipHash)) !== null) {
                    $membershipHash = hash("sha256", random_bytes(256));
                }

                /*
                 * Fetches the default member role.
                 * If there is none, create some.
                 */
                $defaultMemberRole = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
                    ->findOneBy(array("defaultMember" => true, "project" => $project));

                if ($defaultMemberRole == null) {
                    $defaultMemberRole = new ProjectRole();
                    $defaultMemberRole->setName("Member");
                    $defaultMemberRole->setDefaultMember(true);
                    $defaultMemberRole->setHash(hash("sha256", random_bytes(256)));
                    $project->addRole($defaultMemberRole);
                }

                $membership->setHash($membershipHash);
                $membership->setProject($project);
                $membership->setRole($defaultMemberRole);
                $membership->setUser($invitation->getUser());

                $em = $this->getDoctrine()->getManager();
                $em->remove($invitation);
                $em->persist($membership);
                $em->flush();

                return $this->view("Invitation accepted", 200);

            }

            return $this->view("You are already member of the project", 409);

        }

        return $this->view("You cannot perform this action", 403);

    }

}