<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 29/10/2017
 * Time: 11:30
 */

namespace Treviz\ProjectBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\FOSRestController;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CoreBundle\Entity\User;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Treviz\ProjectBundle\Entity\ProjectJob;
use Treviz\ProjectBundle\Entity\ProjectMembership;
use Treviz\ProjectBundle\Entity\ProjectRole;
use Treviz\ProjectBundle\Form\ProjectJobType;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;


/**
 * Class ProjectJobController
 * @package Treviz\ProjectBundle\Controller
 *
 * @Security("has_role('ROLE_USER')")
 */
class ProjectJobController extends FOSRestController
{

    /**
     * Fetches the jobs of a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project from which the jobs should be fetched"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the jobs of a specified project",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectJob::class, groups={"Default"})
     *     )
     * ),
     *
     * @SWG\Response(
     *     response=404,
     *     description="No project was found for the specified hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to fetch the jobs of this project",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @View(serializerGroups={"Default"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectJobsAction($hash){

        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")->findOneBy(array("hash" => $hash));

        if ($project) {

            /*
             * If the project is public, return its jobs.
             * Otherwise, check if the user has access to it.
             */

            if($project->isPublic()) {
                $view = $this->view($project->getJobs(), 200);
                return $view;
            } else {

                /** @var User $user */
                $user = $this->getUser();

                /*
                 * Firstly, we check if the user is a member of the project.
                 * Otherwise, we check if they share at least one community.
                 */
                if(!array_intersect($project->getMemberships()->toArray(), $user->getProjectsMemberships()->toArray())) {
                    $communities = [];
                    /** @var CommunityMembership $communitiesMembership */
                    foreach ($user->getCommunitiesMemberships() as $communitiesMembership) {
                        $communities[] = $communitiesMembership->getCommunity();
                    }

                    if(!array_intersect($communities, $project->getCommunities()->toArray())){
                        $view = $this->view("You can't see the jobs of this project", 403);
                        return $view;
                    };
                }

                $view = $this->view($project->getJobs(), 200);
                return $view;

            }

        }

        $view = $this->view("No project was found for this hash", 400);
        return $view;
    }

    /**
     * Fetches the jobs as specified by the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the jobs specified by the query",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectJob::class, groups={"Default", "jobs"})
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @QueryParam(name="holder", description="username of the user who currently has the job", nullable=true)
     * @QueryParam(name="candidate", description="username of a user who is candidate", nullable=true)
     * @QueryParam(name="tags", description="tags the job must have", nullable=true)
     * @QueryParam(name="skills", description="skills required for the job", nullable=true)
     * @QueryParam(name="project", description="project the job must be linked to", nullable=true)
     * @QueryParam(name="min_reward", description="minimum amount of the monthly reward", nullable=true)
     * @QueryParam(name="max_reward", description="maximum amount of the monthly reward", nullable=true)
     * @QueryParam(name="task", description="hash of the task the job must be linked to", nullable=true)
     * @QueryParam(name="attributed", description="Is the job attributed or not. Defaults to 0.", nullable=true)
     * @QueryParam(name="name", description="name of the job", nullable=true)
     * @QueryParam(name="nb", description="Number of results to fetch", nullable=true)
     * @QueryParam(name="offset", description="Pagination", nullable=true)
     *
     * @View(serializerGroups={"Default", "jobs"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectsJobsAction(ParamFetcher $paramFetcher){

        $qb = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectJob")->createQueryBuilder("job");
        $qb->join("job.project", "project");

        if ($name = $paramFetcher->get("name")) {
            $qb->andWhere("job.name LIKE :name")
                ->setParameter("name", "%" . $name . "%");
        }

        if ($min_reward = $paramFetcher->get("min_reward")) {
            $qb->andWhere("job.monthlyReward >= :min")
                ->setParameter("min", $min_reward);
        }

        if ($max_reward = $paramFetcher->get("max_reward")) {
            $qb->andWhere("job.monthlyReward <= :max")
                ->setParameter("max", $max_reward);
        }

        if ($holder = $paramFetcher->get("holder")) {
            $qb->join("job.holder", "holder");
            $qb->andWhere("holder.username = :user")
                ->setParameter("user", $holder);
        }

        if ($candidate = $paramFetcher->get("candidate")) {
            $qb->andWhere("EXISTS(
                            SELECT pc
                            FROM TrevizProjectBundle:ProjectCandidacy pc
                            WHERE pc.user = :user
                            AND pc.job = job
                          )")
                ->setParameter("user", $candidate);
        }

        if ($project = $paramFetcher->get("project")) {
            $qb->andWhere("project.hash = :project")
                ->setParameter("project", $project);
        }

        if ($task = $paramFetcher->get("task")) {
            $task = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Task")->findOneByHash($task);
            if ($task) {
                $qb->andWhere(":task MEMBER OF job.tasks")
                    ->setParameter("task", $task);
            }
        }

        if($tags = $paramFetcher->get("tags")){
            foreach ($tags as $key=>$tag){
                $qb->andWhere("EXISTS(
                                SELECT tag$key
                                FROM TrevizTagBundle:Tag tag$key
                                WHERE tag$key.name = :tag$key
                                AND tag$key MEMBER OF job.tags
                              )");
                $qb->setParameter('tag'.$key, $tag);
            }
        }

        if($skills = $paramFetcher->get("skills")){
            foreach ($skills as $key=>$skill){
                $qb->andWhere("EXISTS(
                                SELECT skill$key
                                FROM TrevizSkillBundle:Skill skill$key
                                WHERE skill$key.name = :skill$key
                                AND skill$key MEMBER OF job.skills
                              )");
                $qb->setParameter('skill'.$key, $skill);
            }
        }

        if ($paramFetcher->get("attributed") === 'true') {
            $qb->andWhere("job.holder IS NOT NULL");
        } elseif($paramFetcher->get("attributed") === 'false') {
            $qb->andWhere("job.holder IS NULL");
        }

        $user = $this->getUser();

        if ($user !== null) {
            $qb->andWhere("project.public = TRUE
                        OR EXISTS (
                            SELECT pm
                            FROM TrevizProjectBundle:ProjectMembership pm
                            WHERE pm.user = :user
                            AND pm.project = project
                        )
                        OR EXISTS (
                            SELECT cm
                            FROM TrevizCommunityBundle:CommunityMembership cm
                            WHERE cm.user = :user
                            AND cm.community MEMBER OF project.communities
                        )");
            $qb->setParameter("user", $user);
        } else {
            $qb->andWhere("project.public = TRUE");
        }

        if($nb = $paramFetcher->get('nb')){
            $qb->setMaxResults($nb);
        }
        else{
            $qb->setMaxResults(10);
        }

        if($offset = $paramFetcher->get('offset')){
            $qb->setFirstResult($offset);
        }

        return $this->view($qb->getQuery()->getResult(), 200);

    }


    /**
     * Fetches a job identified by its hash.
     *
     * @SWG\Response(
     *     response=200,
     *     description="The job was successfully fetched",
     *     @Model(type=ProjectJob::class, groups={"Default", "job"})
     * ),
     *
     * @SWG\Response(
     *     response=404,
     *     description="No project was found for the specified hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to fetch this job",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @View(serializerGroups={"Default", "job"}, serializerEnableMaxDepthChecks=true)
     *
     * @Get("/projects/jobs/{hash}")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectJobAction($hash)
    {
        $job = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectJob")->findOneBy(array("hash" => $hash));

        if ($job) {

            $project = $job->getProject();

            /*
             * If the project is public, return its jobs.
             * Otherwise, check if the user has access to it.
             */

            if($project->isPublic()) {
                $view = $this->view($job, 200);
                return $view;
            } else {

                /** @var User $user */
                $user = $this->getUser();

                /*
                 * Firstly, we check if the user is a member of the project.
                 * Otherwise, we check if they share at least one community.
                 */
                if(!array_intersect($project->getMemberships()->toArray(), $user->getProjectsMemberships()->toArray())) {
                    $communities = [];
                    /** @var CommunityMembership $communitiesMembership */
                    foreach ($user->getCommunitiesMemberships() as $communitiesMembership) {
                        $communities[] = $communitiesMembership->getCommunity();
                    }

                    if(!array_intersect($communities, $project->getCommunities()->toArray())){
                        $view = $this->view("You can't see the jobs of this project", 403);
                        return $view;
                    };
                }

                $view = $this->view($job, 200);
                return $view;

            }

        }

        $view = $this->view("No job was found for this hash", 400);
        return $view;
    }

    /**
     * Add a new member to a project's team.
     * The member must be either invited or candidate.
     *
     * @SWG\Parameter(
     *     name="Job",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the job to create"
     *         ),
     *         @SWG\Property(
     *             property="description",
     *             type="string",
     *             description="Description of the job"
     *         ),
     *         @SWG\Property(
     *             property="monthlyReward",
     *             type="integer",
     *             description="Monthly reward of the job"
     *         ),
     *         @SWG\Property(
     *             property="contact",
     *             type="string",
     *             description="Username of the contact for the job"
     *         ),
     *         @SWG\Property(
     *             property="holder",
     *             type="string",
     *             description="Username of the user who has the job"
     *         ),
     *         @SWG\Property(
     *             property="skills",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of skill names"
     *         ),
     *         @SWG\Property(
     *             property="tags",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of tag names"
     *         )
     *     ),
     *     required=true,
     *     description="Job that should be created"
     * )
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to which the job should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created job",
     *     @Model(type=ProjectJob::class, groups={"Default", "job"})
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to create the job",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The project was not found",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="The submitted data is incorrect",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @View(serializerGroups={"Default", "job"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postProjectJobAction(Request $request, $hash)
    {

        $currentUser = $this->getUser();
        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")->findOneBy(array("hash" => $hash));

        if ($project != null) {

            /** @var ProjectMembership $currentUserMembership */
            $currentUserMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $currentUser
                ));

            /*
             * If the user has no job yet, he must be invited.
             * Check his or her invitation number.
             */
            if ($currentUserMembership !== null) {

                $job = new ProjectJob();
                $form = $this->createForm(ProjectJobType::class, $job);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $hash = hash("sha256", random_bytes(256));
                    while ($this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectJob")->findOneByHash($hash) !== null) {
                        $hash = hash("sha256", random_bytes(256));
                    }

                    $job->setHash($hash);
                    $job->setProject($project);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($job);
                    $em->flush();

                    return $this->view($job, 201);

                }

                return $this->view($form, 422);

            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No project was found matching hash " . $hash, 404);

    }

    /**
     * Updates an existing job of a project.
     *
     * @SWG\Parameter(
     *     name="Job",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the job to create"
     *         ),
     *         @SWG\Property(
     *             property="description",
     *             type="string",
     *             description="Description of the job"
     *         ),
     *         @SWG\Property(
     *             property="monthlyReward",
     *             type="integer",
     *             description="Monthly reward of the job"
     *         ),
     *         @SWG\Property(
     *             property="contact",
     *             type="string",
     *             description="Username of the contact for the job"
     *         ),
     *         @SWG\Property(
     *             property="holder",
     *             type="string",
     *             description="Username of the user who has the job"
     *         ),
     *         @SWG\Property(
     *             property="skills",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of skill names"
     *         ),
     *         @SWG\Property(
     *             property="tags",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of tag names"
     *         )
     *     ),
     *     required=true,
     *     description="Job that should be updated"
     * )
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the job to update"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The job was successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The job could not be fetched",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to update the job",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Put("/projects/jobs/{hash}")
     *
     * @View(serializerGroups={"Default", "job"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putProjectJobAction(Request $request, $hash)
    {

        $currentUser = $this->getUser();
        $job = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectJob")->findOneBy(array("hash" => $hash));

        if ($job != null) {

            /** @var ProjectMembership $currentUserMembership */
            $currentUserMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $job->getProject(),
                    "user" => $currentUser
                ));

            /*
             * If the user has no job yet, he must be invited.
             * Check his or her invitation number.
             */
            if ($currentUserMembership !== null
                && (in_array(ProjectPermissions::MANAGE_JOBS, $currentUserMembership->getRole()->getPermissions())
                    || $job->getContact() == $currentUser)) {

                $em = $this->getDoctrine()->getManager();

                $form = $this->createForm(ProjectJobType::class, $job);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    /*
                     * The job may have various candidacies.
                     * If the job has been attributed, remove all of them.
                     */
                    if ($job->getHolder() != null) {
                        foreach ($job->getApplications() as $application) {
                            if ($application !== null) {
                                $job->removeApplication($application);
                                $em->remove($application);
                            }
                        }
                    }

                    $em->flush();

                    return $this->view($job, 200);

                }

                return $this->view($form, 422);

            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No job was found matching hash " . $hash, 404);


    }

    /**
     * Removes a job.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the job to delete"
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the job is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The job could not be fetched",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to delete the job",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Delete("/projects/jobs/{hash}")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteProjectJobAction($hash){

        $currentUser = $this->getUser();

        $job = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectJob")
            ->findOneBy(array("hash" => $hash));

        if ($job != null) {

            $project = $job->getProject();

            $currentUserMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectJob")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $currentUser
                ));

            if( $currentUserMembership != null
                && in_array(ProjectPermissions::MANAGE_JOBS, $currentUserMembership->getRole()->getPermissions())) {

                $em = $this->getDoctrine()->getManager();
                $em->remove($job);
                $em->flush();

                return $this->view(null, 204);

            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No job was found matching hash " . $hash, 404);
    }

}