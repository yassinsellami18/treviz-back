<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 29/10/2017
 * Time: 11:30
 */

namespace Treviz\ProjectBundle\Controller;

use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\FOSRestController;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CoreBundle\Entity\User;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Treviz\ProjectBundle\Entity\ProjectMembership;
use Treviz\ProjectBundle\Entity\ProjectRole;
use Treviz\ProjectBundle\Form\ProjectMembershipType;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;


/**
 * Class ProjectMembershipController
 * @package Treviz\ProjectBundle\Controller
 *
 * @Security("has_role('ROLE_USER')")
 */
class ProjectMembershipController extends FOSRestController
{

    /**
     * Fetches the memberships of a project.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the memberships of a specified project",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectMembershipsAction($hash){

        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")->findOneBy(array("hash" => $hash));

        if ($project) {

            /*
             * If the project is public, return it
             * Otherwise, check if the user has access to it.
             */
            if($project->isPublic()) {
                $view = $this->view($project->getMemberships(), 200);
                return $view;
            } else {

                /** @var User $user */
                $user = $this->getUser();

                /*
                 * Firstly, we check if the user is a member of the project.
                 * Otherwise, we check if they share at least one community.
                 */
                if(!array_intersect($project->getMemberships()->toArray(), $user->getProjectsMemberships()->toArray())) {
                    $communities = [];
                    /** @var CommunityMembership $communitiesMembership */
                    foreach ($user->getCommunitiesMemberships() as $communitiesMembership) {
                        $communities[] = $communitiesMembership->getCommunity();
                    }

                    if(!array_intersect($communities, $project->getCommunities()->toArray())){
                        $view = $this->view("You can't see the memberships of this project", 403);
                        return $view;
                    };
                }

                    $view = $this->view($project->getMemberships(), 200);
                    return $view;

            }

        }

        $view = $this->view("No project was found for this hash", 400);
        return $view;
    }

    /**
     * Fetches the projects of a user specified in the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the memberships specified by the query",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @QueryParam(name="user", description="username of the user from whom fetch memberships", nullable=false)
     *
     * @View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectsMembershipsAction(ParamFetcher $paramFetcher){

        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")
            ->findOneBy(array("username" => $paramFetcher->get("user")));

        if ($user != null) {

            $currentUser = $this->getUser();

            if ($user == $currentUser){
                return $this->view($user->getProjectsMemberships(), 200);
            } else {

                /** @var QueryBuilder $qb */
                $qb = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")->createQueryBuilder("pm");
                $qb->join("pm.project", "p");
                $qb->andWhere("pm.user = :user");
                $qb->setParameter('user', $user);

                /*
                 * If the user making the query has not sent any authentication header, only return the public projects.
                 * Otherwise, return the public projects, the projects in which the current user is member, and those
                 * that are linked to at least one community he is into.
                 */
                if ($currentUser == null) {

                    $qb->andWhere("p.public = true");
                    return $this->view($qb->getQuery()->getResult(), 200);

                } else {
                    $publicOrShareProjectOrShareCommunity = $qb->expr()->orX();

                    $publicProject = $qb->expr()
                        ->exists("p.public = true");

                    $sharedProject = $qb->expr()
                        ->exists("SELECT pm2
                                  FROM TrevizProjectBundle:ProjectMembership pm2
                                  WHERE pm2.user = :currentUser
                                  AND pm2.project = p");

                    $sharedCommunity = $qb->expr()
                        ->exists("SELECT cm
                                  FROM TrevizCommunityBundle:CommunityMembership cm
                                  JOIN cm.community c
                                  WHERE cm.user = :currentUser
                                  AND c MEMBER OF p.communities");

                    $publicOrShareProjectOrShareCommunity
                        ->add($publicProject)
                        ->add($sharedProject)
                        ->add($sharedCommunity);

                    return $this->view($qb->getQuery()->getResult(), 200);

                }

            }

        }

        $view = $this->view(null, 200);
        return $view;

    }

    /**
     * Updates an existing membership of a project.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned the memberships was successfully updated",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Put("/projects/memberships/{hash}")
     *
     * @View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putProjectMembershipAction(Request $request, $hash)
    {

        $currentUser = $this->getUser();

        $currentMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
            ->findOneBy(array("hash" => $hash));

        if ($currentMembership != null) {

            $project = $currentMembership->getProject();

            $currentUserMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $currentUser
                ));

            if( $currentUserMembership != null
                && in_array(ProjectPermissions::MANAGE_MEMBERSHIP, $currentUserMembership->getRole()->getPermissions())) {

                $form = $this->createForm(ProjectMembershipType::class, $currentMembership);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                }

                return $this->view("Invalid data", 400);

            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No membership was found matching hash " . $hash, 404);

    }

    /**
     * Removes a user from a project team.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the membership is successfully deleted",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Delete("/projects/memberships/{hash}")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteProjectMembershipAction($hash){

        $currentUser = $this->getUser();

        $currentMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
            ->findOneBy(array("hash" => $hash));

        if ($currentMembership != null) {

            $project = $currentMembership->getProject();

            $currentUserMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $currentUser
                ));

            if( $currentUserMembership != null
                && in_array(ProjectPermissions::MANAGE_MEMBERSHIP, $currentUserMembership->getRole()->getPermissions())) {

                $em = $this->getDoctrine()->getManager();
                $em->remove($currentMembership);
                $em->flush();

            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No membership was found matching hash " . $hash, 404);
    }

}