<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 29/10/2017
 * Time: 11:36
 */

namespace Treviz\ProjectBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ProjectIllustrationController
 * @package Treviz\ProjectBundle\Controller
 *
 * @Security("has_role('ROLE_USER')")
 */
class ProjectIllustrationController extends FOSRestController
{

    /**
     * Return an array containing all the base illustration urls for projects.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned an array of the different pictures urls used to illustrate the projects",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(
     *             type="string"
     *         ),
     *         description="array of urls"
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Get("/projects/base-illustrations")
     */
    public function getProjectBaseIllustrationsAction(){
        $file_urls = [];

        $dir = $this->getParameter("kernel.root_dir") .
            '/../web' .
            $this->getParameter('base_projects');

        $baseUrl = $this->getParameter("backend_url") . $this->getParameter('base_projects');

        if(is_dir($dir)){
            if($dh = opendir($dir)){
                while (($file = readdir($dh)) !== false){
                    if($file != "." && $file != "..") {
                        $file_urls[] = $baseUrl . '/' .$file;
                    }
                }
            }
        }

        $view = $this->view($file_urls, 200);

        return $this->handleView($view);

    }

}