<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 28/06/2018
 * Time: 13:33
 */

namespace Treviz\ProjectBundle\Controller;


use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Treviz\CoreBundle\Entity\User;
use Treviz\ProjectBundle\Entity\Project;
use Treviz\ProjectBundle\Entity\ProjectMembership;
use Treviz\ProjectBundle\Entity\ProjectNotificationPreferences;

class ProjectPreferencesController extends FOSRestController
{

    /**
     * @Rest\Get("/projects/memberships/{hash}/preferences)
     *
     * @param string $hash
     * @return \FOS\RestBundle\View\View|\Treviz\ProjectBundle\Entity\ProjectNotificationPreferences
     */
    public function getProjectPreferencesAction(string $hash)
    {

        /** @var ProjectMembership $membership */
        $membership = $this->getDoctrine()->getRepository('TrevizProjectBundle:ProjectMembership')
            ->findOneByHash($hash);

        if($membership !== null) {

            /** @var User $currentUser */
            $currentUser = $this->getUser();

            if($membership->getUser() === $currentUser) {
                return $this->view($membership->getPreferences(), 200);
            }

          return $this->view('You do not have the rights to do this', 403);

        }

        return $this->view("No membership was found for hash $hash", 404);

    }

    /**
     * Updates the preferences of a project membership.
     *
     * @Rest\Put("/projects/memberships/{hash}/preferences)
     *
     * @param Request $request
     * @param         $hash
     *
     * @return \FOS\RestBundle\View\View
     */
    public function updateProjectMembershipPreferencesAction(Request $request, string $hash): \FOS\RestBundle\View\View
    {
        /** @var ProjectMembership $membership*/
        $membership = $this->getDoctrine()->getRepository('TrevizProjectBundle:ProjectMembership')
            ->findOneByHash($hash);

        if ($membership !== null) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            if ($membership->getUser() === $currentUser) {

                $preferences = $membership->getPreferences();
                $form = $this->createForm(ProjectNotificationPreferences::class, $preferences);
                $form->submit($request);

                if($form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                    return $this->view($preferences, 200);
                }

                return $this->view('Invalid data', 400);
            }

            return $this->view('You can only see your own membership\'s preferences', 403);
        }

        return $this->view("No membership was found with hash $hash", 404);
    }

}