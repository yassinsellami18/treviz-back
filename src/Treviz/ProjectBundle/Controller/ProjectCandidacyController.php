<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 30/10/2017
 * Time: 15:25
 */

namespace Treviz\ProjectBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\ProjectBundle\Events\ProjectCandidacyCreatedEvent;
use Treviz\CoreBundle\Entity\User;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Treviz\ProjectBundle\Entity\ProjectCandidacy;
use Treviz\ProjectBundle\Entity\ProjectMembership;
use Treviz\ProjectBundle\Entity\ProjectRole;
use Treviz\ProjectBundle\Form\ProjectCandidacyType;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class ProjectCandidacyController
 * @package Treviz\ProjectBundle\Controller
 */
class ProjectCandidacyController extends FOSRestController
{


    /**
     * Fetches the candidacies of a project
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project from which the candidacies should be fetched"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the candidacies of a specified project",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectCandidacy::class, groups={"Default", "project"})
     *     )
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to fetch candidacies for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Project does not exist",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectCandidaciesAction($hash)
    {
        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")
            ->findOneBy(array("hash" => $hash));

        if ($project) {
            $user = $this->getUser();

            /** @var ProjectMembership $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $user
                ));

            if($membership !== null
            && in_array(ProjectPermissions::MANAGE_CANDIDACIES, $membership->getRole()->getPermissions())) {
                return $this->view($project->getCandidacies(), 200);
            }

            $candidacy = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectCandidacy")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $user
                ));

            if ($candidacy !== null) {
                return $this->view([$candidacy], 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No project was found with hash " . $hash, 404);

    }

    /**
     * Fetches the candidacies according to the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the candidacies specified by the query",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectCandidacy::class, groups={"Default", "user"})
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @QueryParam(name="user", description="username of the user from whom fetch candidacies", nullable=false)
     * @QueryParam(name="job", description="Hash of the job from which fetch the candidacies", nullable=false)
     *
     * @View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectsCandidaciesAction(ParamFetcher $paramFetcher)
    {

        $repository = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectCandidacy");
        $qb = $repository->createQueryBuilder("pc");

        if ($username = $paramFetcher->get('user')) {
            $qb->join('pc.user', 'user');
            $qb->andWhere("user.username = :user");
            $qb->setParameter("user", $username);
        }

        /*
         * If the current user does not look for his own candidacies, only display those he should be able to see.
         */
        if (!$username || $username !== $this->getUser()->getUsername()) {
            $qb->andWhere("EXISTS (
                            SELECT pm
                            FROM TrevizProjectBundle:ProjectMembership pm
                            JOIN pm.role r
                            WHERE pm.project = pc.project
                            AND pm.user = :current_user
                            AND r.permissions LIKE :read_candidacies
                          )");
            $qb->setParameter("current_user", $this->getUser());
            $qb->setParameter("read_candidacies", '%'.ProjectPermissions::MANAGE_CANDIDACIES.'%');
        }

        if ($job = $paramFetcher->get('job')) {
            $qb->join('pc.job', 'job');
            $qb->andWhere('job.hash = :job')
                ->setParameter('job', $job);
        }

        $candidacies = $qb->getQuery()->getResult();
        return $this->view($candidacies, 200);

    }

    /**
     * Creates a new candidacy for a project or job.
     * A candidacy is only acceptable for a job if it is not currently attributed.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project for which the candidacy should be created"
     * )
     *
     * @SWG\Parameter(
     *     name="candidacy",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="job",
     *             type="string",
     *             description="Hash of the job to which candidate"
     *         ),
     *         @SWG\Property(
     *             property="message",
     *             type="string",
     *             description="Message to justify the candidacy"
     *         )
     *     ),
     *     required=true,
     *     description="Candidacy that should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created candidacy",
     *     @Model(type=ProjectCandidacy::class, groups={"Default"})
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to create candidacies for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Project does not exist",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data was submitted",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postProjectCandidacyAction(Request $request, $hash): \FOS\RestBundle\View\View
    {
        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")
            ->findOneBy(array('hash' => $hash));

        if ($project) {

            $user = $this->getUser();

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    'project' => $project,
                    'user' => $user
                ));

            $invitation = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectInvitation")
                ->findOneBy(array(
                    'project' => $project,
                    'user' => $user
                ));

            $candidacy = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectCandidacy")
                ->findOneBy(array(
                    'project' => $project,
                    'user' => $user
                ));

            /*
             * A user can only candidate to projects if he has not yet been invited, candidate, or if he candidates
             * for a job.
             */
            if($invitation === null && $candidacy === null && $project->isOpen() && ($membership === null || $request->get('job'))) {

                $candidacy = new ProjectCandidacy();
                $form = $this->createForm(ProjectCandidacyType::class, $candidacy);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    if ($job = $candidacy->getJob()) {
                        if ($job->getHolder() !== null) {
                            return $this->view('The job is already attributed', 409);
                        }
                    }

                    $candidacy->setProject($project);
                    $candidacy->setUser($user);

                    $candidacyHash = hash('sha256', random_bytes(256));

                    while ($this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectCandidacy")
                            ->findOneBy(array('hash' => $candidacyHash)) !== null) {
                        $candidacyHash = hash('sha256', random_bytes(256));
                    }

                    $candidacy->setHash($candidacyHash);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($candidacy);
                    $em->flush();

                    $event = new ProjectCandidacyCreatedEvent($candidacy);
                    $dispatcher = $this->get('treviz_core.event_dispatcher.dispatcher');
                    $dispatcher->dispatch(ProjectCandidacyCreatedEvent::NAME, $event);

                    return $this->view($candidacy, 200);

                }

                return $this->view($form, 422);

            }

            return $this->view('You cannot candidate to this project', 403);

        }

        return $this->view('No project was found with hash '. $hash, 404);
    }

    /**
     * Removes a candidacy from a project.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the candidacy is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User does not have the rights to delete candidacies for this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Candidacy does not exist",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Delete("/projects/candidacies/{hash}")
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteProjectCandidacyAction($hash)
    {
        $candidacy = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectCandidacy")
            ->findOneBy(array("hash" => $hash));

        if ($candidacy) {
            $user = $this->getUser();

            /** @var ProjectMembership $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $candidacy->getProject(),
                    "user" => $user
                ));

            if($candidacy->getUser() == $user
                || ($membership ==! null
                    && in_array(ProjectPermissions::MANAGE_CANDIDACIES, $membership->getRole()->getPermissions()))){

                $em = $this->getDoctrine()->getManager();
                $em->remove($candidacy);
                $em->flush();
                return $this->view(null, 204);

            }

            return $this->view("You do not have the rights to do this", 403);

        }

        return $this->view("No candidacy was found with hash " . $hash, 404);
    }

    /**
     * Accept a candidacy for a project and/or job.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the candidacy to accept"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The candidacy is successfully accepted",
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="The submitted role is invalid",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to accept the candidacy",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The candidacy could not be fetched",
     * )
     *
     * @SWG\Response(
     *     response=409,
     *     description="The job the user wants is already taken.",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Post("/projects/candidacies/{hash}/accept")
     *
     * @RequestParam(name="role", description="hash of the role to give to the user if he/she is not yet member of the project.")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function acceptProjectCandidacyAction($hash)
    {
        /** @var ProjectCandidacy $candidacy */
        $candidacy = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectCandidacy")
            ->findOneByHash($hash);

        if ($candidacy) {

            /** @var User $currentUser */
            $currentUser = $this->getUser();

            /** @var ProjectMembership $currentUserMembership */
            $currentUserMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $candidacy->getProject(),
                    "user" => $currentUser
                ));

            if ($currentUserMembership !== null
                && in_array(ProjectPermissions::MANAGE_CANDIDACIES, $currentUserMembership->getRole()->getPermissions())
            ) {

                $userCurrentMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $candidacy->getProject(),
                        "user" => $candidacy->getUser()
                    ));

                $em = $this->getDoctrine()->getManager();

                /*
                 * A candidacy can only be accepted for a job if it has no current holder.
                 * If such is the case, all other candidacies are deleted.
                 * The candidate becomes a member of the project (if he/she was not already).
                 */
                if ($job = $candidacy->getJob()) {
                    if ($job->getHolder() == null) {
                        $job->setHolder($candidacy->getUser());
                        foreach ($job->getApplications() as $application) {
                            $job->removeApplication($application);
                        }
                    } else {
                        return $this->view("This job is already taken", 409);
                    }
                }

                if (!$userCurrentMembership) {
                    $membership = new ProjectMembership();
                    $membership->setProject($candidacy->getProject());
                    $membership->setUser($candidacy->getUser());

                    $membershipHash = hash("sha256", random_bytes(256));

                    while ($this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                            ->findOneBy(array('hash' => $membershipHash)) !== null) {
                        $membershipHash = hash("sha256", random_bytes(256));
                    }
                    $membership->setHash($membershipHash);

                    /** @var ProjectRole $role */
                    $role = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
                        ->findOneBy(array(
                            'defaultMember' => true,
                            'project' => $candidacy->getProject()
                        ));

                    if ($role) {
                        $membership->setRole($role);
                    } else {
                        $role = new ProjectRole();

                        $roleHash = hash("sha256", random_bytes(256));
                        while ($this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
                                ->findOneBy(array("hash" => $roleHash)) != null) {
                            $roleHash = hash("sha256", random_bytes(256));
                        }
                        $role->setHash($roleHash);
                        $role->setName('Member');
                        $role->setProject($candidacy->getProject());
                        $role->setDefaultMember(true);
                        $em->persist($role);
                        $membership->setRole($role);
                    }

                    $em->persist($membership);

                }

                $em->remove($candidacy);
                $em->flush();

                return $this->view("Candidacy was accepted", 200);

            }

            return $this->view("You do not have the rights to do this" . $hash, 403);

        }

        return $this->view("No candidacy was found for hash " . $hash, 404);

    }

}