<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 29/10/2017
 * Time: 12:11
 */

namespace Treviz\ProjectBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Treviz\ProjectBundle\Entity\ProjectRole;
use Treviz\ProjectBundle\Form\ProjectRoleType;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Manages the various roles of a project.
 *
 * @Security("has_role('ROLE_USER')")
 *
 * Class ProjectRoleController
 * @package Treviz\ProjectBundle\Controller
 */
class ProjectRoleController extends FOSRestController
{

    /**
     * Fetches the roles of a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project from which the roles should be fetched"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the roles of a specified project",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectRole::class)
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getProjectRolesAction($hash)
    {

        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")
            ->findOneBy(array("hash" => $hash));

        if ($project) {

            $roles = $project->getRoles();

            $globalRoles = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
                ->findBy(array("global" => true));

            foreach ($globalRoles as $globalRole) {
                $roles->add($globalRole);
            }

            $view = $this->view($roles, 200);
            return $this->handleView($view);
        }

        $view = $this->view("No project was found for hash " . $hash, 404);
        return $this->handleView($view);

    }

    /**
     * Adds a new role to a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to which the role should be created"
     * )
     *
     * @SWG\Parameter(
     *     name="Role",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the role to create"
     *         ),
     *         @SWG\Property(
     *             property="permissions",
     *             type="array",
     *             @SWG\Items(
     *                 type="string"
     *             ),
     *             description="Array of the permissions the role should have",
     *             enum={"UPDATE_PROJECT",
     *                   "DELETE_PROJECT",
     *                   "MANAGE_ROLE",
     *                   "MANAGE_MEMBERSHIP",
     *                   "MANAGE_POST",
     *                   "MANAGE_CANDIDACIES",
     *                   "MANAGE_INVITATION",
     *                   "MANAGE_DOCUMENT",
     *                   "MANAGE_BOARD",
     *                   "MANAGE_CROWD_FUND",
     *                   "MANAGE_JOB"}
     *         ),
     *         @SWG\Property(
     *             property="defaultMember",
     *             type="boolean",
     *             description="True if this role is the role by default of new members of the project."
     *         )
     *     ),
     *     required=true,
     *     description="Role that should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created role",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectRole::class)
     *     )
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to create this role"
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The project does not exist"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Submitted data is invalid"
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @param Request $request
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postProjectRoleAction(Request $request, $hash)
    {

        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")
            ->findOneBy(array("hash" => $hash));

        if ($project) {

            $user = $this->getUser();

            $userMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "user" => $user,
                    "project" => $project
                ));

            if ( $userMembership != null
                && in_array(ProjectPermissions::MANAGE_ROLE, $userMembership->getRole()->getPermissions())) {
                $role = new ProjectRole();
                $form = $this->createForm(ProjectRoleType::class, $role);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $role->setProject($project);
                    $role->setDefaultCreator(false);
                    $role->setGlobal(false);

                    /*
                     * If this is the default member role, unset the previous default member role.
                     */
                    if ($role->isDefaultMember()) {
                        $previousDefaultRole = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
                            ->findOneBy(array(
                                "defaultMember" => true,
                                "project" => $project
                            ));
                        if ($previousDefaultRole !== null) {
                            $previousDefaultRole->setDefaultMember(false);
                        }
                    }

                    $roleHash = hash("sha256", random_bytes(256));

                    while ($this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
                            ->findOneBy(array("hash" => $roleHash)) != null) {
                        $roleHash = hash("sha256", random_bytes(256));
                    }

                    $role->setHash($roleHash);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($role);
                    $em->flush();

                    $view = $this->view($role, 200);
                    return $this->handleView($view);

                }

                $view = $this->view($form, 422);
                return $this->handleView($view);

            }

            $view = $this->view("You do not have the rights to do this ", 403);
            return $this->handleView($view);
        }

        $view = $this->view("No project was found for hash " . $hash, 404);
        return $this->handleView($view);
    }

    /**
     * Adds a new role that can be used in all projects.
     *
     * @SWG\Parameter(
     *     name="Role",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the role to create"
     *         ),
     *         @SWG\Property(
     *             property="permissions",
     *             type="array",
     *             @SWG\Items(
     *                 type="string"
     *             ),
     *             description="Array of the permissions the role should have",
     *             enum={"UPDATE_PROJECT",
     *                   "DELETE_PROJECT",
     *                   "MANAGE_ROLE",
     *                   "MANAGE_MEMBERSHIP",
     *                   "MANAGE_POST",
     *                   "MANAGE_CANDIDACIES",
     *                   "MANAGE_INVITATION",
     *                   "MANAGE_DOCUMENT",
     *                   "MANAGE_BOARD",
     *                   "MANAGE_CROWD_FUND",
     *                   "MANAGE_JOB"}
     *         ),
     *         @SWG\Property(
     *             property="defaultMember",
     *             type="boolean",
     *             description="True if this role is the role by default of new members of the project."
     *         ),
     *         @SWG\Property(
     *             property="defaultCreator",
     *             type="boolean",
     *             description="True if this role is the role by default of new project creators."
     *         )
     *     ),
     *     required=true,
     *     description="Role that should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created global role",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=ProjectRole::class)
     *     )
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Submitted data is invalid"
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @Post("/projects/roles")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postProjectsRoleAction(Request $request)
    {
        $role = new ProjectRole();
        $form = $this->createForm(ProjectRoleType::class, $role);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $roleHash = hash("sha256", random_bytes(256));

            while ($this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
                    ->findOneBy(array("hash" => $roleHash)) != null) {
                $roleHash = hash("sha256", random_bytes(256));
            }

            /*
             * If this is the default creator role, unset the previous default creator role.
             */
            if ($role->isDefaultCreator()) {
                $previousDefaultRole = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
                    ->findOneBy(array(
                        "defaultCreator" => true,
                        "global" => true
                    ));
                if ($previousDefaultRole !== null) {
                    $previousDefaultRole->setDefaultMember(false);
                }
            }

            /*
             * If this is the default member role, unset the previous default member role.
             */
            if ($role->isDefaultMember()) {
                $previousDefaultRole = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
                    ->findOneBy(array(
                        "defaultMember" => true,
                        "global" => true
                    ));
                if ($previousDefaultRole !== null) {
                    $previousDefaultRole->setDefaultMember(false);
                }
            }

            $role->setHash($roleHash);
            $role->setGlobal(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();

            $view = $this->view($role, 200);
            return $this->handleView($view);

        }

        $view = $this->view($form, 422);
        return $this->handleView($view);
    }

    /**
     * Updates an existing role.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the role that should be updated"
     * )
     *
     * @SWG\Parameter(
     *     name="Role",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the role to create"
     *         ),
     *         @SWG\Property(
     *             property="permissions",
     *             type="array",
     *             @SWG\Items(
     *                 type="string"
     *             ),
     *             description="Array of the permissions the role should have",
     *             enum={"MANAGE_PROJECT",
     *                   "MANAGE_PROJECT",
     *                   "MANAGE_ROLE",
     *                   "MANAGE_ROLE",
     *                   "MANAGE_ROLE",
     *                   "MANAGE_MEMBERSHIP",
     *                   "MANAGE_MEMBERSHIP",
     *                   "MANAGE_POST",
     *                   "MANAGE_POST",
     *                   "MANAGE_POST",
     *                   "READ_CANDIDACIES",
     *                   "READ_INVITATIONS",
     *                   "MANAGE_INVITATION",
     *                   "MANAGE_INVITATION",
     *                   "MANAGE_DOCUMENT",
     *                   "MANAGE_DOCUMENTS",
     *                   "MANAGE_DOCUMENT",
     *                   "MANAGE_BOARD",
     *                   "MANAGE_BOARD",
     *                   "MANAGE_BOARD",
     *                   "MANAGE_CROWD_FUND",
     *                   "MANAGE_JOB",
     *                   "MANAGE_JOB",
     *                   "MANAGE_JOB",
     *                   "MANAGE_APPLICATION"}
     *         ),
     *         @SWG\Property(
     *             property="defaultMember",
     *             type="boolean",
     *             description="True if this role is the role by default of new members of the project."
     *         ),
     *         @SWG\Property(
     *             property="defaultCreator",
     *             type="boolean",
     *             description="True if this role is the role by default of new project creators."
     *         )
     *     ),
     *     required=true,
     *     description="Role that should be updated"
     * )

     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated role",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Put("/projects/roles/{hash}")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function updateProjectRoleAction(Request $request, $hash)
    {

        $role = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
            ->findOneBy(array("hash" => $hash));

        if ($role) {

            $user = $this->getUser();

            $userMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "user" => $user,
                    "project" => $role->getProject()
                ));

            if ((!$role->isGlobal()
                    && $userMembership !== null
                    && in_array(ProjectPermissions::MANAGE_ROLE, $userMembership->getRole()->getPermissions()))
                || ($role->isGlobal()
                    && ($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')))
            ) {
                $form = $this->createForm(ProjectRoleType::class, $role);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    if (!$role->isGlobal()) {
                        $role->setDefaultCreator(false);
                    }

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->view($role, 200);

                }

                return $this->view($form, 422);

            }

            return $this->view("You do not have the rights to do this ", 403);
        }

        return $this->view("No role was found for hash " . $hash, 404);
    }

    /**
     * Deletes an existing role of a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the role that should be deleted"
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the role was successfully deleted",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Delete("/projects/roles/{hash}")
     *
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteProjectRoleAction($hash)
    {

        $role = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")
            ->findOneBy(array("hash" => $hash));

        if ($role!= null) {

            if(($role->isGlobal() && $this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($role);
                $em->flush();

                $view = $this->view(null, 204);
                return $this->handleView($view);
            }

            if(!$role->isGlobal()) {
                $user = $this->getUser();

                $userMembership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "user" => $user,
                        "project" => $role->getProject()
                    ));

                /*
                 * Checks if the user has the rights to delete the membership.
                 */
                if ($userMembership != null
                    && in_array(ProjectPermissions::MANAGE_ROLE, $userMembership->getRole()->getPermissions())) {

                    $em = $this->getDoctrine()->getManager();
                    $em->remove($role);
                    $em->flush();

                    $view = $this->view(null, 204);
                    return $this->handleView($view);
                }
            }

            $view = $this->view("You do not have the rights to do this.", 403);
            return $this->handleView($view);
        }

        $view = $this->view("No role was found for hash " . $hash, 404);
        return $this->handleView($view);
    }

}