<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 10/02/2017
 * Time: 12:42
 */

namespace Treviz\ProjectBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CommunityBundle\Entity\Community;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\ProjectBundle\Events\ProjectCreatedEvent;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Treviz\ProjectBundle\Entity\Project;
use Treviz\CoreBundle\Entity\User;
use Treviz\ProjectBundle\Entity\ProjectMembership;
use Treviz\ProjectBundle\Entity\ProjectRole;
use Treviz\ProjectBundle\Form\ProjectType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\FileParam;
use FOS\RestBundle\Controller\Annotations\Post;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class ProjectController
 * @package Treviz\ProjectBundle\Controller
 * @Security("has_role('ROLE_USER')")
 */
class ProjectController extends FOSRestController
{

    /**
     * Fetches a specific project, if the user has access to it.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to fetch"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns a project identified by its hash",
     *     @Model(type=Project::class, groups={"Default", "project"})
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to see this project",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The project does not exist",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectAction($hash){
        /** @var Project $project */
        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")->findOneBy(array("hash" => $hash));

        /*
         * We check if the project exist
         */
        if ($project) {

            /*
             * If the project is public, return it
             * Otherwise, check if the user has access to it.
             */
            if($project->isPublic()) {
                $view = $this->view($project, 200);
                return $view;
            } else {

                /** @var User $user */
                $user = $this->getUser();

                /*
                 * Firstly, we check if the user is a member of the project.
                 * Otherwise, we check if they share at least one community.
                 */
                if(!array_intersect($project->getMemberships()->toArray(), $user->getProjectsMemberships()->toArray())) {
                    $communities = [];
                    /** @var CommunityMembership $communitiesMembership */
                    foreach ($user->getCommunitiesMemberships() as $communitiesMembership) {
                        $communities[] = $communitiesMembership->getCommunity();
                    }

                    if(!array_intersect($communities, $project->getCommunities()->toArray())){
                        $view = $this->view("You can't see this project", 403);
                        return $view;
                    };
                } else {
                    $view = $this->view($project, 200);
                    return $view;
                }

            }

        }
        $view = $this->view("No project was found for this hash", 404);
        return $view;
    }

    /**
     * Fetches specific projects according to query parameters.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of projects matching the query",
     *     @SWG\Schema(
     *         type="array",
     *         @Model(type=Project::class, groups={"Default"})
     *     )
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @QueryParam(name="name", requirements="[\p{L}\p{N}_\s]+", description="search  by name", nullable=true)
     * @QueryParam(name="tags", description="search by tag", nullable=true)
     * @QueryParam(name="skills", description="search by skills", nullable=true)
     * @QueryParam(name="offset", description="\d+", nullable=true)
     * @QueryParam(name="nb", description="\d+", nullable=true)
     * @QueryParam(name="user", description="username of the user who must take part in the project", nullable=true)
     * @QueryParam(name="community", description="hash of the community the project must be linked to", nullable=true)
     * @QueryParam(name="role", description="name of role of the current user in those projects", nullable=true)
     *
     * @View(serializerGroups={"Default"},serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getProjectsAction(ParamFetcher $paramFetcher)
    {
        $s_name = $paramFetcher->get("name");
        $s_community = $paramFetcher->get("community");
        $s_interests = $paramFetcher->get("tags");
        $s_skills = $paramFetcher->get("skills");
        $s_user = $paramFetcher->get("user");
        $s_role = $paramFetcher->get('role');
        $s_start = $paramFetcher->get("offset");
        $s_nb = $paramFetcher->get("nb");

        $repository = $this->getDoctrine()->getRepository('TrevizProjectBundle:Project');

        /** @var QueryBuilder $query */
        $qb = $repository->createQueryBuilder('project');

        if($s_name){
            $qb->andWhere("project.name LIKE :name");
            $qb->setParameter('name', '%'.$s_name.'%');
        }

        /*
         * If a community is specified, only return the projects of this community.
         * Otherwise, return all visible projects.
         *
         * Check if the user can see the community first.
         *
         */
        if($s_community){

            /** @var Community $community */
            $community = $this->getDoctrine()
                ->getRepository("TrevizCommunityBundle:Community")
                ->findOneBy(array("hash" => $s_community));

            if ($community == null) {
                $view = $this->view([], 404);
                return $view;
            } elseif( !$community->isPublic()
                AND !array_intersect($community->getMemberships()->toArray(),
                    $this->getUser()->getCommunitiesMemberships()->toArray())) {
                $view = $this->view([], 403);
                return $view;
            } else {
                $qb->andWhere(":community MEMBER OF project.communities");
                $qb->setParameter('community', $community->getId());
            }

        } else {

            /** @var User $user */
            $user = $this->getUser();

            if ($user) {

                /*
                 * We only return the public projects, the private ones who share at least one community with the
                 * current user, and those in which he is member.
                 */

                $qb->andWhere("project.public = true 
                           OR EXISTS(
                            SELECT pm
                            FROM TrevizProjectBundle:ProjectMembership pm
                            WHERE pm.user = :currentUser
                            AND pm.project = project
                           )
                           OR EXISTS(
                            SELECT cm
                            FROM TrevizCommunityBundle:CommunityMembership cm
                            JOIN cm.community c
                            WHERE cm.user = :currentUser
                            AND c MEMBER OF project.communities
                           )");
                $qb->setParameter("currentUser", $user);

            } else {
                $qb->andWhere("project.public = true");
            }
        }

        if($s_interests){
            foreach ($s_interests as $key=>$s_interest){
                $qb->andWhere("EXISTS(
                                SELECT tag$key
                                FROM TrevizTagBundle:Tag tag$key
                                WHERE tag$key.name = :tag$key
                                AND tag$key MEMBER OF project.tags
                              )");
                $qb->setParameter('tag'.$key, $s_interest);
            }
        }

        if($s_skills){
            foreach ($s_skills as $key=>$s_skill){
                $qb->andWhere("EXISTS(
                                SELECT skill$key
                                FROM TrevizSkillBundle:Skill skill$key
                                WHERE skill$key.name = :skill$key
                                AND skill$key MEMBER OF project.skills
                              )");
                $qb->setParameter('skill'.$key, $s_skill);
            }
        }

        if($s_user){

            $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(array("username" => $s_user));

            if($user == null) {
                $view = $this->view([], 200);
                return $view;
            }
            else {

                if ($s_role !== null) {
                    $qb->andWhere("EXISTS (
                                    SELECT pm2
                                    FROM TrevizProjectBundle:ProjectMembership pm2
                                    JOIN pm2.role r
                                    WHERE pm2.project = project
                                    AND pm2.user = :user
                                    AND r.name = :role
                                   )");
                    $qb->setParameter("user", $user);
                    $qb->setParameter("role", $s_role);

                } else {
                    $qb->andWhere('EXISTS (
                                    SELECT pm2
                                    FROM TrevizProjectBundle:ProjectMembership pm2
                                    WHERE pm2.user = :user
                                    AND pm2.project = project)');
                    $qb->setParameter("user", $user);
                }

            }

        }

        if(isset($s_nb)){
            $qb->setMaxResults($s_nb);
        }
        else{
            $qb->setMaxResults(10);
        }

        if(isset($s_start)){
            $qb->setFirstResult($s_start);
        }

       $qb->orderBy("project.creationDate", 'DESC');

        /** @var ArrayCollection<Project> $users */
        $projects = $qb->getQuery()->getResult();

        $view = $this->view($projects, 200);
        return $view;
    }

    /**
     * Creates a new project in database, and returns it
     *
     * @SWG\Parameter(
     *     name="Project",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the project to create"
     *         ),
     *         @SWG\Property(
     *             property="description",
     *             type="string",
     *             description="Description of the project"
     *         ),
     *         @SWG\Property(
     *             property="shortDescription",
     *             type="string",
     *             description="Short description of the project",
     *             maxLength=140
     *         ),
     *         @SWG\Property(
     *             property="isPublic",
     *             type="string",
     *             description="If true, the project will be displayed to all users, logged in or not"
     *         ),
     *         @SWG\Property(
     *             property="logo",
     *             type="file",
     *             description="Logo of the project to upload"
     *         ),
     *         @SWG\Property(
     *             property="logoUrl",
     *             type="string",
     *             description="URL of the logo to upload, of a file cannot be sent."
     *         ),
     *         @SWG\Property(
     *             property="skills",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of skill names"
     *         ),
     *         @SWG\Property(
     *             property="tags",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of tag names"
     *         ),
     *         @SWG\Property(
     *             property="communities",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of community names to which link the project"
     *         ),
     *         @SWG\Property(
     *             property="idea",
     *             type="file",
     *             description="Hash of the idea the project comes from"
     *         )
     *     ),
     *     required=true,
     *     description="Project that should be created"
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created project",
     *     @Model(type=Project::class, groups={"Default", "project"})
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postProjectAction(Request $request){

        /** @var User $user */
        $user = $this->getUser();

        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            /** @var File $logo */
            $logo = $request->files->get("logo");

            if ($logo !== null) {
                $logoName = md5($logo->getFilename() . uniqid()) . '.' . $logo->guessExtension();

                $logo->move(
                    $this->getParameter("kernel.root_dir") .
                    '/../web' .
                    $this->getParameter('upload_project_logo_path'),
                    $logoName
                );

                $project->setLogoUrl(
                    $this->getParameter("backend_url") .
                    $this->getParameter("upload_project_logo_path") .
                    $logoName
                );

            }

            $membership = new ProjectMembership();
            $membership->setProject($project);
            $membership->setUser($user);

            $defaultCreatorRole = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectRole")->findOneBy(array("defaultCreator" => true));
            /*
             * If no default role has been defined for project creators, we generate one.
             */
            if ($defaultCreatorRole == null) {
                $defaultCreatorRole = new ProjectRole();
                $defaultCreatorRole->setName("Creator");
                $defaultCreatorRole->setDefaultCreator(true);
                $defaultCreatorRole->setDefaultMember(false);
                $defaultCreatorRole->setGlobal(true);
                $defaultCreatorRole->setPermissions(ProjectPermissions::getAvailablePermissions());
                $defaultCreatorRole->setHash(hash("sha256", random_bytes(256)));
            }

            $membership->setRole($defaultCreatorRole);

            /*
             * Generate a unique hash for the project and membership
             */
            $projectHash = hash("sha256", random_bytes(256));

            while ($this->getDoctrine()->getRepository("TrevizProjectBundle:Project")
                    ->findOneBy(array("hash" => $projectHash)) !== null) {
                $projectHash = hash("sha256", random_bytes(256));
            }

            $project->setHash($projectHash);

            $membershipHash = hash("sha256", random_bytes(256));

            while ($this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array("hash" => $membershipHash)) !== null) {
                $membershipHash = hash("sha256", random_bytes(256));
            }

            $membership->setHash($membershipHash);

            /*
             * Add user to the default chatroom
             */
            $user->addRoom($project->getRooms()->first());

            /*
             * Persist data
             */
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->persist($membership);
            $em->flush();

            /*
             * Sends a mail to the members of the project's communities
             */
            $event = new ProjectCreatedEvent($project);
            $dispatcher = $this->get('treviz_core.event_dispatcher.dispatcher');
            $dispatcher->dispatch(ProjectCreatedEvent::NAME, $event);

            return $this->view($project, 201);
        }

        return $this->view($form, 422);

    }

    /**
     * Updates an existing project
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to update"
     * )
     *
     * @SWG\Parameter(
     *     name="Project",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             description="Name of the project to create"
     *         ),
     *         @SWG\Property(
     *             property="description",
     *             type="string",
     *             description="Description of the project"
     *         ),
     *         @SWG\Property(
     *             property="shortDescription",
     *             type="string",
     *             description="Short description of the project",
     *             maxLength=140
     *         ),
     *         @SWG\Property(
     *             property="isPublic",
     *             type="string",
     *             description="If true, the project will be displayed to all users, logged in or not"
     *         ),
     *         @SWG\Property(
     *             property="logo",
     *             type="file",
     *             description="Logo of the project to upload"
     *         ),
     *         @SWG\Property(
     *             property="logoUrl",
     *             type="string",
     *             description="URL of the logo to upload, of a file cannot be sent."
     *         ),
     *         @SWG\Property(
     *             property="skills",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of skill names"
     *         ),
     *         @SWG\Property(
     *             property="tags",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of tag names"
     *         ),
     *         @SWG\Property(
     *             property="communities",
     *             type="array",
     *             @SWG\Items(
     *                  type="string"
     *             ),
     *             description="Array of community names to which link the project"
     *         ),
     *         @SWG\Property(
     *             property="idea",
     *             type="file",
     *             description="Hash of the idea the project comes from"
     *         )
     *     ),
     *     required=true,
     *     description="Project that should be updated"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated project"
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to update the project"
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The project does not exist"
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @View(serializerGroups={"Default", "project"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putProjectAction(Request $request, $hash){

        $this->getUser();

        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")->findOneBy(array("hash" => $hash));

        if ($project) {

            /*
             * Then, we check if the user has rights to update the project.
             * Otherwise, we send a 403 error code.
             */
            /** @var ProjectMembership $membership */
            $membership = $this->getDoctrine()
                ->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array("user" => $this->getUser(), "project" => $project));

            if( $membership !== null
                && in_array(ProjectPermissions::UPDATE_PROJECT, $membership->getRole()->getPermissions())){

                /*
                 * Sets the project logo with the actual file so that the form does not crash.
                 */
                if ($projectLogoName = $project->getLogo()) {
                    $project->setLogo(new File($this->getParameter("kernel.root_dir") . '/../web' . $this->getParameter('upload_project_logo_path') . $projectLogoName));
                }

                $form = $this->createForm(ProjectType::class, $project);
                $form->submit($request->request->all());

                if ($form->isValid()) {
                    /** @var File $logo */
                    $logo = $request->files->get("logo");

                    if ($logo !== null) {
                        $logoName = md5($logo->getFilename() . uniqid()) . '.' . $logo->guessExtension();

                        $logo->move(
                            $this->getParameter("kernel.root_dir") .
                            '/../web' .
                            $this->getParameter('upload_project_logo_path'),
                            $logoName
                        );

                        $project->setLogo($logoName);
                        $project->setLogoUrl(
                            $this->getParameter("backend_url") .
                            $this->getParameter("upload_project_logo_path") .
                            $logoName
                        );
                    }

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->view($project, 201);
                }

                return $this->view("Invalid data", 400);

            }

            return $this->view("You are not allowed to update this project", 403);

        }

        return $this->view("No project was found for hash " . $hash, 404);
    }

    /**
     * Deletes an existing project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to delete"
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when the project was successfully deleted"
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteProjectAction($hash){

        /** @var User $user */
        $user = $this->getUser();
        /** @var Project $project */
        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")->findOneBy(array("hash" => $hash));

        /*
         * At first, we check if the project exist.
         * Otherwise, we send a 404 error code.
         */
        if($project){

            /*
             * Then, we check if the user has rights to delete project.
             * Otherwise, we send a 403 error code.
             */
            /** @var ProjectMembership $membership */
            $membership = $this->getDoctrine()
                ->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array("user" => $user, "project" => $project));

            if( $membership !==null
                && in_array(ProjectPermissions::DELETE_PROJECT, $membership->getRole()->getPermissions())){
                $em = $this->getDoctrine()->getManager();
                $em->remove($project);
                $em->flush();

                $view = $this->view(null, 204);
                return $this->handleView($view);
            }

            $view = $this->view("You are not authorized to perform this action", 403);
            return $this->handleView($view);
        }

        $view = $this->view("No project was found for hash " . $hash, 404);
        return $this->handleView($view);
    }

    /**
     * Uploads a new logo for a project.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the project to delete"
     * )
     *
     * @SWG\Parameter(
     *     name="image",
     *     in="body",
     *     @SWG\Schema(
     *         type="file"
     *     ),
     *     required=true,
     *     description="New logo to upload"
     * )
     *
     * @Post("/projects/{hash}/logo")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated project with a new logo"
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @FileParam(name="image", image=true, nullable=false)
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param ParamFetcher $paramFetcher
     * @param $hash
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function postProjectLogoAction(ParamFetcher $paramFetcher, $hash){

        /** @var Project $project */
        $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")->findOneBy(array("hash" => $hash));

        if($project == null) return $this->view("No project was found for hash " . $hash, 404);

        /*
         * We make sure the user has admin rights to this project
         */

        /** @var ProjectMembership $membership */
        $membership = $this->getDoctrine()
            ->getRepository("TrevizProjectBundle:ProjectMembership")
            ->findOneBy(array("user" => $this->getUser(), "project" => $project));

        if(in_array(ProjectPermissions::UPDATE_PROJECT, $membership->getRole()->getPermissions())){

            $logo = $paramFetcher->get("image");
            $fileName = md5(uniqid()).'.'.$logo->guessExtension();

            $logo->move(
                $this->getParameter("kernel.root_dir") .
                '/../web' .
                $this->getParameter('upload_project_logo_path'),
                $fileName
            );

            $project->setLogo($fileName);
            $project->setLogoUrl(
                $this->getParameter("backend_url") .
                $this->getParameter("upload_project_logo_path") .
                $fileName
            );

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $view = $this->view($project, 200);
            return $view;
        }

        $view = $this->view("You do not have the rights to perform this action", 403);
        return $view;

    }

}