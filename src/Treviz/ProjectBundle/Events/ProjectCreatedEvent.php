<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 23:41
 */

namespace Treviz\ProjectBundle\Events;


use Symfony\Component\EventDispatcher\Event;
use Treviz\ProjectBundle\Entity\Project;

class ProjectCreatedEvent extends Event
{

    const NAME = 'project.created';

    protected $project;

    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

}