<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 23:41
 */

namespace Treviz\ProjectBundle\Events;


use Symfony\Component\EventDispatcher\Event;
use Treviz\ProjectBundle\Entity\ProjectCandidacy;

class ProjectCandidacyCreatedEvent extends Event
{

    const NAME = 'project.candidacy.created';

    protected $candidacy;

    public function __construct(ProjectCandidacy $candidacy)
    {
        $this->candidacy = $candidacy;
    }

    /**
     * @return ProjectCandidacy
     */
    public function getCandidacy(): ProjectCandidacy
    {
        return $this->candidacy;
    }

}