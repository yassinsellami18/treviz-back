<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 28/06/2018
 * Time: 13:36
 */

namespace Treviz\ProjectBundle\Services;


use Doctrine\ORM\EntityManager;
use Treviz\CoreBundle\Entity\User;
use Treviz\ProjectBundle\Entity\Project;
use Treviz\ProjectBundle\Entity\ProjectMembership;
use Treviz\ProjectBundle\Repository\ProjectMemberRepository;

class ProjectMembershipService
{

    /**
     * @var ProjectMemberRepository
     */
    private $repository;

    public function __construct(EntityManager $entityManager)
    {
        $this->repository = $entityManager->getRepository('TrevizProjectBundle:ProjectMembership');
    }

    /**
     * @param User $user
     * @param Project $project
     * @return null|ProjectMembership
     */
    public function getUserMembership(User $user, Project $project): ?ProjectMembership
    {
        return $this->repository->findOneBy(array(
            'project' => $project,
            'user' => $user
        ));
    }


    /**
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function isUserMember($user, $project): bool
    {
        return $this->getUserMembership($user, $project) !== null;
    }

}