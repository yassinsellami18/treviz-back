<?php

namespace Treviz\DocumentBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Treviz\DocumentBundle\Events\DocumentCreatedEvent;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Treviz\ProjectBundle\Entity\Project;
use Treviz\CoreBundle\Entity\User;
use Treviz\DocumentBundle\Entity\Document;
use Treviz\DocumentBundle\Form\DocumentType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Treviz\ProjectBundle\Entity\ProjectMembership;

/**
 * Class DocumentController
 * @package Treviz\DocumentBundle\Controller
 *
 * @Security("has_role('ROLE_USER')")
 */
class DocumentController extends FOSRestController 
{

    /**
     * Fetches a document according to its hash.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the document identified by its hash",
     * )
     *
     * @SWG\Tag(name="documents")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getDocumentAction($hash)
    {

        /** @var Document $doc */
        $doc = $this->getDoctrine()->getRepository("TrevizDocumentBundle:Document")->findOneByHash($hash);

        if ($doc !== null) {

            if ($project = $doc->getProject()) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $project,
                        "user" => $this->getUser()
                    ));

                if ($membership !== null || $project->isPublic()) {

                    return $this->view($doc, 200);

                }

                return $this->view("You cannot view this document", 200);

            }

            if ($community = $doc->getCommunity()) {

                $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                    ->findOneBy(array(
                        "community" => $community,
                        "user" => $this->getUser()
                    ));

                if ($membership !== null || $community->isPublic()) {

                    return $this->view($doc, 200);

                }

                return $this->view("You cannot view this document", 200);

            }

            if ($idea = $doc->getIdea()) {

                $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                    ->findOneBy(array(
                        "community" => $idea->getSession()->getCommunity(),
                        "user" => $this->getUser()
                    ));

                if ($membership !== null || $community->isPublic()) {

                    return $this->view($doc, 200);

                }

                return $this->view("You cannot view this document", 200);

            }

            if ($room = $doc->getRoom()) {

                if ($room->getUsers()->contains($this->getUser())) {

                    return $this->view($doc, 200);

                }

                return $this->view("You cannot view this document", 200);

            }

            if ($doc->getOwner() == $this->getUser()) {

                return $this->view($doc, 200);

            }

            return $this->view("You cannot view this document", 200);

        }

        throw new HttpException(404, "No document found for this hash");


    }

    /**
     * Fetches documents according to query parameters.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of documents matching the query parameter",
     * )
     *
     * @SWG\Tag(name="documents")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @QueryParam(name="project", description="hash of the project to which the documents belong")
     * @QueryParam(name="community", description="hash of the community to which the documents belong")
     * @QueryParam(name="idea", description="hash of the idea to which the documents belong")
     * @QueryParam(name="room", description="hash of the room to which the documents belong")
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getDocumentsAction(ParamFetcher $paramFetcher)
    {
        /** @var Project $project */
        $s_project = $paramFetcher->get("project");
        $s_community = $paramFetcher->get("community");
        $s_idea = $paramFetcher->get("idea");
        $s_room = $paramFetcher->get("room");

        /** @var User $user */
        $user = $this->getUser();

        $qb = $this->getDoctrine()->getRepository("TrevizDocumentBundle:Document")->createQueryBuilder("document");

        if ($s_project) {

            $qb->join("document.project", "project");
            $qb->andWhere("project.hash = :project_hash");
            $qb->setParameter("project_hash", $s_project);

            if ($user == null) {
                $qb->andWhere("project.public = true");
            } else {
                $qb->andWhere("project.public = true
                               OR EXISTS(
                                SELECT pm
                                FROM TrevizProjectBundle:ProjectMembership pm
                                WHERE pm.project = project
                                AND pm.user = :user
                               )
                               OR EXISTS(
                                SELECT cm
                                FROM TrevizCommunityBundle:CommunityMembership cm
                                WHERE cm.community MEMBER OF project.communities
                                AND cm.user = :user
                               )");
            }

        } else {
            $qb->andWhere("document.project IS NULL");
        }

        if ($s_community)
        {
            $qb->join("document.community", "community");
            $qb->andWhere("community.hash = :community_hash");
            $qb->setParameter("community_hash", $s_community);

            if ($user == null) {
                $qb->andWhere("community.public = true");
            } else {
                $qb->andWhere("community.public = true
                                OR EXISTS(
                                    SELECT cm
                                    FROM TrevizCommunityBundle:CommunityMembership cm
                                    WHERE cm.community = community
                                    AND cm.user = :user
                                )");
            }

        } else {
            $qb->andWhere("document.community IS NULL");
        }


        if ($s_idea) {

            $qb->join("document.idea", "idea");
            $qb->andWhere("idea.hash = :idea_hash");
            $qb->setParameter("idea_hash", $s_community);

            if ($user !== null) {

            } else {
                $qb->andWhere("EXISTS (
                                SELECT cm
                                FROM TrevizCommunityBundle:CommunityMembership cm
                                JOIN cm.community = community
                                WHERE cm.user = :user
                                AND idea.session MEMBER OF community.brainstormingSessions
                            )");
            }

        } else {
            $qb->andWhere("document.idea IS NULL");
        }

        if ($s_room) {

            $qb->join("document.room", "room");
            $qb->andWhere("room.hash = :room_hash");
            $qb->setParameter("room_hash", $s_community);

            if ($user !== null) {
                return $this->view([], 200);
            } else {
                $qb->andWhere("room.hash = :room_hash
                                AND :user MEMBER OF room.users
                                ");
                $qb->setParameter("room_hash", $s_room);
            }

        } else {
            $qb->andWhere("document.room IS NULL");
        }

        if ($user) {
            $qb->setParameter("user", $user);
        }

        return $this->view($qb->getQuery()->getResult(), 200);

    }

    /**
     * Creates a new document.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created document",
     * )
     *
     * @SWG\Tag(name="documents")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postDocumentAction(Request $request)
    {

        $document = new Document();
        $form = $this->createForm(DocumentType::class, $document);
        $form->submit($request->request->all());
        $dispatcher = $this->get('treviz_core.event_dispatcher.dispatcher');

        if ($form->isValid()) {

            /** @var File $file */
            $file = $request->files->get("file");

            $fileExtension = $file->guessExtension();
            $fileName = md5($file->getFilename() . uniqid()) . '.' . $fileExtension;


            $file->move(
                $this->getParameter("kernel.root_dir") .
                '/../web' .
                $this->getParameter('upload_files'),
                $fileName
            );

            $document->setFileUrl(
                $this->getParameter("backend_url") .
                $this->getParameter("upload_files") .
                $fileName
            );
            $document->setExtension($fileExtension);
            $document->setHash($fileName);

            $user = $this->getUser();

            $document->setOwner($user);

            /*
             * Checks if the user is part of the project or community.
             */
            $em = $this->getDoctrine()->getManager();

            if ($project = $document->getProject()) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "user" => $this->getUser(),
                        "project" => $project
                        ));

                if ($membership !== null) {
                    $em->persist($document);
                    $em->flush();

                    $event = new DocumentCreatedEvent($document);
                    $dispatcher->dispatch(DocumentCreatedEvent::NAME, $event);

                    return $this->view($document, 200);
                }

                throw new HttpException(403, "You are not authorized to do this");

            } elseif ($community = $document->getCommunity()) {

                $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                    ->findOneBy(array(
                        "user" => $this->getUser(),
                        "community" => $community
                    ));

                if ($membership !== null) {
                    $em->persist($document);
                    $em->flush();

                    $event = new DocumentCreatedEvent($document);
                    $dispatcher->dispatch(DocumentCreatedEvent::NAME, $event);

                    return $this->view($document, 200);
                }

                throw new HttpException(403, "You are not authorized to do this");

            } elseif ($room = $document->getRoom()) {

                if ($user->isRoomMember($room)) {
                    $em->persist($document);
                    $em->flush();

                    return $this->view($document, 200);
                }

                throw new HttpException(403, "You are not authorized to do this");

            } elseif ($idea = $document->getIdea()) {

                if( $project = $idea->getSession()->getProject() ) {
                    $membership = $this->getDoctrine()->getRepository('TrevizProjectBundle:ProjectMembership')
                        ->findOneBy(array(
                            'user' => $this->getUser(),
                            'project' => $project
                        ));
                } elseif ($community = $idea->getSession()->getCommunity()) {
                    $membership = $this->getDoctrine()->getRepository('TrevizCommunityBundle:CommunityMembership')
                        ->findOneBy(array(
                            'user' => $this->getUser(),
                            'community' => $community
                        ));
                }

                if (isset($membership)) {
                    $em->persist($document);
                    $em->flush();

                    $event = new DocumentCreatedEvent($document);
                    $dispatcher->dispatch(DocumentCreatedEvent::NAME, $event);

                    return $this->view($document, 201);
                }

            }

            throw new HttpException(400, "You must specify a project, community, idea or room number");

        }

        throw new HttpException(422, $form);

    }

    /**
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the document was successfully deleted",
     * )
     *
     * @SWG\Tag(name="documents")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteDocumentAction($hash)
    {

        /** @var Document $document */
        $document = $this->getDoctrine()->getRepository("TrevizDocumentBundle:Document")->findOneByHash($hash);

        if ($doc =! null) {

            $em = $this->getDoctrine()->getManager();

            if($document->getOwner() === $this->getUser()) {

                $em->remove($document);
                $em->flush();

                return $this->view(null, 204);

            } elseif ($project = $document->getProject()) {

                /** @var ProjectMembership $membership */
                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "user" => $this->getUser(),
                        "project" => $project
                    ));

                if ($membership !== null ||
                    in_array(ProjectPermissions::MANAGE_DOCUMENT, $membership->getRole()->getPermissions())) {

                    $em->remove($document);
                    $em->flush();

                    return $this->view(null, 204);

                }

            } elseif ($community = $document->getCommunity()) {

                /** @var CommunityMembership $membership */
                $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                    ->findOneBy(array(
                        "user" => $this->getUser(),
                        "community" => $community
                    ));

                if ($membership != null ||
                    in_array(CommunityPermissions::MANAGE_DOCUMENT, $membership->getRole()->getPermissions())) {

                    $em->remove($document);
                    $em->flush();

                    return $this->view(null, 204);

                }

            } elseif ($idea = $document->getIdea()) {

                if( $project = $idea->getSession()->getProject() ) {
                    /** @var ProjectMembership $membership */
                    $membership = $this->getDoctrine()->getRepository('TrevizProjectBundle:ProjectMembership')
                        ->findOneBy(array(
                            'user' => $this->getUser(),
                            'project' => $project
                        ));

                    if ($membership !== null ||
                        in_array(ProjectPermissions::MANAGE_DOCUMENT, $membership->getRole()->getPermissions())) {

                        $em->remove($document);
                        $em->flush();

                        return $this->view(null, 204);

                    }

                } elseif ($community = $idea->getSession()->getCommunity()) {
                    /** @var CommunityMembership $membership */
                    $membership = $this->getDoctrine()->getRepository('TrevizCommunityBundle:CommunityMembership')
                        ->findOneBy(array(
                            'user' => $this->getUser(),
                            'community' => $community
                        ));

                    if ($membership != null ||
                        in_array(CommunityPermissions::MANAGE_DOCUMENT, $membership->getRole()->getPermissions())) {

                        $em->remove($document);
                        $em->flush();

                        return $this->view(null, 204);

                    }
                }

            }

            throw new HttpException(403, "You are not authorized to do this");

        }

        throw new HttpException(404, "No document found for this hash");

    }

}
