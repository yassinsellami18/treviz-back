<?php

namespace Treviz\DocumentBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\BrainstormingBundle\Form\DataTransformer\HashToIdeaTransformer;
use Treviz\ChatBundle\Form\DataTransformer\IdToRoomTransformer;
use Treviz\CommunityBundle\Form\DataTransformer\HashToCommunityTransformer;
use Treviz\ProjectBundle\Form\DataTransformer\HashToProjectTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentType extends AbstractType
{

    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('description')
            ->add('file')
            ->add('project', TextType::class, array("required" => false))
            ->add('community', TextType::class, array("required" => false))
            ->add('idea', TextType::class, array("required" => false))
            ->add('room', TextType::class, array("required" => false));

        $builder->get('project')
            ->addModelTransformer(new HashToProjectTransformer($this->em), true);
        $builder->get('community')
            ->addModelTransformer(new HashToCommunityTransformer($this->em), true);
        $builder->get('idea')
            ->addModelTransformer(new HashToIdeaTransformer($this->em), true);
        $builder->get('room')
            ->addModelTransformer(new IdToRoomTransformer($this->em), true);

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\DocumentBundle\Entity\Document',
            'csrf_protection' => false
        ));
    }


}
