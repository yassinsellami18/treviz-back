<?php

namespace Treviz\DocumentBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\DocumentBundle\Entity\Document;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * Document: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToDocumentTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * HashToDocumentTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Document $document
     * @return string
     */
    public function transform($document)
    {
        if($document == null) {
            return '';
        }

        return $document->getHash();
    }

    /**
     * @param string $hash
     * @return Document|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $document = $this->em->getRepository("TrevizDocumentBundle:Document")->findOneByHash($hash);

        if ($document == null) {
            throw new TransformationFailedException(sprintf('No document with hash ' . $hash . ' exists.'));
        }

        return $document;

    }
}