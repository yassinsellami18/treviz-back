<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 23:41
 */

namespace Treviz\DocumentBundle\Events;


use Symfony\Component\EventDispatcher\Event;
use Treviz\DocumentBundle\Entity\Document;

class DocumentCreatedEvent extends Event
{

    const NAME = 'document.created';

    protected $document;

    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    /**
     * @return Document
     */
    public function getDocument(): Document
    {
        return $this->document;
    }

}