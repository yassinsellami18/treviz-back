<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 28/10/2017
 * Time: 17:43
 */

namespace Treviz\CommunityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Treviz\CommunityBundle\Entity\Superclass\Hashable;

/**
 * Community Role
 *
 * @ORM\Table(name="community_role")
 * @ORM\Entity(repositoryClass="Treviz\CommunityBundle\Repository\CommunityRoleRepository")
 */
class CommunityRole extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @var array
     *
     * @ORM\Column(name="permissions", type="array")
     */
    private $permissions;

    /**
     * @var Community
     *
     * @ORM\ManyToOne(targetEntity="Community", inversedBy="roles")
     * @JMS\Exclude()
     */
    private $community;

    /**
     * If true, this role will be available to all users.
     *
     * @var bool
     *
     * @ORM\Column(name="global", type="boolean")
     */
    private $global = false;

    /**
     * If true, the role is affected by default to all project creators.
     *
     * @var bool
     *
     * @ORM\Column(name="default_creator", type="boolean")
     */
    private $defaultCreator = false;

    /**
     * If true, the role is affected by default to all project members other than the creator.
     *
     * @var bool
     *
     * @ORM\Column(name="default_member", type="boolean")
     */
    private $defaultMember = false;

    public function __construct()
    {
        $this->permissions = [];
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }


    /**
     * @return array
     */
    public function getPermissions(): ?array
    {
        return $this->permissions;
    }

    /**
     * @param array $permissions
     */
    public function setPermissions(array $permissions)
    {
        foreach ($permissions as $permission) {
            $this->addPermission($permission);
        }

    }

    /**
     * @param string $permission
     */
    public function addPermission(string $permission)
    {
        if (in_array($permission, CommunityPermissions::getAvailablePermissions())
            && !in_array($permission, $this->permissions) ) {
            $this->permissions[] = $permission;
        }
    }

    /**
     * @param string $permission
     */
    public function removePermission(string $permission)
    {
        $this->permissions = array_diff($this->permissions, [$permission]);
    }

    /**
     * @return Community
     */
    public function getCommunity(): ?Community
    {
        return $this->community;
    }

    /**
     * @param Community $community
     */
    public function setCommunity(Community $community)
    {
        $this->community = $community;
        if(!$community->getRoles()->contains($this)){
            $community->addRole($this);
        }
    }

    /**
     * @return boolean
     */
    public function isGlobal(): ?bool
    {
        return $this->global;
    }

    /**
     * @param boolean $global
     */
    public function setGlobal(bool $global)
    {
        $this->global = $global;
    }

    /**
     * @return boolean
     */
    public function isDefaultCreator(): ?bool
    {
        return $this->defaultCreator;
    }

    /**
     * @param boolean $defaultCreator
     */
    public function setDefaultCreator(bool $defaultCreator)
    {
        $this->defaultCreator = $defaultCreator;
    }

    /**
     * @return boolean
     */
    public function isDefaultMember(): ?bool
    {
        return $this->defaultMember;
    }

    /**
     * @param boolean $defaultMember
     */
    public function setDefaultMember(bool $defaultMember)
    {
        $this->defaultMember = $defaultMember;
    }

}