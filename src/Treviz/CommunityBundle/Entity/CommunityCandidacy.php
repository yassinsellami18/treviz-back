<?php

namespace Treviz\CommunityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Treviz\CommunityBundle\Entity\Superclass\Hashable;
use Treviz\CoreBundle\Entity\User;

/**
 * CommunityCandidacy
 *
 * @ORM\Table(name="community_candidacy")
 * @ORM\Entity(repositoryClass="Treviz\CommunityBundle\Repository\CommunityCandidacyRepository")
 */
class CommunityCandidacy extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var Community
     *
     * @ORM\ManyToOne(targetEntity="Treviz\CommunityBundle\Entity\Community", inversedBy="candidacies", cascade={"persist"})
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"user"})
     */
    private $community;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Treviz\CoreBundle\Entity\User", inversedBy="communitiesCandidacies", cascade={"persist"})
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"community"})
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Community
     */
    public function getCommunity(): ?Community
    {
        return $this->community;
    }

    /**
     * @param Community $community
     */
    public function setCommunity(Community $community)
    {
        $this->community = $community;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

}

