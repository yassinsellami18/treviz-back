<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 25/06/2018
 * Time: 23:00
 */

namespace Treviz\CommunityBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Treviz\CommunityBundle\Repository\CommunityNotificationPreferencesRepository")
 * @ORM\Table(name="community_notification_preferences")
 */
class CommunityNotificationPreferences
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a new project is linked to the community.
     */
    private $onNewProject = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a brainstorming session is created.
     */
    private $onNewBrainstorming = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when someones candidates to the project.
     */
    private $onCandidacy = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a new post is created.
     */
    private $onPost = true;

    /**
     * @var boolean
     *
     * Should an e-mail be sent when a new document is created in the project.
     */
    private $onNewDocument = true;

    /**
     * @var CommunityMembership
     *
     * Membership those preferences are linked to.
     *
     * @ORM\OneToOne(targetEntity="Treviz\CommunityBundle\Entity\CommunityMembership", cascade={"persist"})
     * @JMS\Exclude()
     */
    private $communityMembership;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isOnNewProject(): bool
    {
        return $this->onNewProject;
    }

    /**
     * @param bool $onNewProject
     */
    public function setOnNewProject(bool $onNewProject): void
    {
        $this->onNewProject = $onNewProject;
    }

    /**
     * @return bool
     */
    public function isOnNewBrainstorming(): bool
    {
        return $this->onNewBrainstorming;
    }

    /**
     * @param bool $onNewBrainstorming
     */
    public function setOnNewBrainstorming(bool $onNewBrainstorming): void
    {
        $this->onNewBrainstorming = $onNewBrainstorming;
    }

    /**
     * @return bool
     */
    public function isOnCandidacy(): bool
    {
        return $this->onCandidacy;
    }

    /**
     * @param bool $onCandidacy
     */
    public function setOnCandidacy(bool $onCandidacy): void
    {
        $this->onCandidacy = $onCandidacy;
    }

    /**
     * @return bool
     */
    public function isOnPost(): bool
    {
        return $this->onPost;
    }

    /**
     * @param bool $onPost
     */
    public function setOnPost(bool $onPost): void
    {
        $this->onPost = $onPost;
    }

    /**
     * @return bool
     */
    public function isOnNewDocument(): bool
    {
        return $this->onNewDocument;
    }

    /**
     * @param bool $onNewDocument
     */
    public function setOnNewDocument(bool $onNewDocument): void
    {
        $this->onNewDocument = $onNewDocument;
    }

    /**
     * @return CommunityMembership
     */
    public function getCommunityMembership(): CommunityMembership
    {
        return $this->communityMembership;
    }

    /**
     * @param CommunityMembership $communityMembership
     */
    public function setCommunityMembership(
        CommunityMembership $communityMembership
    ): void {
        $this->communityMembership = $communityMembership;
    }

}