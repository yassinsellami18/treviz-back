<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 29/10/2017
 * Time: 12:02
 */

namespace Treviz\CommunityBundle\Entity\Enums;

/**
 * Class CommunityPermissions
 * List the various operations that can be performed inside a community.
 *
 * @package Treviz\CommunityBundle\Entity\Enums
 */
class CommunityPermissions
{

    /*
     * Manage Community description
     */
    CONST UPDATE_COMMUNITY = "UPDATE_COMMUNITY";
    CONST DELETE_COMMUNITY = "DELETE_COMMUNITY";


    /*
     * Manage Community roles
     */
    CONST MANAGE_ROLE = "MANAGE_ROLE";

    /*
     * Manage members
     */
    CONST MANAGE_MEMBERSHIP = "MANAGE_MEMBERSHIP";

    /*
     * Manage posts
     */
    CONST MANAGE_POST = "MANAGE_POST";

    /*
     * Manage candidacies
     */
    CONST MANAGE_CANDIDACIES = "MANAGE_CANDIDACIES";

    /*
     * Manage invitations
     */
    CONST MANAGE_INVITATIONS = "MANAGE_INVITATIONS";

    /*
     * Manage Documents
     */
    CONST MANAGE_DOCUMENT = "MANAGE_DOCUMENT";

    /*
     * Manage brainstorming sessions
     */
    CONST MANAGE_BRAINSTORMING_SESSION = "MANAGE_BRAINSTORMING_SESSION";
    CONST MANAGE_BRAINSTORMING_IDEAS = "MANAGE_BRAINSTORMING_IDEAS";

    /** @var array  */
    protected static $permissionName = [
        self::UPDATE_COMMUNITY => "Update Community",
        self::DELETE_COMMUNITY => "Delete Community",
        self::MANAGE_ROLE => "Manage Role",
        self::MANAGE_MEMBERSHIP => "Manage Membership",
        self::MANAGE_POST => "Manage Post",
        self::MANAGE_CANDIDACIES => "Manage Candidacies",
        self::MANAGE_INVITATIONS => "Manage Invitations",
        self::MANAGE_DOCUMENT => "Manage Document",
        self::MANAGE_BRAINSTORMING_SESSION => "Manage Brainstorming session",
        self::MANAGE_BRAINSTORMING_IDEAS => "Manage Brainstorming ideas",
    ];

    /**
     * @param $permissionShortName
     * @return mixed|string
     * @internal param $roleShortName
     */
    public static function getPermissionName($permissionShortName): string
    {
        if(!isset(static::$permissionName[$permissionShortName])){
            return "Unknown permission ($permissionShortName)";
        }

        return static::$permissionName[$permissionShortName];
    }

    public static function getAvailablePermissions(): array
    {
        return [
            self::UPDATE_COMMUNITY,
            self::DELETE_COMMUNITY,
            self::MANAGE_ROLE,
            self::MANAGE_MEMBERSHIP,
            self::MANAGE_POST,
            self::MANAGE_CANDIDACIES,
            self::MANAGE_INVITATIONS,
            self::MANAGE_DOCUMENT,
            self::MANAGE_BRAINSTORMING_SESSION,
            self::MANAGE_BRAINSTORMING_IDEAS
        ];
    }

}