<?php

namespace Treviz\CommunityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Treviz\CommunityBundle\Entity\Superclass\Hashable;
use Treviz\CoreBundle\Entity\User;

/**
 * CommunityMembership
 *
 * @ORM\Table(name="community_membership")
 * @ORM\Entity(repositoryClass="Treviz\CommunityBundle\Repository\CommunityMembershipRepository")
 */
class CommunityMembership extends Hashable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var CommunityRole $role
     *
     * @ORM\ManyToOne(targetEntity="Treviz\CommunityBundle\Entity\CommunityRole", cascade={"persist"})
     */
    private $role;

    /**
     * @var Community $community
     *
     * @ORM\ManyToOne(targetEntity="Treviz\CommunityBundle\Entity\Community", inversedBy="memberships", cascade={"persist"})
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"user"})
     */
    private $community;

    /**
     * @var User $user
     *
     * @ORM\ManyToOne(targetEntity="Treviz\CoreBundle\Entity\User", inversedBy="communitiesMemberships", cascade={"persist"})
     * @JMS\MaxDepth(5)
     * @JMS\Groups({"community"})
     */
    private $user;

    /**
     * @var CommunityNotificationPreferences
     *
     * @ORM\OneToOne(targetEntity="Treviz\CommunityBundle\Entity\CommunityNotificationPreferences", cascade={"persist"})
     * @JMS\Exclude()
     */
    private $preferences;

    /**
     * CommunityMembership constructor.
     */
    public function __construct()
    {
        $this->preferences = new CommunityNotificationPreferences();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return CommunityRole
     */
    public function getRole(): ?CommunityRole
    {
        return $this->role;
    }

    /**
     * @param CommunityRole $role
     */
    public function setRole(CommunityRole $role)
    {
        $this->role = $role;
    }

    /**
     * @return Community
     */
    public function getCommunity(): ?Community
    {
        return $this->community;
    }

    /**
     * @param Community $community
     */
    public function setCommunity(Community $community)
    {
        $this->community = $community;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return CommunityNotificationPreferences
     */
    public function getPreferences(): CommunityNotificationPreferences
    {
        return $this->preferences;
    }

    /**
     * @param CommunityNotificationPreferences $preferences
     */
    public function setPreferences(CommunityNotificationPreferences $preferences
    ): void {
        $this->preferences = $preferences;
    }

}

