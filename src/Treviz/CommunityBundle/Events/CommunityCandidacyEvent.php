<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 23:41
 */

namespace Treviz\CommunityBundle\Events;


use Symfony\Component\EventDispatcher\Event;
use Treviz\CommunityBundle\Entity\CommunityCandidacy;

class CommunityCandidacyEvent extends Event
{

    const NAME = 'community.candidacy';

    protected $candidacy;

    public function __construct(CommunityCandidacy $candidacy)
    {
        $this->candidacy = $candidacy;
    }

    /**
     * @return CommunityCandidacy
     */
    public function getCandidacy(): CommunityCandidacy
    {
        return $this->candidacy;
    }

}