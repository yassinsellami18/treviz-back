<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 28/06/2018
 * Time: 13:36
 */

namespace Treviz\CommunityBundle\Services;


use Doctrine\ORM\EntityManager;
use Treviz\CommunityBundle\Entity\Community;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CommunityBundle\Repository\CommunityMembershipRepository;
use Treviz\CoreBundle\Entity\User;


class CommunityMembershipService
{

    /**
     * @var CommunityMembershipRepository
     */
    private $repository;

    public function __construct(EntityManager $entityManager)
    {
        $this->repository = $entityManager->getRepository('TrevizCommunityBundle:CommunityMembership');
    }

    /**
     * @param User $user
     * @param Community $community
     * @return null|CommunityMembership
     */
    public function getUserMembership(User $user, Community $community): ?CommunityMembership
    {
        return $this->repository->findOneBy(array(
            'community' => $community,
            'user' => $user
        ));
    }


    /**
     * @param User $user
     * @param Community $community
     * @return bool
     */
    public function isUserMember(User $user, Community $community): bool
    {
        return $this->getUserMembership($user, $community) !== null;
    }

}