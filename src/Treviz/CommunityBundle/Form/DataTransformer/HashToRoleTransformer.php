<?php

namespace Treviz\CommunityBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\CommunityBundle\Entity\CommunityRole;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToRoleTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param CommunityRole $role
     * @return string
     */
    public function transform($role)
    {
        if($role == null) {
            return '';
        }

        return $role->getHash();
    }

    /**
     * @param string $hash
     * @return CommunityRole|null
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $role = $this->em->getRepository("TrevizCommunityBundle:CommunityRole")->findOneBy(array("hash" => $hash));

        if ($role == null) {
            throw new TransformationFailedException(sprintf('No role with hash ' . $hash . ' exists.'));
        }

        return $role;

    }
}