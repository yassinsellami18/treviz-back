<?php

namespace Treviz\CommunityBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\CommunityBundle\Entity\Community;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToCommunityTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * IdToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Community $community
     * @return string
     */
    public function transform($community): string
    {
        if($community == null) {
            return '';
        }

        return $community->getHash();
    }

    /**
     * @param string $hash
     * @return Community|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $community = $this->em->getRepository("TrevizCommunityBundle:Community")->findOneBy(array("hash" => $hash));

        if ($community == null) {
            throw new TransformationFailedException(sprintf('No community with hash ' . $hash . ' exists.'));
        }

        return $community;

    }
}