<?php

namespace Treviz\CommunityBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\CommunityBundle\Entity\CommunityCandidacy;
use Treviz\CommunityBundle\Entity\CommunityRole;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToCandidacyTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param CommunityCandidacy $candidacy
     * @return string
     */
    public function transform($candidacy)
    {
        if($candidacy == null) {
            return '';
        }

        return $candidacy->getHash();
    }

    /**
     * @param string $hash
     * @return CommunityCandidacy|null
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $candidacy = $this->em->getRepository("TrevizCommunityBundle:CommunityCandidacy")->findOneBy(array("hash" => $hash));

        if ($candidacy == null) {
            throw new TransformationFailedException(sprintf('No candidacy with hash ' . $hash . ' exists.'));
        }

        return $candidacy;

    }
}