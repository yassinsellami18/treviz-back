<?php

namespace Treviz\CommunityBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\CoreBundle\Form\DataTransformer\UsernameToUserTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommunityInvitationType extends AbstractType
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * CommunityInvitationType constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message')
            ->add('user', TextType::class, array("required" => true));

        $builder->get('user')->addModelTransformer(new UsernameToUserTransformer($this->em));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\CommunityBundle\Entity\CommunityInvitation',
            'csrf_protection' => false
        ));
    }

}
