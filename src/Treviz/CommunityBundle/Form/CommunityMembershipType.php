<?php

namespace Treviz\CommunityBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\CommunityBundle\Form\DataTransformer\HashToRoleTransformer;
use Treviz\CoreBundle\Form\DataTransformer\UsernameToUserTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommunityMembershipType extends AbstractType
{

    private $em;

    /**
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('role')
            ->add('user');

        $builder->get('role')->addModelTransformer(new HashToRoleTransformer($this->em), true);
        $builder->get('user')->addModelTransformer(new UsernameToUserTransformer($this->em), true);

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\CommunityBundle\Entity\CommunityMembership',
            'csrf_protection' => false
        ));
    }


}
