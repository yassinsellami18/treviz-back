<?php

namespace Treviz\CommunityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommunityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array("required" => true))
            ->add('isPublic', TextType::class, array("required" => true, 'property_path' => 'public'))
            ->add('open', TextType::class, array("required" => false))
            ->add('description', TextType::class, array("required" => true))
            ->add('website', TextType::class, array("required" => false))
            ->add('logo')
            ->add('logoUrl', TextType::class, array("required" => false))
            ->add('backgroundImage')
            ->add('backgroundImageUrl', TextType::class, array("required" => false));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\CommunityBundle\Entity\Community',
            'csrf_protection' => false
        ));
    }

}
