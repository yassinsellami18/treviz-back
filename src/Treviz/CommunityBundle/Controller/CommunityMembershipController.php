<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 31/10/2017
 * Time: 00:29
 */

namespace Treviz\CommunityBundle\Controller;

use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CommunityBundle\Entity\CommunityCandidacy;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CommunityBundle\Entity\CommunityRole;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Treviz\CommunityBundle\Form\CommunityMembershipType;
use Treviz\CoreBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Swagger\Annotations as SWG;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class CommunityMembershipController
 * @package Treviz\CommunityBundle\Controller
 */
class CommunityMembershipController extends FOSRestController
{

    /**
     * Fetches the memberships of a community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the memberships of a specified community",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getCommunityMembershipsAction($hash)
    {

        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if ($community) {

            /*
             * If the community is public, return its memberships.
             * Otherwise, check if the user has access to it.
             */
            if($community->isPublic()) {
                $view = $this->view($community->getMemberships(), 200);
                return $view;
            } else {

                /** @var User $user */
                $user = $this->getUser();

                /*
                 * Check if the user is member of the community
                 */
                if(array_intersect($community->getMemberships()->toArray(), $user->getCommunitiesMemberships()->toArray())) {

                    $view = $this->view($community->getMemberships(), 200);
                    return $view;

                }

                $view = $this->view("You can't see the memberships of this community", 403);
                return $view;

            }

        }

        $view = $this->view("No community was found for this hash", 400);
        return $view;

    }

    /**
     * Fetches the communities of a user specified in the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the memberships specified by the query",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @QueryParam(name="user", description="username of the user from whom fetch memberships", nullable=false)
     *
     * @View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getCommunitiesMembershipsAction(ParamFetcher $paramFetcher)
    {

        /** @var User $user */
        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")
            ->findOneBy(array("username" => $paramFetcher->get("user")));

        if ($user != null) {

            $currentUser = $this->getUser();

            if ($user == $currentUser){
                return $this->view($user->getCommunitiesMemberships(), 200);
            } else {

                /** @var QueryBuilder $qb */
                $qb = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")->createQueryBuilder("cm");
                $qb->join("cm.community", "c");

                $qb->andWhere("cm.user = :user");
                $qb->setParameter('user', $user);

                /*
                 * If the user making the query has not sent any authentication header, only return the public communities.
                 * Otherwise, return the public communities and the communities in which the current user is member.
                 */
                if ($currentUser == null) {

                    $qb->andWhere("c.public = true");
                    return $this->view($qb->getQuery()->getResult(), 200);

                } else {

                    $qb->andWhere("c.public = true
                                   OR EXISTS(
                                    SELECT cm2
                                    FROM TrevizCommunityBundle:CommunityMembership cm2
                                    WHERE cm2.user = :currentUser
                                    AND cm2.community = c
                                   )");
                    $qb->setParameter("currentUser", $currentUser);

                    return $this->view($qb->getQuery()->getResult(), 200);

                }

            }

        }

        $view = $this->view([], 200);
        return $view;

    }

    /**
     * Add a new member to a community.
     * If the community is public, one user can create its own membership. His role will therefore be the default one.
     * If the user has been invited, he or she can also create its own membership, with default role.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the created membership",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postCommunityMembershipAction(Request $request, $hash)
    {

        $currentUser = $this->getUser();
        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")->findOneBy(array("hash" => $hash));

        if ($community != null) {

            $currentUserMembership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $currentUser
                ));

            /*
             * If the user who wants to create the membership is not a member of the project,
             * it must either follow an invitation he or she accepts, or the community must be open.
             */
            if ($currentUserMembership == null) {

                $invitation = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityInvitation")
                    ->findOneBy(array("hash" => $request->request->get("invitation")));

                if (($invitation !== null
                    && $invitation->getUser() == $currentUser
                    && $invitation->getCommunity() == $community)
                || $community->isOpen()) {

                    $membership = new CommunityMembership();

                    /*
                     * Create the hash for this membership.
                     */
                    $membershipHash = hash("sha256", random_bytes(256));

                    while ($this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                            ->findOneBy(array("hash" => $membershipHash)) !== null) {
                        $membershipHash = hash("sha256", random_bytes(256));
                    }

                    /*
                     * Fetches the default member role.
                     * If there is not, create some.
                     */
                    $defaultMemberRole = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityRole")
                        ->findOneBy(array("defaultMember" => true, "global" => true));

                    if ($defaultMemberRole == null) {
                        $defaultMemberRole = new CommunityRole();
                        $defaultMemberRole->setName("Member");
                        $defaultMemberRole->setDefaultMember(true);
                        $defaultMemberRole->setHash(hash("sha256", random_bytes(256)));
                        $community->addRole($defaultMemberRole);
                    }

                    $membership->setHash($membershipHash);
                    $membership->setCommunity($community);
                    $membership->setRole($defaultMemberRole);
                    $membership->setUser($currentUser);

                    $em = $this->getDoctrine()->getManager();
                    if($invitation) { $em->remove($invitation); }
                    $em->persist($membership);
                    $em->flush();

                    return $this->view($membership, 200);

                }

                return $this->view("You cannot perform this action", 403);

            } else {

                $candidacy = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityCandidacy")
                    ->findOneBy(array("hash" => $request->request->get("candidacy")));

                if($candidacy !== null
                && in_array(CommunityPermissions::MANAGE_CANDIDACIES, $currentUserMembership->getRole()->getPermissions())) {

                    /*
                     * Checks if there is no conflict with an existing member.
                     */
                    $userCurrentMembership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                        ->findOneBy(array(
                            "community" => $community,
                            "user" => $candidacy->getUser()
                        ));

                    if ($userCurrentMembership == null) {

                        $membership = new CommunityMembership();

                        $membershipHash = hash("sha256", random_bytes(256));

                        while ($this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                                ->findOneBy(array("hash" => $membershipHash)) != null) {
                            $membershipHash = hash("sha256", random_bytes(256));
                        }

                        $membership->setHash($membershipHash);
                        $membership->setCommunity($community);
                        $membership->setUser($candidacy->getUser());

                        $role = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityRole")
                            ->findOneBy(array("hash" => $request->request->get("role")));

                        if ($role->isGlobal() || $community->getRoles()->contains($role)) {
                            $membership->setRole($role);

                            $em = $this->getDoctrine()->getManager();
                            $em->persist($membership);
                            $em->remove($candidacy);
                            $em->flush();

                            return $this->view($membership, 200);
                        }

                        return $this->view("Invalid role", 422);

                    }

                    return $this->view("An existing membership already exist between this user and this community", 409);

                }

                return $this->view("You are not authorized to do this", 403);

            }

        }

        return $this->view("No community was found matching hash " . $hash, 404);

    }

    /**
     * Updates an existing membership of a community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated membership",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Put("/communities/memberships/{hash}")
     *
     * @View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putCommunityMembershipAction(Request $request, $hash)
    {

        $currentUser = $this->getUser();

        /** @var CommunityMembership $currentMembership */
        $currentMembership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
            ->findOneBy(array("hash" => $hash));

        if ($currentMembership != null) {

            $community = $currentMembership->getCommunity();

            $currentUserMembership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $currentUser
                ));

            if( $currentUserMembership != null
                && in_array(CommunityPermissions::MANAGE_MEMBERSHIP, $currentUserMembership->getRole()->getPermissions())) {

                $form = $this->createForm(CommunityMembershipType::class, $currentMembership);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                }

                return $this->view($form, 422);

            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No membership was found matching hash " . $hash, 404);

    }

    /**
     * Removes a user from a community.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when a membership was successfully deleted",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Delete("/communities/memberships/{hash}")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteCommunityMembershipAction($hash)
    {
        $currentUser = $this->getUser();

        /** @var CommunityMembership $currentMembership */
        $currentMembership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
            ->findOneBy(array("hash" => $hash));

        if ($currentMembership != null) {

            $community = $currentMembership->getCommunity();

            $currentUserMembership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $currentUser
                ));

            if( $currentUserMembership != null
                && in_array(CommunityPermissions::MANAGE_MEMBERSHIP, $currentUserMembership->getRole()->getPermissions())) {

                $em = $this->getDoctrine()->getManager();
                $em->remove($currentMembership);
                $em->flush();

            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No membership was found matching hash " . $hash, 404);
    }

}