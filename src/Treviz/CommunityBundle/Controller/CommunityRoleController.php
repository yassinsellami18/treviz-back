<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 31/10/2017
 * Time: 00:29
 */

namespace Treviz\CommunityBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Treviz\CommunityBundle\Entity\CommunityRole;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Treviz\CommunityBundle\Form\CommunityRoleType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use Swagger\Annotations as SWG;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class CommunityRoleController
 * @package Treviz\CommunityBundle\Controller
 */
class CommunityRoleController extends FOSRestController
{

    /**
     * Fetches the roles of a community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the roles of a specified community",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCommunityRolesAction($hash)
    {

        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if ($community) {

            $roles = $community->getRoles();

            $globalRoles = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityRole")
                ->findBy(array("global" => true));

            foreach ($globalRoles as $globalRole) {
                $roles->add($globalRole);
            }

            $view = $this->view($roles, 200);
            return $this->handleView($view);
        }

        $view = $this->view("No community was found for hash " . $hash, 404);
        return $this->handleView($view);

    }

    /**
     * Adds a new role to a community.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created role",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @param Request $request
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postCommunityRoleAction(Request $request, $hash)
    {

        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if ($community) {

            $user = $this->getUser();

            $userMembership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $user,
                    "community" => $community
                ));

            if ( $userMembership != null
                && in_array(CommunityPermissions::MANAGE_ROLE, $userMembership->getRole()->getPermissions())) {
                $role = new CommunityRole();
                $form = $this->createForm(CommunityRoleType::class, $role);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $role->setCommunity($community);
                    $role->setDefaultCreator(false);
                    $role->setDefaultMember(false);
                    $role->setGlobal(false);

                    $roleHash = hash("sha256", random_bytes(256));

                    while ($this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityRole")
                            ->findOneBy(array("hash" => $roleHash)) != null) {
                        $roleHash = hash("sha256", random_bytes(256));
                    }

                    $role->setHash($roleHash);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($role);
                    $em->flush();

                    $view = $this->view($role, 200);
                    return $this->handleView($view);

                }

                $view = $this->view($form, 422);
                return $this->handleView($view);

            }

            $view = $this->view("You do not have the rights to do this ", 403);
            return $this->handleView($view);
        }

        $view = $this->view("No community was found for hash " . $hash, 404);
        return $this->handleView($view);
    }

    /**
     * Add a new role that can be used in all communities.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the newly created global role",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @Post("/communities/roles")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function postCommunitiesRoleAction(Request $request)
    {
        $role = new CommunityRole();
        $form = $this->createForm(CommunityRoleType::class, $role);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $roleHash = hash("sha256", random_bytes(256));

            while ($this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityRole")
                    ->findOneBy(array("hash" => $roleHash)) != null) {
                $roleHash = hash("sha256", random_bytes(256));
            }

            $role->setHash($roleHash);

            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();

            $view = $this->view($role, 200);
            return $this->handleView($view);

        }

        $view = $this->view("Invalid data", 422);
        return $this->handleView($view);
    }

    /**
     * Updates an existing role of a community.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated role",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Put("/communities/roles/{hash}")
     *
     * @param Request $request
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function putCommunityRoleAction(Request $request, $hash)
    {

        $role = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityRole")
            ->findOneBy(array("hash" => $hash));

        if ($role) {

            $user = $this->getUser();

            $userMembership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "user" => $user,
                    "community" => $role->getCommunity()
                ));

            if ( $userMembership != null
                && in_array(CommunityPermissions::MANAGE_ROLE, $userMembership->getRole()->getPermissions())) {
                $form = $this->createForm(CommunityRoleType::class, $role);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    $view = $this->view($role, 200);
                    return $this->handleView($view);

                }
            }

            $view = $this->view("You do not have the rights to do this ", 403);
            return $this->handleView($view);
        }

        $view = $this->view("No role was found for hash " . $hash, 404);
        return $this->handleView($view);
    }

    /**
     * Deletes an existing role of a community.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when the role was successfully deleted",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Delete("/communities/roles/{hash}")
     *
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function deleteCommunityRoleAction($hash)
    {

        $role = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityRole")
            ->findOneBy(array("hash" => $hash));

        if ($role!= null) {

            if(($role->isGlobal() && $this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($role);
                $em->flush();

                $view = $this->view(null, 204);
                return $this->handleView($view);
            }

            if(!$role->isGlobal()) {
                $user = $this->getUser();

                $userMembership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                    ->findOneBy(array(
                        "user" => $user,
                        "community" => $role->getCommunity()
                    ));

                /*
                 * Checks if the user has the rights to delete the membership.
                 */
                if ($userMembership != null
                    && in_array(CommunityPermissions::MANAGE_ROLE, $userMembership->getRole()->getPermissions())) {

                    $em = $this->getDoctrine()->getManager();
                    $em->remove($role);
                    $em->flush();

                    $view = $this->view(null, 204);
                    return $this->handleView($view);
                }
            }

            $view = $this->view("You do not have the rights to do this.", 403);
            return $this->handleView($view);
        }

        $view = $this->view("No role was found for hash " . $hash, 404);
        return $this->handleView($view);
    }
    
}