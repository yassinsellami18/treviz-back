<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 12/03/2017
 * Time: 15:03
 */

namespace Treviz\CommunityBundle\Controller;

use Doctrine\DBAL\Query\QueryBuilder;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\FileParam;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use Treviz\ProjectBundle\Entity\Project;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CommunityBundle\Entity\CommunityRole;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Treviz\CommunityBundle\Form\CommunityType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CommunityBundle\Entity\Community;
use Treviz\CoreBundle\Entity\User;
use Swagger\Annotations as SWG;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class CommunityController
 * @package Treviz\CommunityBundle\Controller
 */
class CommunityController extends FOSRestController
{

    /**
     * Fetches a community specified by its hash, as long as the user can see it.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns a community identified by its hash",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getCommunityAction($hash){
        /** @var Community $community */
        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if(isset($community)){

            if($community->isPublic()){
                $view = $this->view($community, 200);
                return $view;
            } else {

                /** @var User $user */
                $user = $this->getUser();

                /*
                 * Firstly, we check if the user is a member of the project.
                 * Otherwise, we check if they share at least one community.
                 */
                if(array_intersect($community->getMemberships()->toArray(), $user->getCommunitiesMemberships()->toArray())){
                    $view = $this->view($community, 200);
                    return $view;
                }

                $view = $this->view("You cannot see this community", 403);
                return $view;

            }

        }

        $view = $this->view("No community was found for hash " . $hash, 404);
        return $view;

    }

    /**
     * Fetches specific communities, according to query parameters.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of communities matching the query",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @QueryParam(name="name", requirements="[\p{L}\p{N}_\s]+", description="search  by name", nullable=true)
     * @QueryParam(name="offset", description="pagination", nullable=true)
     * @QueryParam(name="nb", description="pagination", nullable=true)
     * @QueryParam(name="user", description="username of the user who must take part in the project", nullable=true)
     * @QueryParam(name="role", description="name of role of the user in those projects", nullable=true)
     * @QueryParam(name="project", description="hash of the project", nullable=true)
     *
     * @View(serializerGroups={"Default"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getCommunitiesAction(ParamFetcher $paramFetcher){

        $s_name = $paramFetcher->get("name");
        $s_user = $paramFetcher->get("user");
        $s_role = $paramFetcher->get("role");
        $s_start = $paramFetcher->get("offset");
        $s_nb = $paramFetcher->get("nb");
        $s_project = $paramFetcher->get("project");

        $repository = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community");
        $qb = $repository->createQueryBuilder('c');

        if ($s_name) {
            $qb->andWhere('c.name LIKE :name');
            $qb->setParameter('name', '%' . $s_name . '%');
        }

        /** @var User $currentUser
         * Checks first if the user can actually see the project, that is to say:
         *  - the project is public
         *  - the user is member of the project
         *  - the user is member of a community the project is in

         */
        $currentUser = $this->getUser();
        if (isset($currentUser)) {

            $qb->andWhere("c.public = true 
                           OR EXISTS(
                            SELECT cm
                            FROM TrevizCommunityBundle:CommunityMembership cm
                            WHERE cm.user = :currentUser
                            AND cm.community = c
                           )");
            $qb->setParameter("currentUser", $currentUser);

        } else {
            $qb->andWhere("c.public = true");
        }


        /*
         * If a project is specified, returns the communities of this project
         * Checks first if the user can see the project, otherwise send empty array.
         */
        if($s_project) {

            /** @var Project $project */
            $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")->findOneByHash($s_project);

            if($project->isPublic()
            || (isset($currentUser)
                && array_intersect($currentUser->getProjectsMemberships()->toArray(),$project->getMemberships()->toArray()))
            ) {
                $qb->andWhere(":project MEMBER OF c.projects");
                $qb->setParameter("project", $project);
            } else {
                $view = $this->view([], 200);
                return $view;
            }
        }

        if($s_user !== null){

            $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")
                ->findOneBy(array("username" => $s_user));

            if ($user != null) {

                if($s_role !== null) {
                    $qb->andWhere("EXISTS(
                                SELECT cm2
                                FROM TrevizCommunityBundle:CommunityMembership cm2
                                JOIN cm2.role r
                                WHERE cm2.user = :user
                                AND r.name = :role
                                AND cm2.community = c
                              )");
                    $qb->setParameter('user', $user);
                    $qb->setParameter('role', $s_role);
            } else {
                    $qb->andWhere("EXISTS(
                                SELECT cm2
                                FROM TrevizCommunityBundle:CommunityMembership cm2
                                WHERE cm2.user = :user
                                AND cm2.community = c
                              )");
                    $qb->setParameter('user', $user);
                }

            } else {

                $view = $this->view([], 200);
                return $view;

            }

        }

        if (isset($s_nb)) {
            $qb->setFirstResult($s_nb);
        }

        if (isset($s_start)) {
            $qb->setFirstResult($s_start);
        }

        $qb->orderBy('c.creationDate', 'DESC');
        $communities = $qb->getQuery()->getResult();

        $view = $this->view($communities, 200);
        return $view;

    }

    /**
     * Creates a new Community and returns it
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the newly created community"
     * )
     *
     * @SWG\Tag(name="community")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postCommunityAction(Request $request)
    {

        $community = new Community();
        $form = $this->createForm(CommunityType::class, $community);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            /** @var File $logo */
            $logo = $request->files->get("logo");

            if($logo){

                $logoName = md5($logo->getFilename() . uniqid()) . '.' . $logo->guessExtension();

                $logo->move(
                    $this->getParameter("kernel.root_dir") .
                    '/../web' .
                    $this->getParameter('upload_community_logo_path'),
                    $logoName
                );

                $community->setLogo($logoName);
                $community->setLogoUrl(
                    $this->getParameter("backend_url") .
                    $this->getParameter("upload_community_logo_path") .
                    $logoName
                );

            }

            /** @var File $logo */
            $background = $request->files->get("background");

            if($background){

                $backgroundName = md5($background->getFilename() . uniqid()) . '.' . $background->guessExtension();

                $background->move(
                    $this->getParameter("kernel.root_dir") .
                    '/../web' .
                    $this->getParameter('upload_community_background_path'),
                    $backgroundName
                );

                $community->setBackgroundImage($backgroundName);
                $community->setBackgroundImageUrl(
                    $this->getParameter("backend_url") .
                    $this->getParameter("upload_community_background_path") .
                    $backgroundName
                );

            }

            /** @var User $user */
            $user = $this->getUser();
            $membership = new CommunityMembership();
            $membership->setCommunity($community);
            $membership->setUser($user);

            $defaultCreatorRole = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityRole")->findOneBy(array("defaultCreator" => true));

            if ($defaultCreatorRole == null) {
                $defaultCreatorRole = new CommunityRole();
                $defaultCreatorRole->setName("Creator");
                $defaultCreatorRole->setDefaultCreator(true);
                $defaultCreatorRole->setDefaultMember(false);
                $defaultCreatorRole->setGlobal(true);
                $defaultCreatorRole->setPermissions(CommunityPermissions::getAvailablePermissions());
                $defaultCreatorRole->setHash(hash("sha256", random_bytes(256)));
            }

            $membership->setRole($defaultCreatorRole);

            /*
             * Generate a unique hash for the community and membership
             */
            $communityHash = hash("sha256", random_bytes(256));

            while ($this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
                    ->findOneBy(array("hash" => $communityHash)) != null) {
                $communityHash = hash("sha256", random_bytes(256));
            }

            $community->setHash($communityHash);

            $membershipHash = hash("sha256", random_bytes(256));

            while ($this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                    ->findOneBy(array("hash" => $membershipHash)) != null) {
                $membershipHash = hash("sha256", random_bytes(256));
            }

            $membership->setHash($membershipHash);

            $user->addRoom($community->getRooms()->first());

            $em = $this->getDoctrine()->getManager();
            $em->persist($community);
            $em->persist($membership);
            $em->flush();

            return $this->view($community, 201);

        }

        return $this->view($form, 422);

    }

    /**
     * Updates an existing community
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated community"
     * )
     *
     * @SWG\Tag(name="community")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putCommunityAction(Request $request, $hash)
    {
        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if (isset($community)) {

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $this->getUser()
                ));

            if (isset($membership)
                && in_array(CommunityPermissions::UPDATE_COMMUNITY, $membership->getRole()->getPermissions())) {


                /*
                 * Set the community logo an background with the actual files so that the form builder does not crash.
                 */
                if ($logoFileName = $community->getLogo()) {
                    $community->setLogo(new File($this->getParameter("kernel.root_dir") . '/../web' . $this->getParameter('upload_community_logo_path') . $logoFileName));
                }

                if ($backgroundLogoName = $community->getBackgroundImage()) {
                    $community->setBackgroundImage(new File($this->getParameter("kernel.root_dir") . '/../web' . $this->getParameter('upload_community_background_path') . $backgroundLogoName));
                }

                $form = $this->createForm(CommunityType::class, $community);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    /** @var File $logo */
                    $logo = $request->files->get("logo");

                    if($logo){

                        $logoName = md5($logo->getFilename() . uniqid()) . '.' . $logo->guessExtension();

                        $logo->move(
                            $this->getParameter("kernel.root_dir") .
                            '/../web' .
                            $this->getParameter('upload_community_logo_path'),
                            $logoName
                        );

                        /*$community->setLogo($logoName);*/
                        $community->setLogoUrl(
                            $this->getParameter("backend_url") .
                            $this->getParameter("upload_community_logo_path") .
                            $logoName
                        );

                    }

                    /** @var File $logo */
                    $background = $request->files->get("background");

                    if($background){

                        $backgroundName = md5($background->getFilename() . uniqid()) . '.' . $background->guessExtension();

                        $background->move(
                            $this->getParameter("kernel.root_dir") .
                            '/../web' .
                            $this->getParameter('upload_community_background_path'),
                            $backgroundName
                        );

                        $community->setBackgroundImage($backgroundName);
                        $community->setBackgroundImageUrl(
                            $this->getParameter("backend_url") .
                            $this->getParameter("upload_community_background_path") .
                            $backgroundName
                        );

                    }

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->view($community, 200);

                }

                return $this->view($form, 422);
            }

            return $this->view("You do not have the rights to do this", 403);

        }

        return $this->view("No community was found for hash " . $hash, 404);

    }

    /**
     * Deletes an existing community
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when the community was successfully deleted"
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param $hash
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteCommunityAction($hash)
    {
        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if ($community) {

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $this->getUser()
                ));

            if (isset($membership)
                && in_array(CommunityPermissions::DELETE_COMMUNITY, $membership->getRole()->getPermissions())) {

                $em = $this->getDoctrine()->getManager();
                $em->remove($community);
                $em->flush();
                $view = $this->view(null, 204);
                return $this->handleView($view);

            }

            $view = $this->view("You do not have the rights to perform this action", 403);
            return $this->handleView($view);

        }

        $view = $this->view("No community was foudn for hash " . $hash, 404);
        return $this->handleView($view);

    }

    /**
     * Posts a new logo for the community
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated community with a new logo"
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Post("/communities/{hash}/logo")
     * @FileParam(name="image", image=true, nullable=false)
     *
     * @View(serializerGroups={"Default"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @param $hash
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function postCommunityLogoAction(ParamFetcher $paramFetcher, $hash)
    {

        /** @var Community $community */
        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if (isset($community)) {

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $this->getUser()
                ));

            if (isset($membership)
                && in_array(CommunityPermissions::UPDATE_COMMUNITY, $membership->getRole()->getPermissions())) {

                $logo = $paramFetcher->get("image");

                $logoName = md5($logo->getFilename() . uniqid()) . '.' . $logo->guessExtension();

                $logo->move(
                    $this->getParameter("kernel.root_dir") .
                    '/../web' .
                    $this->getParameter('upload_community_logo_path'),
                    $logoName
                );

                $community->setLogo($logoName);
                $community->setLogoUrl(
                    $this->getParameter("backend_url") .
                    $this->getParameter("upload_community_logo_path") .
                    $logoName
                );

                $em = $this->getDoctrine()->getManager();
                $em->flush();

                return  $this->view($community, 200);


            }

            return $this->view("You do not have the rights to perform this action", 403);

        }

        return $this->view("No community was found for hash " . $hash, 404);

    }

    /**
     * Posts a new background for the community
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated community with a new background image"
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Post("/communities/{hash}/background")
     * @FileParam(name="image", image=true, nullable=false)
     *
     * @View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @param $hash
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function postCommunityBackgroundAction(ParamFetcher $paramFetcher, $hash)
    {

        /** @var Community $community */
        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if (isset($community)) {

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $this->getUser()
                ));

            if (isset($membership)
                && in_array(CommunityPermissions::UPDATE_COMMUNITY, $membership->getRole()->getPermissions())) {

                $logo = $paramFetcher->get("image");

                $logoName = md5($logo->getFilename() . uniqid()) . '.' . $logo->guessExtension();

                $logo->move(
                    $this->getParameter("kernel.root_dir") .
                    '/../web' .
                    $this->getParameter('upload_community_background_path'),
                    $logoName
                );

                $community->setBackgroundImage($logoName);
                $community->setBackgroundImageUrl(
                    $this->getParameter("backend_url") .
                    $this->getParameter("upload_community_background_path") .
                    $logoName
                );

                $em = $this->getDoctrine()->getManager();
                $em->flush();

                return  $this->view($community, 200);


            }

            return $this->view("You do not have the rights to perform this action", 403);

        }

        return $this->view("No community was found for hash " . $hash, 404);

    }

}