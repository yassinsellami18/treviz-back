<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 31/10/2017
 * Time: 00:29
 */

namespace Treviz\CommunityBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CommunityBundle\Entity\CommunityInvitation;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Treviz\CommunityBundle\Form\CommunityInvitationType;
use Treviz\CoreBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Swagger\Annotations as SWG;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class CommunityInvitationController
 * @package Treviz\CommunityBundle\Controller
 */
class CommunityInvitationController extends FOSRestController
{

    /**
     * Fetches the invitations of a community
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the invitations of a specified community",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getCommunityInvitationsAction($hash)
    {
        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if ($community) {
            $user = $this->getUser();

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $user
                ));

            if($membership != null
                && in_array(CommunityPermissions::MANAGE_INVITATIONS, $membership->getRole()->getPermissions())) {
                return $this->view($community->getInvitations(), 200);
            }

            $invitation = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityInvitation")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $user
                ));

            if ($invitation != null) {
                return $this->view([$invitation], 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No community was found with hash " . $hash, 404);

    }

    /**
     * Fetches the communities of a user specified in the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the invitations specified by the query",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @QueryParam(name="user", description="username of the user from whom fetch invitations", nullable=false)
     *
     * @View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getCommunitiesInvitationsAction(ParamFetcher $paramFetcher)
    {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")
            ->findOneBy(array("username" => $paramFetcher->get("user")));

        if ($user ==! null) {

            $currentUser = $this->getUser();

            if ($user == $currentUser) {
                $invitations = $user->getCommunitiesInvitations();
            } else {
                $repository = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityInvitation");
                $qb = $repository->createQueryBuilder("pc");

                $qb->andWhere("pc.user = :user");
                $qb->setParameter("user", $user);

                $qb->andWhere("EXISTS (
                                SELECT cm
                                FROM TrevizCommunityBundle:CommunityMembership cm
                                JOIN cm.role r
                                WHERE cm.community = pc.community
                                AND cm.user = :current_user
                                AND r.permissions LIKE :read_invitations
                              )");
                $qb->setParameter("current_user", $currentUser);
                $qb->setParameter("read_invitations", '%'.CommunityPermissions::MANAGE_INVITATIONS.'%');

                $invitations = $qb->getQuery()->getResult();

            }

            return $this->view($invitations, 200);
        }

        $view = $this->view(null, 200);
        return $view;

    }

    /**
     * Creates a new invitation for a community.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created invitation",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postCommunityInvitationAction(Request $request, $hash)
    {
        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if ($community) {
            $user = $this->getUser();

            /** @var  $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $user
                ));

            if($membership !== null
                && in_array(CommunityPermissions::MANAGE_INVITATIONS, $membership->getRole()->getPermissions())) {

                $invitation = new CommunityInvitation();
                $form = $this->createForm(CommunityInvitationType::class, $invitation);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $invitation->setCommunity($community);

                    $invitationHash = hash("sha256", random_bytes(256));

                    while ($this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityInvitation")
                            ->findOneBy(array("hash" => $invitationHash)) != null) {
                        $invitationHash = hash("sha256", random_bytes(256));
                    }

                    $invitation->setHash($invitationHash);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($invitation);
                    $em->flush();

                    return $this->view($invitation, 200);

                }

                return $this->view($form, 422);

            }

            return $this->view("You cannot candidate to a community you are part of, candidate or invited to.", 403);

        }

        return $this->view("No community was found with hash " . $hash, 404);
    }

    /**
     * Removes an invitation from a community.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when the invitation was successfully deleted",
     * )
     *
     * @SWG\Tag(name="projects")
     *
     * @Delete("/communities/invitations/{hash}")
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteCommunityInvitationAction($hash)
    {
        $invitation = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityInvitation")
            ->findOneBy(array("hash" => $hash));

        if ($invitation) {
            $user = $this->getUser();

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $invitation->getCommunity(),
                    "user" => $user
                ));

            if($invitation->getUser() == $user
                || ($membership ==! null
                    && in_array(CommunityPermissions::MANAGE_INVITATIONS, $membership->getRole()->getPermissions()))){

                $em = $this->getDoctrine()->getManager();
                $em->remove($invitation);
                $em->flush();
                return $this->view(null, 204);

            }

            return $this->view("You do not have the rights to do this", 403);

        }

        return $this->view("No invitation was found with hash " . $hash, 404);
    }

}