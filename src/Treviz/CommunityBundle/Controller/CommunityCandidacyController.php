<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 31/10/2017
 * Time: 00:29
 */

namespace Treviz\CommunityBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CommunityBundle\Entity\CommunityCandidacy;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CommunityBundle\Entity\CommunityRole;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Treviz\CommunityBundle\Events\CommunityCandidacyEvent;
use Treviz\CommunityBundle\Form\CommunityCandidacyType;
use Treviz\CoreBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use Swagger\Annotations as SWG;

/**
 *
 * @Security("has_role('ROLE_USER')")
 *
 * Class CommunityCandidacyController
 * @package Treviz\CommunityBundle\Controller
 */
class CommunityCandidacyController extends FOSRestController
{

    /**
     * Fetches the candidacies of a community
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the candidacies of a specified community",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @View(serializerGroups={"Default", "community"}, serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getCommunityCandidaciesAction($hash)
    {
        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        if ($community) {
            $user = $this->getUser();

            /** @var CommunityMembership $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $user
                ));

            if($membership !== null
                && in_array(CommunityPermissions::MANAGE_CANDIDACIES, $membership->getRole()->getPermissions())) {
                return $this->view($community->getCandidacies(), 200);
            }

            $candidacy = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityCandidacy")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $user
                ));

            if ($candidacy !== null) {
                return $this->view([$candidacy], 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No community was found with hash " . $hash, 404);
    }

    /**
     * Fetches the communities of a user specified in the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the candidacies specified by the query",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @QueryParam(name="user", description="username of the user from whom fetch candidacies", nullable=false)
     *
     * @View(serializerGroups={"Default", "user"}, serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getCommunitiesCandidaciesAction(ParamFetcher $paramFetcher)
    {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")
            ->findOneBy(array("username" => $paramFetcher->get("user")));

        if ($user ==! null) {

            $currentUser = $this->getUser();

            if ($user == $currentUser) {
                $candidacies = $user->getCommunitiesCandidacies();
            } else {
                $repository = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityCandidacy");
                $qb = $repository->createQueryBuilder("cc");

                $qb->andWhere("cc.user = :user");
                $qb->setParameter("user", $user);

                $qb->andWhere("EXISTS (
                                SELECT cm
                                FROM TrevizCommunityBundle:CommunityMembership cm
                                JOIN cm.role r
                                WHERE cm.community = cc.community
                                AND cm.user = :current_user
                                AND r.permissions LIKE :read_candidacies
                              )");
                $qb->setParameter("current_user", $currentUser);
                $qb->setParameter("read_candidacies", '%'.CommunityPermissions::MANAGE_CANDIDACIES.'%');

                $candidacies = $qb->getQuery()->getResult();

            }

            return $this->view($candidacies, 200);
        }

        $view = $this->view(null, 200);
        return $view;

    }

    /**
     * Creates a new candidacy for a community.
     * If the community is open, this creates and returns a new membership for the current user.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created candidacy",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postCommunityCandidacyAction(Request $request, $hash) {
        $community = $this->getDoctrine()->getRepository("TrevizCommunityBundle:Community")
            ->findOneBy(array("hash" => $hash));

        $dispatcher = $this->get('treviz_core.event_dispatcher.dispatcher');

        if ($community) {
            $user = $this->getUser();

            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $user
                ));

            $invitation = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityInvitation")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $user
                ));

            $candidacy = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityCandidacy")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $user
                ));

            if($membership == null && $invitation == null && $candidacy == null) {

                $candidacy = new CommunityCandidacy();
                $form = $this->createForm(CommunityCandidacyType::class, $candidacy);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    $candidacy->setCommunity($community);
                    $candidacy->setUser($user);

                    $candidacyHash = hash("sha256", random_bytes(256));

                    while ($this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityCandidacy")
                            ->findOneBy(array("hash" => $candidacyHash)) !== null) {
                        $candidacyHash = hash("sha256", random_bytes(256));
                    }

                    $candidacy->setHash($candidacyHash);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($candidacy);
                    $em->flush();

                    $event = new CommunityCandidacyEvent($candidacy);
                    $dispatcher->dispatch(CommunityCandidacyEvent::NAME, $event);

                    return $this->view($candidacy, 200);

                }

                return $this->view($form, 422);

            }

            return $this->view("You cannot candidate to a community you are part of, candidate or invited to.", 403);

        }

        return $this->view("No community was found with hash " . $hash, 404);

    }

    /**
     * Removes a candidacy from a community.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returns when the candidacy was successfully deleted",
     * )
     *
     * @SWG\Tag(name="communities")
     *
     * @Delete("/communities/candidacies/{hash}")
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteCommunityCandidacyAction($hash)
    {
        
        $candidacy = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityCandidacy")
            ->findOneBy(array("hash" => $hash));

        if ($candidacy) {
            $user = $this->getUser();

            /** @var CommunityMembership $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $candidacy->getCommunity(),
                    "user" => $user
                ));

            if($candidacy->getUser() == $user
                || ($membership ==! null
                    && in_array(CommunityPermissions::MANAGE_CANDIDACIES, $membership->getRole()->getPermissions()))){

                $em = $this->getDoctrine()->getManager();
                $em->remove($candidacy);
                $em->flush();
                return $this->view(null, 204);

            }

            return $this->view("You do not have the rights to do this", 403);

        }

        return $this->view("No candidacy was found with hash " . $hash, 404);
    }

    /**
     * Accepts a candidacy from a community.
     *
     * @SWG\Parameter(
     *     name="hash",
     *     in="path",
     *     type="string",
     *     required=true,
     *     description="Hash of the candidacy to accept"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="The candidacy is successfully accepted",
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="The submitted role is invalid",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user does not have the rights to accept the candidacy",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="The candidacy could not be fetched",
     * )
     *
     *
     * @SWG\Tag(name="communities")
     *
     * @Post("/communities/candidacies/{hash}/accept")
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function acceptCommunityCandidacyAction($hash)
    {
        /** @var CommunityCandidacy $candidacy */
        $candidacy = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityCandidacy")
            ->findOneByHash($hash);

        if ($candidacy != null) {

            /** @var User $currentUser */
            $currentUser = $this->getUser();
            $community = $candidacy->getCommunity();
            $user = $candidacy->getUser();

            $communityService = $this->get('treviz_community.community_membership_service');
            $membership = $communityService->getUserMembership($currentUser, $community);

            if ($membership !== null &&
                in_array(CommunityPermissions::MANAGE_CANDIDACIES, $membership->getRole()->getPermissions())) {

                $newMembership = new CommunityMembership();
                $newMembership->setCommunity($community);
                $newMembership->setUser($user);
                $membershipHash = hash("sha256", random_bytes(256));

                while ($this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                        ->findOneBy(array('hash' => $membershipHash)) !== null) {
                    $membershipHash = hash("sha256", random_bytes(256));
                }
                $membership->setHash($membershipHash);

                /** @var CommunityRole $role */
                $role = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityRole")
                    ->findOneBy(array(
                        'defaultMember' => true,
                        'community' => $community
                    ));

                $em = $this->getDoctrine()->getManager();

                if ($role) {
                    $membership->setRole($role);
                } else {
                    $role = new CommunityRole();

                    $roleHash = hash("sha256", random_bytes(256));
                    while ($this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityRole")
                            ->findOneBy(array("hash" => $roleHash)) != null) {
                        $roleHash = hash("sha256", random_bytes(256));
                    }
                    $role->setHash($roleHash);
                    $role->setName('Member');
                    $role->setCommunity($community);
                    $role->setDefaultMember(true);
                    $em->persist($role);
                    $membership->setRole($role);
                }

                $em->persist($newMembership);
                $em->remove($candidacy);
                $em->flush();

                return $this->view("Candidacy was accepted", 200);
            }

            return $this->view("You do not have the rights to do this" . $hash, 403);

        }

        return $this->view("No candidacy was found for hash " . $hash, 404);
    }

}