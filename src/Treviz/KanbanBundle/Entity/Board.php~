<?php

namespace Treviz\KanbanBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;
use Treviz\KanbanBundle\Entity\Traits\Hashable;

/**
 * Board
 *
 * @ORM\Table(name="board")
 * @ORM\Entity(repositoryClass="Treviz\KanbanBundle\Repository\BoardRepository")
 */
class Board
{

    use Hashable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="open", type="boolean")
     */
    private $open;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Column", mappedBy="board", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $columns;

    /**
     * Board constructor.
     */
    public function __construct()
    {
        $this->columns = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Board
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Board
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set open
     *
     * @param boolean $open
     *
     * @return Board
     */
    public function setOpen($open)
    {
        $this->open = $open;

        return $this;
    }

    /**
     * Get open
     *
     * @return bool
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * Add column
     *
     * @param \Treviz\KanbanBundle\Entity\Column $column
     *
     * @return Board
     */
    public function addColumn(\Treviz\KanbanBundle\Entity\Column $column)
    {
        $this->columns[] = $column;

        return $this;
    }

    /**
     * Remove column
     *
     * @param \Treviz\KanbanBundle\Entity\Column $column
     */
    public function removeColumn(\Treviz\KanbanBundle\Entity\Column $column)
    {
        $this->columns->removeElement($column);
    }

    /**
     * Get columns
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getColumns(): Collection
    {
        return $this->columns;
    }
}
