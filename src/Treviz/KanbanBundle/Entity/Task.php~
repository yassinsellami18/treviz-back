<?php

namespace Treviz\KanbanBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Treviz\KanbanBundle\Entity\Traits\Hashable;

/**
 * Task
 *
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="Treviz\KanbanBundle\Repository\TaskRepository")
 */
class Task
{

    use Hashable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime")
     */
    private $deadline;

    /**
     * @var bool
     *
     * @ORM\Column(name="archived", type="boolean")
     */
    private $archived;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var Column
     *
     * @ORM\ManyToOne(targetEntity="Column", inversedBy="tasks")
     * @JMS\Exclude()
     */
    private $column;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Label", mappedBy="column", cascade={"persist", "remove"})
     * @JMS\Exclude()
     */
    private $labels;

    /**
     * The assignees are the ones who are to perform the task
     *
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Treviz\CoreBundle\Entity\Users")
     * @ORM\JoinTable(name="tasks_assignees")
     * @JMS\MaxDepth(3)
     */
    private $assignees;

    /**
     * The supervisors determine if the task was correctly performed, by who, and in which proportions.
     * Their opinion is used to share the reward of the task, and archive it.
     *
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Treviz\CoreBundle\Entity\Users")
     * @ORM\JoinTable(name="tasks_supervisors")
     * @JMS\MaxDepth(3)
     */
    private $supervisor;

    /**
     * Users can vote for a task, and tell they want it to be made.
     *
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Treviz\CoreBundle\Entity\Users")
     * @ORM\JoinTable(name="tasks_voters")
     * @JMS\MaxDepth(3)
     */
    private $voters;

    /**
     * Each task can come with a reward.
     * In the future, this reward will represent a crypto asset, like ether.
     *
     * @var integer
     *
     * @ORM\Column(name="reward", type="integer")
     */
    private $reward;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Task
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return Task
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set archived
     *
     * @param boolean $archived
     *
     * @return Task
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get archived
     *
     * @return bool
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Task
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Task
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}

