<?php

namespace Treviz\KanbanBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Treviz\CoreBundle\Entity\Post;
use Treviz\CoreBundle\Entity\User;
use Treviz\KanbanBundle\Entity\Traits\Hashable;
use Treviz\ProjectBundle\Entity\ProjectJob;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Task
 *
 * @ORM\Table(name="board_task")
 * @ORM\Entity(repositoryClass="Treviz\KanbanBundle\Repository\TaskRepository")
 */
class Task
{

    use Hashable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime", nullable=true)
     */
    private $deadline;

    /**
     * @var bool
     *
     * @ORM\Column(name="pending_approval", type="boolean")
     */
    private $pendingApproval = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="archived", type="boolean")
     */
    private $archived = false;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var Column
     *
     * @ORM\ManyToOne(targetEntity="Column", inversedBy="tasks")
     * @JMS\Groups({"job"})
     * @JMS\MaxDepth(4)
     */
    private $column;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Label", cascade={"persist"})
     * @ORM\JoinTable(name="tasks_labels")
     */
    private $labels;

    /**
     * The assignee is the one who is to perform the task
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Treviz\CoreBundle\Entity\User")
     * @JMS\MaxDepth(3)
     */
    private $assignee;

    /**
     * The supervisor determine if the task was correctly performed, by who, and in which proportions.
     * His·er opinion is used to send the reward of the task, and archive it.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Treviz\CoreBundle\Entity\User")
     * @JMS\MaxDepth(3)
     */
    private $supervisor;

    /**
     * Users can vote for a task, and tell they want it to be made.
     *
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Treviz\CoreBundle\Entity\User")
     * @ORM\JoinTable(name="tasks_voters")
     * @JMS\MaxDepth(3)
     */
    private $voters;

    /**
     * Each task can come with a reward.
     * In the future, this reward will represent a crypto asset, like ether.
     *
     * @var int
     *
     * @ORM\Column(name="reward", type="integer", nullable=true)
     */
    private $reward;

    /**
     * @var ProjectJob
     *
     * @ORM\ManyToOne(targetEntity="Treviz\ProjectBundle\Entity\ProjectJob", inversedBy="tasks")
     */
    private $job;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Treviz\CoreBundle\Entity\Post", mappedBy="task", cascade={"persist", "remove"})
     * @JMS\MaxDepth(7) In order to return posts and the users who posted them.
     */
    private $posts;

    /**
     * The supervisor of a task can grant feedback when a task is submitted.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Treviz\KanbanBundle\Entity\Feedback", mappedBy="task")
     * @JMS\Exclude()
     */
    private $feedbacks;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->labels = new ArrayCollection();
        $this->voters = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->feedbacks = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Task
     */
    public function setName($name): Task
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return Task
     */
    public function setDeadline($deadline): Task
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline(): ?\DateTime
    {
        return $this->deadline;
    }

    /**
     * Set archived
     *
     * @param boolean $archived
     *
     * @return Task
     */
    public function setArchived($archived): Task
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get archived
     *
     * @return bool
     */
    public function isArchived(): ?bool
    {
        return $this->archived;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Task
     */
    public function setPosition($position): Task
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Task
     */
    public function setDescription($description): Task
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set reward
     *
     * @param int $reward
     *
     * @return Task
     */
    public function setReward($reward): Task
    {
        $this->reward = $reward;

        return $this;
    }

    /**
     * Get reward
     *
     * @return int
     */
    public function getReward(): ?int
    {
        return $this->reward;
    }

    /**
     * Set column
     * If the column to set is different from the existing one, remove the task from the previously assigned column.
     *
     * @param Column $column
     *
     * @return Task
     */
    public function setColumn(Column $column = null): Task
    {
        /*
         * If the given column is null, do not unset the task's column.
         */
        if ($column == null) {
            return $this;
        }

        if ($this->column && $this->column !== $column && $this->column->getTasks()->contains($this)) {
            $this->column->removeTask($this);
        }

        $this->column = $column;
        if (!$this->column->getTasks()->contains($this)) {
            $this->column->addTask($this);
        }

        return $this;
    }

    /**
     * Get column
     *
     * @return Column
     */
    public function getColumn(): ?Column
    {
        return $this->column;
    }

    /**
     * Add label
     *
     * @param Label $label
     *
     * @return Task
     */
    public function addLabel(Label $label): Task
    {
        $this->labels[] = $label;

        return $this;
    }

    /**
     * Remove label
     *
     * @param Label $label
     */
    public function removeLabel(Label $label)
    {
        $this->labels->removeElement($label);
    }

    /**
     * Get labels
     *
     * @return Collection
     */
    public function getLabels(): Collection
    {
        return $this->labels;
    }

    /**
     * @return User
     */
    public function getAssignee(): ?User
    {
        return $this->assignee;
    }

    /**
     * @param User|null $assignee
     */
    public function setAssignee(User $assignee = null)
    {
        $this->assignee = $assignee;
    }

    /**
     * @return User
     */
    public function getSupervisor(): ?User
    {
        return $this->supervisor;
    }

    /**
     * @param User $supervisor
     */
    public function setSupervisor(User $supervisor)
    {
        $this->supervisor = $supervisor;
    }

    /**
     * Add voter
     *
     * @param User $voter
     *
     * @return Task
     */
    public function addVoter(User $voter): Task
    {
        $this->voters[] = $voter;

        return $this;
    }

    /**
     * Remove voter
     *
     * @param \Treviz\CoreBundle\Entity\User $voter
     */
    public function removeVoter(User $voter)
    {
        $this->voters->removeElement($voter);
    }

    /**
     * Get voters
     *
     * @return Collection
     */
    public function getVoters(): Collection
    {
        return $this->voters;
    }

    /**
     * @return ProjectJob
     */
    public function getJob(): ?ProjectJob
    {
        return $this->job;
    }

    /**
     * @param ProjectJob $job
     * @return Task
     */
    public function setJob(ProjectJob $job): Task
    {
        $this->job = $job;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param ArrayCollection $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
        if ($post->getTask() == null) {
            $post->setTask($this);
        }
    }

    /**
     * @param Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * @return boolean
     */
    public function isPendingApproval(): ?bool
    {
        return $this->pendingApproval;
    }

    /**
     * @param boolean $pendingApproval
     */
    public function setPendingApproval(bool $pendingApproval)
    {
        $this->pendingApproval = $pendingApproval;
    }

    /**
     * @return Collection
     */
    public function getFeedbacks(): Collection
    {
        return $this->feedbacks;
    }

    /**
     * @param ArrayCollection $feedbacks
     */
    public function setFeedbacks(ArrayCollection $feedbacks)
    {
        $this->feedbacks = $feedbacks;
    }

    /**
     * @param Feedback $feedback
     */
    public function addFeedback(Feedback $feedback)
    {
        $this->feedbacks->add($feedback);
        if ($feedback->getTask() == null) {
            $feedback->setTask($this);
        }
    }

    /**
     * @param Feedback $feedback
     */
    public function removeFeedback(Feedback $feedback)
    {
        $this->feedbacks->removeElement($feedback);
    }

}
