<?php

namespace Treviz\KanbanBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\KanbanBundle\Entity\Label;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;

/**
 * Label controller.
 *
 * @Security("has_role('ROLE_USER')")
 *
 */
class LabelController extends FOSRestController
{

    /**
     * Lists all labels of a board.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of labels matching the query",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No board is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the board's labels",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @param string $hash Hash of the board from which fetch the label.
     * @return \FOS\RestBundle\View\View
     */
    public function getBoardLabelsAction($hash)
    {
        $board = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Board")
            ->findOneBy(array("hash" => $hash));

        if ($board) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $board->getProject(),
                    "user" => $this->getUser()
                ));

            if ($membership !== null) {
                return $this->view($board->getLabels(), 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No board was found for hash " . $hash, 404);

    }

    /**
     * Finds and returns a label.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Label is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No label is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the label",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Get("/boards/labels/{hash}")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getBoardLabelAction($hash)
    {

        $label = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Label")
            ->findOneBy(array("hash" => $hash));

        if ($label) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $label->getBoard()->getProject(),
                    "user" => $this->getUser()
                ));

            if ($membership !== null) {
                return $this->view($label, 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No label was found for hash " . $hash, 404);

    }


    /**
     * Creates a new label.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Label is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to create the label",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postBoardLabelAction(Request $request, $hash)
    {

        $board = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Board")->findOneBy(array("hash" => $hash));

        if ($board !== null) {

            $label = new Label();
            $form = $this->createForm('Treviz\KanbanBundle\Form\LabelType', $label);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $board->getProject(),
                        "user" => $this->getUser()
                    ));

                if($membership !== null
                    && in_array(ProjectPermissions::MANAGE_KANBAN, $membership->getRole()->getPermissions())) {

                    $hash = hash("sha256", random_bytes(256));
                    while ($this->getDoctrine()->getRepository("TrevizKanbanBundle:Label")->findOneByHash($hash) !== null) {
                        $hash = hash("sha256", random_bytes(256));
                    }

                    $board->addLabel($label);
                    $label->setHash($hash);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($label);
                    $em->flush();

                    return $this->view($label, 201);
                }

                return $this->view("You are not authorized to do this", 403);

            }

            return $this->view($form, 422);
        }

        return $this->view("No board was found for hash " . $hash, 404);

    }


    /**
     * Edit an existing label.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Label is successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to update the label",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Put("/boards/labels/{hash}")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putLabelAction(Request $request, $hash)
    {

        $label = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Label")->findOneBy(array("hash" => $hash));

        if ($label !== null) {

            $form = $this->createForm('Treviz\KanbanBundle\Form\LabelType', $label);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $label->getBoard()->getProject(),
                        "user" => $this->getUser()
                    ));

                if($membership !== null
                    && in_array(ProjectPermissions::MANAGE_KANBAN, $membership->getRole()->getPermissions())) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->view($label, 200);
                }

                return $this->view("You are not authorized to do this", 403);

            }

            return $this->view($form, 422);
        }

        return $this->view("No label was found for hash " . $hash, 404);

    }

    /**
     * Deletes a label entity.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Label is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to delete the label",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No label is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Delete("/boards/labels/{hash}")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteBoardLabelAction($hash)
    {

        $label = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Label")->findOneBy(array("hash" => $hash));

        if ($label !== null) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $label->getBoard()->getProject(),
                    "user" => $this->getUser()
                ));

            if($membership !== null
                && in_array(ProjectPermissions::MANAGE_KANBAN, $membership->getRole()->getPermissions())) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($label);
                $em->flush();

                return $this->view(null, 204);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No label was found for hash " . $hash, 404);
    }

}
