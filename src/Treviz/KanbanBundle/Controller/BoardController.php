<?php

namespace Treviz\KanbanBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\KanbanBundle\Entity\Board;
use Treviz\KanbanBundle\Form\BoardType;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Board controller.
 *
 * @Security("has_role('ROLE_USER')")
 *
 */
class BoardController extends FOSRestController
{

    /**
     * Lists all boards according to the query.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of boards matching the query",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @QueryParam(name="user", description="Username of the user from which the boards should be fetched", nullable=true)
     * @QueryParam(name="project", description="hash of the project from which the boards should be fetched", nullable=true)
     * @QueryParam(name="archived", description="set to true to fetch closed boards. False by default.", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getBoardsAction(ParamFetcher $paramFetcher)
    {

        $qb = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Board")->createQueryBuilder("board");
        $qb->join("board.project", "project");
        $qb->andWhere("EXISTS(
                        SELECT pm
                        FROM TrevizProjectBundle:ProjectMembership pm
                        WHERE pm.project = project
                        AND pm.user = :currentUser
                       )");
        $qb->setParameter('currentUser', $this->getUser());

        if ($paramFetcher->get('project')) {
            $project = $this->getDoctrine()->getRepository("TrevizProjectBundle:Project")
                ->findOneBy(array("hash" => $paramFetcher->get("project")));

            if ($project) {
                $qb->andWhere("project = :project");
                $qb->setParameter("project", $project);
            }
        }

        if ($paramFetcher->get('user')) {
            $user = $this->getDoctrine()->getRepository('TrevizCoreBundle:User')
                ->findOneBy(array('username' => $paramFetcher->get('user')));
            $qb->andWhere("EXISTS(
                            SELECT task
                            FROM TrevizKanbanBundle:Task task
                            WHERE (
                              task.assignee = :user
                              OR task.supervisor = :user
                            )
                            AND task.column MEMBER OF board.columns
                           )");
            $qb->setParameter("user", $user);
        }

        if ($paramFetcher->get('archived') !== null) {
            $qb->andWhere("board.archived = :archived");
            $qb->setParameter("archived", $paramFetcher->get('archived'));
        } else {
            $qb->andWhere("board.archived = FALSE");
        }

        return $this->view($qb->getQuery()->getResult(), 200);

    }

    /**
     * Finds and returns a board.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Board is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No board is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the board",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getBoardAction($hash)
    {

        $board = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Board")
            ->findOneBy(array("hash" => $hash));

        if ($board) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $board->getProject(),
                    "user" => $this->getUser()
                ));

            if ($membership !== null) {
                return $this->view($board, 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No board was found for hash " . $hash, 404);

    }

    /**
     * Creates a new board entity.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Board is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to create the board",
     * )
     *
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid given object; for instance the project hash does not exist.",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function postBoardAction(Request $request)
    {
        $board = new Board();
        $form = $this->createForm('Treviz\KanbanBundle\Form\BoardType', $board);
        $form->submit($request->request->all());

        if ($form->isValid()) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $board->getProject(),
                    "user" => $this->getUser()
                ));

            if($membership !== null
            && in_array(ProjectPermissions::MANAGE_KANBAN, $membership->getRole()->getPermissions())) {

                $hash = hash("sha256", random_bytes(256));
                while ($this->getDoctrine()->getRepository("TrevizKanbanBundle:Board")->findOneByHash($hash) !== null) {
                    $hash = hash("sha256", random_bytes(256));
                }

                $board->setHash($hash);
                $em = $this->getDoctrine()->getManager();
                $em->persist($board);
                $em->flush();

                return $this->view($board, 201);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view($form, 422);

    }

    /**
     * Update an existing board
     *
     * @SWG\Response(
     *     response=200,
     *     description="Board is successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to update the board",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No board is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Given object is invalid; for instance the project hash does not exist.",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putBoardAction(Request $request, $hash)
    {
        $board = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Board")->findOneBy(array("hash" => $hash));
        $project = $board->getProject();

        if ($board !== null) {
            $form = $this->createForm(BoardType::class, $board);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $project,
                        "user" => $this->getUser()
                    ));

                if($membership !== null
                    && in_array(ProjectPermissions::MANAGE_KANBAN, $membership->getRole()->getPermissions())) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->view($board, 200);
                }

                return $this->view("You are not authorized to do this", 403);

            }

            return $this->view($form, 422);
        }

        return $this->view("No board was found for hash " . $hash, 404);

    }

    /**
     * Deletes a board.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Board is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to delete the board",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No board is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteBoardAction($hash)
    {
        $board = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Board")->findOneBy(array("hash" => $hash));

        if ($board !== null) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $board->getProject(),
                    "user" => $this->getUser()
                ));

            if($membership !== null
                && in_array(ProjectPermissions::MANAGE_KANBAN, $membership->getRole()->getPermissions())) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($board);
                $em->flush();

                return $this->view(null, 204);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No board was found for hash " . $hash, 404);
    }

}
