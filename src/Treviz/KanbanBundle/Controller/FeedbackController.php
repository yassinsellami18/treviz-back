<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 13/12/2017
 * Time: 18:03
 */

namespace Treviz\KanbanBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\KanbanBundle\Entity\Feedback;
use Treviz\KanbanBundle\Entity\Task;
use Treviz\KanbanBundle\Form\FeedbackType;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class FeedbackController
 * @package Treviz\KanbanBundle\Controller
 *
 * @Security("has_role('ROLE_USER')")
 */
class FeedbackController extends FOSRestController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Return an array of feedback.",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @QueryParam(name="given", description="If true, fetch the feedback that was given; otherwise fetch the received feedback", nullable=true)
     * @QueryParam(name="task", description="Hash of the task from which the feedback should be fetched", nullable=true)
     *
     * @Get("/boards/columns/tasks/feedback")
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getFeedbackAction(ParamFetcher $paramFetcher)
    {
        $s_task = $paramFetcher->get('task');
        $s_given = $paramFetcher->get('given');

        $qb = $this->getDoctrine()->getRepository('TrevizKanbanBundle:Feedback')->createQueryBuilder('feedback');

        if ($s_task) {
            $qb->join('feedback.task', 'task');
            $qb->andWhere('task.hash = :taskHash');
            $qb->setParameter('taskHash', $s_task);
        }

        if ($s_given) {
            $qb->andWhere('feedback.giver = :currentUser');
        } else {
            $qb->andWhere('feedback.receiver = :currentUser');
        }

        $qb->setParameter('currentUser', $this->getUser());

        return $this->view($qb->getQuery()->getResult(), 200);
    }

    /**
     * Post a feedback for a task.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Feedback was successfully given.",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The current user cannot submit feedback for this task.",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No task is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Post("/boards/columns/tasks/{hash}/feedback")
     *
     * @param Request $request
     * @param string $hash Hash of the task to which add feedback.
     * @return \FOS\RestBundle\View\View
     */
    public function postFeedbackAction(Request $request, $hash)
    {
        /** @var Task $task */
        $task = $this->getDoctrine()->getRepository('TrevizKanbanBundle:Task')->findOneByHash($hash);

        if ($task) {

            $feedback = new Feedback();
            $form = $this->createForm(FeedbackType::class, $feedback);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                if ($this->getUser() == $task->getSupervisor()) {

                    $feedback->setTask($task);
                    $feedback->setGiver($this->getUser());
                    $feedback->setReceiver($task->getAssignee());

                    $hash = hash("sha256", random_bytes(256));
                    while ($this->getDoctrine()->getRepository("TrevizKanbanBundle:Feedback")->findOneByHash($hash) !== null) {
                        $hash = hash("sha256", random_bytes(256));
                    }

                    $feedback->setHash($hash);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($feedback);
                    $em->flush();

                    return $this->view($feedback, 201);

                }

                return $this->view('You cannot post feedback to a task you do not supervise.', 404);

            }

        }

        return $this->view('No task was found for hash ' . $hash, 404);

    }

}