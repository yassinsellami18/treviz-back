<?php

namespace Treviz\KanbanBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CoreBundle\Entity\User;
use Treviz\KanbanBundle\Entity\Column;
use Treviz\KanbanBundle\Entity\Task;
use Swagger\Annotations as SWG;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Treviz\ProjectBundle\Entity\ProjectMembership;

/**
 * Task controller.
 *
 * @Security("has_role('ROLE_USER')")
 *
 */
class TaskController extends FOSRestController
{

    /**
     * Lists all tasks of a column.
     * By default, the archived tasks are not sent back, but it can be changed with the query parameter ?show_archived.
     *
     * @QueryParam(name="show_archived", description="List archived tasks alongside pending ones", nullable=true)
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of tasks matching the query",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No column is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the column's tasks",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Get("/boards/columns/{hash}/tasks")
     *
     * @param string $hash Hash of the column from which fetch the task.
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getColumnTasksAction($hash, ParamFetcher $paramFetcher)
    {
        /** @var Column $column */
        $column = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Column")
            ->findOneBy(array("hash" => $hash));

        if ($column) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $column->getBoard()->getProject(),
                    "user" => $this->getUser()
                ));

            if ($membership !== null) {
                if ($paramFetcher->get('show_archived')) {
                    $tasks = $column->getTasks()->filter(function(Task $task) { return $task->isArchived(); } );
                } else {
                    $tasks = $column->getTasks();
                }
                return $this->view($tasks, 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No column was found for hash " . $hash, 404);

    }

    /**
     * Fetches tasks according to a query.
     *
     * @QueryParam(name="assignee", description="Username of the user to whom the task must be assigned", nullable=true)
     * @QueryParam(name="supervisor", description="Username of the user who supervises the task", nullable=true)
     * @QueryParam(name="to_approve", description="True if the task is waiting approval by the current user", nullable=true)
     *
     * @View(serializerGroups={"Default", "job"}, serializerEnableMaxDepthChecks=true)
     *
     * @Get("/boards/columns/tasks")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of tasks matching the query",
     * )
     *
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the column's tasks",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getTasksAction(ParamFetcher $paramFetcher) {
        $qb = $this->getDoctrine()->getRepository('TrevizKanbanBundle:Task')->createQueryBuilder('task');

        if ($assignee = $paramFetcher->get('assignee')) {
            $qb->join('task.assignee', 'assignee');
            $qb->andWhere('assignee.username = :assignee')
                ->setParameter('assignee', $assignee);
        }

        if ($supervisor = $paramFetcher->get('supervisor')) {
            $qb->join('task.supervisor', 'supervisor');
            $qb->andWhere('supervisor.username = :supervisor')
                ->setParameter('supervisor', $supervisor);
        }

        if ($paramFetcher->get('to_approve')) {
            $qb->andWhere('task.pendingApproval = true');
        }

        $qb->andWhere('task.archived = false');

        $qb->join('task.column', 'column');
        $qb->andWhere('EXISTS (
                        SELECT pm
                        FROM TrevizProjectBundle:ProjectMembership pm
                        WHERE pm.user = :user
                        AND EXISTS (
                            SELECT board
                            FROM TrevizKanbanBundle:Board board
                            WHERE board.project = pm.project
                            AND column MEMBER OF board.columns
                        )
                      )');
        $qb->setParameter('user', $this->getUser());

        return $this->view($qb->getQuery()->getResult(), 200);
    }

    /**
     * Finds and returns a task.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Task is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No task is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the task",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Get("/boards/columns/tasks/{hash}")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getTaskAction($hash)
    {

        $task = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Task")
            ->findOneBy(array("hash" => $hash));

        if ($task) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $task->getColumn()->getBoard()->getProject(),
                    "user" => $this->getUser()
                ));

            if ($membership !== null) {
                return $this->view($task, 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No task was found for hash " . $hash, 404);

    }


    /**
     * Creates a new task.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Task is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to create the task",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Post("/boards/columns/{hash}/tasks")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postTaskAction(Request $request, $hash)
    {

        $column = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Column")->findOneBy(array("hash" => $hash));

        if ($column !== null) {

            $task = new Task();
            $form = $this->createForm('Treviz\KanbanBundle\Form\TaskType', $task);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $column->getBoard()->getProject(),
                        "user" => $this->getUser()
                    ));

                if($membership !== null) {

                    $hash = hash("sha256", random_bytes(256));
                    while ($this->getDoctrine()->getRepository("TrevizKanbanBundle:Task")->findOneByHash($hash) !== null) {
                        $hash = hash("sha256", random_bytes(256));
                    }

                    /*
                     * Checks if the assignee and supervisor are member of the project.
                     */
                    if( $assignee = $task->getAssignee() ) {
                        $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                            ->findOneBy(array(
                                    "user" => $assignee,
                                    "project" => $column->getBoard()->getProject()
                                )
                            );
                        if ($membership == null) {
                            return $this->view("Assignees must be part of the project", 403);
                        }
                    }

                    $supervisor = $task->getSupervisor();
                    $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                        ->findOneBy(array(
                                "user" => $supervisor,
                                "project" => $column->getBoard()->getProject()
                            )
                        );
                    if ($membership == null) {
                        return $this->view("Supervisors must be part of the project", 403);
                    }

                    if ($assignee == $supervisor) {
                        return $this->view("The task assignee must be different from its supervisor.", 403);
                    }

                    $task->setColumn($column);
                    $task->setHash($hash);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($task);
                    $em->flush();

                    return $this->view($task, 201);
                }

                return $this->view("You are not authorized to do this", 403);

            }

            return $this->view($form, 422);
        }

        return $this->view("No column was found for hash " . $hash, 404);

    }


    /**
     * Edit an existing task.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Task is successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to update the task",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Put("/boards/columns/tasks/{hash}")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putTaskAction(Request $request, $hash)
    {

        $task = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Task")->findOneBy(array("hash" => $hash));

        if ($task !== null) {

            if ($this->getUser() === $task->getSupervisor()) {

                $form = $this->createForm('Treviz\KanbanBundle\Form\TaskType', $task);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    if( $assignee = $task->getAssignee() ) {
                        $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                            ->findOneBy(array(
                                    "user" => $assignee,
                                    "project" => $task->getColumn()->getBoard()->getProject()
                                )
                            );
                        if ($membership == null) {
                            return $this->view("Assignees must be part of the project", 403);
                        }
                    }

                    $supervisor = $task->getSupervisor();
                    $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                        ->findOneBy(array(
                                "user" => $supervisor,
                                "project" => $task->getColumn()->getBoard()->getProject()
                            )
                        );
                    if ($membership == null) {
                        return $this->view("Supervisor must be part of the project", 403);
                    }

                    if ($assignee !== null && $assignee->getUsername() == $supervisor->getUsername()) {
                        return $this->view("The task assignee must be different from its supervisor.", 403);
                    }

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->view($task, 200);
                }

                return $this->view($form, 422);

            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No task was found for hash " . $hash, 404);

    }

    /**
     * Deletes a task entity.
     * Only a project administrator or the task's supervisor can delete it.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Task is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to delete the task",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No task is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Delete("/boards/columns/tasks/{hash}")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteColumnTaskAction(String $hash)
    {

        /** @var Task $task */
        $task = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Task")->findOneBy(array("hash" => $hash));
        $currentUser = $this->getUser();

        if ($task !== null) {

            /** @var ProjectMembership $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $task->getColumn()->getBoard()->getProject(),
                    "user" => $currentUser
                ));

            if(($membership !== null && in_array(ProjectPermissions::MANAGE_KANBAN, $membership->getRole()->getPermissions()))
                || $task->getSupervisor() == $currentUser) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($task);
                $em->flush();

                return $this->view(null, 204);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No task was found for hash " . $hash, 404);
    }


    /**
     * Submits a task for approval.
     * Only the task's assignee can submit it.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Task was successfully submitted for approval",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to submit the task for approval",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No task is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Post("/boards/columns/tasks/{hash}/submit")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function submitForApprovalAction($hash)
    {
        /** @var Task $task */
        $task = $this->getDoctrine()->getRepository('TrevizKanbanBundle:Task')->findOneByHash($hash);

        if ($task) {

            if ($task->getAssignee() == $this->getUser()) {
                $task->setPendingApproval(true);
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                return $this->view($task);

            }

            return $this->view('You are not authorized to submit this task for approval', 403);

        }

        return $this->view('No task was found for has ' . $hash, 404);
    }

}
