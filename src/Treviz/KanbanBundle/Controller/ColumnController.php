<?php

namespace Treviz\KanbanBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Treviz\KanbanBundle\Entity\Column;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\View;

/**
 * Column controller.
 *
 * @Security("has_role('ROLE_USER')")
 *
 */
class ColumnController extends FOSRestController
{

    /**
     * Lists all columns of a board.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of columns matching the query",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No board is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the board's columns",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @View(serializerGroups={"Default", "columns"},serializerEnableMaxDepthChecks=true)
     *
     * @param string $hash Hash of the board from which fetch the column.
     * @return \FOS\RestBundle\View\View
     */
    public function getBoardColumnsAction($hash)
    {
        $board = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Board")
            ->findOneBy(array("hash" => $hash));

        if ($board) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $board->getProject(),
                    "user" => $this->getUser()
                ));

            if ($membership !== null) {
                return $this->view($board->getColumns(), 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No board was found for hash " . $hash, 404);

    }

    /**
     * Finds and returns a column.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Column is successfully fetched",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No column is found for the given hash",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to fetch the column",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Get("/boards/columns/{hash}")
     *
     * @View(serializerGroups={"Default", "columns"},serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function getBoardColumnAction($hash)
    {

        $column = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Column")
            ->findOneBy(array("hash" => $hash));

        if ($column) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $column->getBoard()->getProject(),
                    "user" => $this->getUser()
                ));

            if ($membership !== null) {
                return $this->view($column, 200);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No column was found for hash " . $hash, 404);

    }


    /**
     * Creates a new column.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Column is successfully created",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to create the column",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function postBoardColumnAction(Request $request, $hash)
    {

        $board = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Board")->findOneBy(array("hash" => $hash));

        if ($board !== null) {

            $column = new Column();
            $form = $this->createForm('Treviz\KanbanBundle\Form\ColumnType', $column);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $board->getProject(),
                        "user" => $this->getUser()
                    ));

                if($membership !== null
                    && in_array(ProjectPermissions::MANAGE_KANBAN, $membership->getRole()->getPermissions())) {

                    $hash = hash("sha256", random_bytes(256));
                    while ($this->getDoctrine()->getRepository("TrevizKanbanBundle:Column")->findOneByHash($hash) !== null) {
                        $hash = hash("sha256", random_bytes(256));
                    }

                    $column->setBoard($board);
                    $column->setHash($hash);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($column);
                    $em->flush();

                    return $this->view($column, 201);
                }

                return $this->view("You are not authorized to do this", 403);

            }

            return $this->view($form, 422);
        }

        return $this->view("No board was found for hash " . $hash, 404);

    }


    /**
     * Edit an existing column.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Column is successfully updated",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="The user is not authorized to update the column",
     * )
     *
     * @SWG\Response(
     *     response=422,
     *     description="Invalid data",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Put("/boards/columns/{hash}")
     *
     * @View(serializerGroups={"Default", "columns"},serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putBoardColumnAction(Request $request, $hash)
    {

        $column = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Column")->findOneBy(array("hash" => $hash));

        if ($column !== null) {

            $form = $this->createForm(Column::class, $column);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $column->getBoard()->getProject(),
                        "user" => $this->getUser()
                    ));

                if($membership !== null
                    && in_array(ProjectPermissions::MANAGE_KANBAN, $membership->getRole()->getPermissions())) {

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->view($column, 200);
                }

                return $this->view("You are not authorized to do this", 403);

            }

            return $this->view($form, 422);
        }

        return $this->view("No column was found for hash " . $hash, 404);

    }

    /**
     * Deletes a column entity.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Column is successfully deleted",
     * )
     *
     * @SWG\Response(
     *     response=403,
     *     description="User is not authorized to delete the column",
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="No column is found for the given hash",
     * )
     *
     * @SWG\Tag(name="kanban")
     *
     * @Delete("/boards/columns/{hash}")
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deleteBoardColumnAction($hash)
    {

        $column = $this->getDoctrine()->getRepository("TrevizKanbanBundle:Column")->findOneBy(array("hash" => $hash));

        if ($column !== null) {

            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $column->getBoard()->getProject(),
                    "user" => $this->getUser()
                ));

            if($membership !== null
                && in_array(ProjectPermissions::MANAGE_KANBAN, $membership->getRole()->getPermissions())) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($column);
                $em->flush();

                return $this->view(null, 204);
            }

            return $this->view("You are not authorized to do this", 403);

        }

        return $this->view("No column was found for hash " . $hash, 404);
    }

}
