<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 13/12/2017
 * Time: 18:04
 */

namespace Treviz\KanbanBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FeedbackType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('feedback')
            ->add('praise');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\KanbanBundle\Entity\Feedback',
            'csrf_protection' => false
        ));
    }
}