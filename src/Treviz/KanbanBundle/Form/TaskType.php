<?php

namespace Treviz\KanbanBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\CoreBundle\Form\DataTransformer\UsernameArrayToUserCollectionTransformer;
use Treviz\CoreBundle\Form\DataTransformer\UsernameToUserTransformer;
use Treviz\KanbanBundle\Form\DataTransformer\HashesToLabelArrayTransformer;
use Treviz\KanbanBundle\Form\DataTransformer\HashToColumnTransformer;
use Treviz\KanbanBundle\Form\DataTransformer\HashToLabelTransformer;
use Treviz\KanbanBundle\Form\DataTransformer\ISO8601ToDateTimeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{

    /** @var  EntityManager */
    private $em;

    /**
     * BoardType constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array("required" => true))
            ->add('deadline', TextType::class, array("required" => false))
            ->add('description', TextType::class, array("required" => false))
            ->add('reward', TextType::class, array("required" => false))
            ->add('column', TextType::class, array("required" => false))
            ->add('position', TextType::class, array("required" => false))
            ->add('archived')
            ->add('pendingApproval')
            ->add('labels', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type' => TextType::class
            ))
            ->add('assignee', TextType::class, array(
                'required' => false
            ))
            ->add('supervisor', TextType::class, array(
                'required' => false
            ));

        $builder->get('deadline')->addModelTransformer(new ISO8601ToDateTimeTransformer(), true);
        $builder->get('labels')->addModelTransformer(new HashesToLabelArrayTransformer($this->em), true);
        $builder->get('assignee')->addModelTransformer(new UsernameToUserTransformer($this->em), true);
        $builder->get('supervisor')->addModelTransformer(new UsernameToUserTransformer($this->em), true);
        $builder->get('column')->addModelTransformer(new HashToColumnTransformer($this->em), true);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\KanbanBundle\Entity\Task',
            'csrf_protection' => false
        ));
    }

}
