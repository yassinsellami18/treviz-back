<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 07/11/2017
 * Time: 13:38
 */

namespace Treviz\KanbanBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\KanbanBundle\Entity\Task;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class HashToTaskTransformer implements DataTransformerInterface
{

    /** @var  EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Task $task
     * @return string
     */
    public function transform($task)
    {
        if($task == null) {
            return '';
        }

        return $task->getHash();
    }

    /**
     * @param string $hash
     * @return Task|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $task = $this->em->getRepository("TrevizKanbanBundle:Task")->findOneByHash($hash);

        if ($task == null) {
            throw new TransformationFailedException(sprintf('No task with hash ' . $hash . ' exists.'));
        }

        return $task;

    }
}