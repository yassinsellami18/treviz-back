<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 07/11/2017
 * Time: 13:38
 */

namespace Treviz\KanbanBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\KanbanBundle\Entity\Label;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class HashToLabelTransformer implements DataTransformerInterface
{

    /** @var  EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Label $label
     * @return string
     */
    public function transform($label)
    {
        if($label == null) {
            return '';
        }

        return $label->getHash();
    }

    /**
     * @param string $hash
     * @return Label|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $label = $this->em->getRepository("TrevizKanbanBundle:Label")->findOneByHash($hash);

        if ($label == null) {
            throw new TransformationFailedException(sprintf('No label with hash ' . $hash . ' exists.'));
        }

        return $label;

    }
}