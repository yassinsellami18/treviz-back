<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 07/11/2017
 * Time: 13:38
 */

namespace Treviz\KanbanBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Treviz\KanbanBundle\Entity\Column;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class HashesToTaskArrayTransformer implements DataTransformerInterface
{

    /** @var  EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Takes an ArrayCollection of tasks, and returns an array of their hashes.
     *
     * @param ArrayCollection $tasks
     * @return string
     */
    public function transform($tasks)
    {
        $labelsHash = [];

        foreach ($tasks as $task) {
            $labelsHash[] = $task->getHash();
        }

        return $labelsHash;
    }

    /**
     * Takes an array of hashes, and return an ArrayCollection of the matching lables.
     *
     * @param array $hashes
     * @return Column|null|object|void
     */
    public function reverseTransform($hashes)
    {

        $labels = new ArrayCollection();

        foreach ($hashes as $hash) {
            $label = $this->em->getRepository("TrevizKanbanBundle:Task")->findOneByHash($hash);

            if ($label == null) {
                throw new TransformationFailedException(sprintf('No task with hash ' . $hash . ' exists.'));
            }

            $labels->add($label);
        }

        return $labels;

    }
}