<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 07/11/2017
 * Time: 13:38
 */

namespace Treviz\KanbanBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\KanbanBundle\Entity\Column;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class HashToColumnTransformer implements DataTransformerInterface
{

    /** @var  EntityManager */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Column $column
     * @return string
     */
    public function transform($column)
    {
        if($column == null) {
            return '';
        }

        return $column->getHash();
    }

    /**
     * @param string $hash
     * @return Column|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $column = $this->em->getRepository("TrevizKanbanBundle:Column")->findOneByHash($hash);

        if ($column == null) {
            throw new TransformationFailedException(sprintf('No column with hash ' . $hash . ' exists.'));
        }

        return $column;

    }
}