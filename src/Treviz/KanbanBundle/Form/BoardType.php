<?php

namespace Treviz\KanbanBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\ProjectBundle\Form\DataTransformer\HashToProjectTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoardType extends AbstractType
{

    /** @var  EntityManager */
    private $em;

    /**
     * BoardType constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('description')
            ->add('archived')
            ->add('project', TextType::class, array('required' => false));

        $builder->get('project')->addModelTransformer(new HashToProjectTransformer($this->em), true);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\KanbanBundle\Entity\Board',
            'csrf_protection' => false
        ));
    }

}
