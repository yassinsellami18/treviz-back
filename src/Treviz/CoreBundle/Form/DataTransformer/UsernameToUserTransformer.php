<?php

namespace Treviz\CoreBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\CoreBundle\Entity\User;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class UsernameToUserTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * UsernameToUserTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param User $user
     * @return string
     */
    public function transform($user)
    {
        if($user == null) {
            return '';
        }

        return $user->getUsername();
    }

    /**
     * @param string $username
     * @return User|null|object|void
     */
    public function reverseTransform($username)
    {
        if (!$username) {
            return;
        }

        $user = $this->em->getRepository("TrevizCoreBundle:User")->findOneByUsername($username);

        if ($user == null) {
            throw new TransformationFailedException(sprintf('No user with username ' . $username . ' exists.'));
        }

        return $user;

    }
}