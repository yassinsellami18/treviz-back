<?php

namespace Treviz\CoreBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManager;
use Treviz\CoreBundle\Entity\Post;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * Post: huber
 * Date: 16/07/2017
 * Time: 23:45
 */
class HashToPostTransformer implements DataTransformerInterface
{
    private $em;

    /**
     * HashToPostTransformer constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Post $post
     * @return string
     */
    public function transform($post)
    {
        if($post == null) {
            return '';
        }

        return $post->getHash();
    }

    /**
     * @param string $hash
     * @return Post|null|object|void
     */
    public function reverseTransform($hash)
    {
        if (!$hash) {
            return;
        }

        $post = $this->em->getRepository("TrevizCoreBundle:Post")->findOneByHash($hash);

        if ($post == null) {
            throw new TransformationFailedException(sprintf('No post with hash ' . $hash . ' exists.'));
        }

        return $post;

    }
}