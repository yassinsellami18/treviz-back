<?php

namespace Treviz\CoreBundle\Form;

use Doctrine\ORM\EntityManager;
use Treviz\SkillBundle\Form\DataTransformer\NamesToSkillsTransformer;
use Treviz\TagBundle\Form\DataTransformer\NamesToTagsTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{

    private $em;

    /**
     * UserType constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', TextType::class, array("required" => true, 'error_bubbling' => false))
            ->add('lastName', TextType::class, array("required" => true, 'error_bubbling' => false))
            ->add('username', TextType::class, array("required" => true, 'error_bubbling' => false))
            ->add('email', TextType::class, array("required" => true, 'error_bubbling' => false))
            ->add('description', TextType::class, array("required" => false, 'error_bubbling' => false))
            ->add('address', TextType::class, array("required" => false, 'error_bubbling' => false))
            ->add('avatar', FileType::class, array("required" => false, 'error_bubbling' => false))
            ->add('avatarUrl', TextType::class, array("required" => false, 'error_bubbling' => false))
            ->add('backgroundImage', FileType::class, array("required" => false, 'error_bubbling' => false))
            ->add('backgroundImageUrl', TextType::class, array("required" => false, 'error_bubbling' => false))
            ->add('skills', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type' => TextType::class,
                'error_bubbling' => false,
            ))
            ->add('interests', CollectionType::class, array(
                'allow_add' => true,
                'required' => false,
                'entry_type' => TextType::class,
                'error_bubbling' => false,
            ));

        $builder->get('skills')
            ->addModelTransformer(new NamesToSkillsTransformer($this->em));
        $builder->get('interests')
            ->addModelTransformer(new NamesToTagsTransformer($this->em));

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Treviz\CoreBundle\Entity\User',
            'csrf_protection' => false
        ));
    }

}
