<?php

namespace Treviz\CoreBundle\Services;

use Doctrine\ORM\EntityManager;
use Treviz\CoreBundle\Entity\User;


class UserService {

    private $em;

    public function __construct(EntityManager $entityManager) {
        $this->em = $entityManager;
    }

    public function persistUser(User $user) {
        $this->em->persist($user);
        $this->em->flush();
    }

    public function deleteUser(User $user) {
        $this->em->remove($user);
        $this->em->flush();
    }

}