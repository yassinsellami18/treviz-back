<?php

namespace Treviz\CoreBundle\Services;

use Symfony\Component\Templating\EngineInterface;
use Treviz\CoreBundle\Entity\User;
use Treviz\ChatBundle\Entity\Message;
use Treviz\KanbanBundle\Entity\Board;
use Treviz\ProjectBundle\Entity\Project;

class MailerService {

    private $mailer;
    private $mailerUser;
    private $templating;

    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating, $mailerUser) {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->mailerUser = $mailerUser;
    }

    public function sendRegistrationMail(User $user, String $url)
    {
        $mail = (new \Swift_Message())
            ->setFrom($this->mailerUser)
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render(
                    'mails/registration.html.twig',
                    array('url' => $url)
                ),
                'text/html'
            );
        $result = $this->mailer->send($mail, $errors);
        return array(
            "result" => $result,
            "errors" => $errors
        );
    }

    public function sendResetPasswordMail(User $user, $url)
    {
        $mail = (new \Swift_Message())
            ->setFrom($this->mailerUser)
            ->setTo($user->getEmail())
            ->setSubject('Forgotten Password')
            ->setBody(
                $this->templating->render(
                    'mails/reset.html.twig',
                    array('url' => $url, 'user' => $user->getFirstName())
                ),
                'text/html'
            );
        $result = $this->mailer->send($mail, $errors);
        return array(
            "result" => $result,
            "errors" => $errors
        );
    }

    public function sendUpdateMail(User $user, string $message, string $url) {
        $mail = (new \Swift_Message())
            ->setFrom($this->mailerUser)
            ->setTo($user->getEmail())
            ->setSubject('Forgotten Password')
            ->setBody(
                $this->templating->render(
                    'mails/update.html.twig',
                    array('url' => $url, 'message' => $message)
                ),
                'text/html'
            );
        $result = $this->mailer->send($mail, $errors);
        return array(
            "result" => $result,
            "errors" => $errors
        );
    }

}