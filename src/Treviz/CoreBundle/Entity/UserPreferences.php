<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 25/06/2018
 * Time: 22:47
 */

namespace Treviz\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;


/**
 * @ORM\Entity(repositoryClass="Treviz\CoreBundle\Repository\UserPreferencesRepository")
 * @ORM\Table(name="user_preferences")
 */
class UserPreferences
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var boolean
     */
    private $onInvitation;

    /**
     * @var boolean
     */
    private $onCandidacyChange;

    /**
     * @var boolean
     */
    private $onDirectMessage;

    /**
     * @var boolean
     */
    private $onChatRoomMessage;

    /**
     * @var boolean
     */
    private $lightTheme;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="Treviz\CoreBundle\Entity\User")
     */
    private $user;

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isOnInvitation(): bool
    {
        return $this->onInvitation;
    }

    /**
     * @param bool $onInvitation
     */
    public function setOnInvitation(bool $onInvitation): void
    {
        $this->onInvitation = $onInvitation;
    }

    /**
     * @return bool
     */
    public function isOnCandidacyChange(): bool
    {
        return $this->onCandidacyChange;
    }

    /**
     * @param bool $onCandidacyChange
     */
    public function setOnCandidacyChange(bool $onCandidacyChange): void
    {
        $this->onCandidacyChange = $onCandidacyChange;
    }

    /**
     * @return bool
     */
    public function isLightTheme(): bool
    {
        return $this->lightTheme;
    }

    /**
     * @param bool $lightTheme
     */
    public function setLightTheme(bool $lightTheme): void
    {
        $this->lightTheme = $lightTheme;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function isOnDirectMessage(): bool
    {
        return $this->onDirectMessage;
    }

    /**
     * @param bool $onDirectMessage
     */
    public function setOnDirectMessage(bool $onDirectMessage): void
    {
        $this->onDirectMessage = $onDirectMessage;
    }

    /**
     * @return bool
     */
    public function isOnChatRoomMessage(): bool
    {
        return $this->onChatRoomMessage;
    }

    /**
     * @param bool $onChatRoomMessage
     */
    public function setOnChatRoomMessage(bool $onChatRoomMessage): void
    {
        $this->onChatRoomMessage = $onChatRoomMessage;
    }

}