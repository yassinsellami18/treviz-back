<?php

namespace Treviz\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlacklistedMailDomain are used to prevent some domains to be used for user registration, and avoid spam.
 *
 * @ORM\Table(name="blacklisted_mail_domain")
 * @ORM\Entity(repositoryClass="Treviz\CoreBundle\Repository\BlacklistedMailDomainRepository")
 */
class BlacklistedMailDomain
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255, unique=true)
     */
    private $domain;


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     */
    public function setDomain(string $domain)
    {
        $this->domain = $domain;
    }

}