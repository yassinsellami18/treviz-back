<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 29/06/2018
 * Time: 23:41
 */

namespace Treviz\CoreBundle\Events;


use Symfony\Component\EventDispatcher\Event;
use Treviz\CoreBundle\Entity\Post;

class PostEvent extends Event
{

    const NAME = 'core.post.created';

    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function getPost()
    {
        return $this->post;
    }

}