<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 13/02/2017
 * Time: 17:12
 */

namespace Treviz\CoreBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Treviz\CommunityBundle\Entity\Community;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CommunityBundle\Entity\CommunityRole;
use Treviz\SkillBundle\Entity\Skill;
use Treviz\TagBundle\Entity\Tag;
use Treviz\CoreBundle\Entity\User;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Treviz\ProjectBundle\Entity\Project;
use Treviz\ProjectBundle\Entity\ProjectMembership;
use Treviz\ProjectBundle\Entity\ProjectRole;

class LoadData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        //Create communities

        $community = new Community();
        $community->setName("General");
        $community->setDescription("Communauté générale à laquelle tout le monde peut participer");
        $community->setLogoUrl("http://www.bluthemes.com/assets/img/blog/12/space-earth.jpg");
        $community->setPublic(true);
        $community->setOpen(true);
        $community->setHash("ze6d57ze6f1ds35fsdfsxwcvvzegzazdefqsf");

        $community1 = new Community();
        $community1->setName("Centrale Lille");
        $community1->setDescription("Communauté des Centraliens de Lille");
        $community1->setLogoUrl("https://pbs.twimg.com/profile_images/683711813553360901/NnSGp79U.png");
        $community1->setPublic(false);
        $community1->setOpen(false);
        $community1->setHash("gez3g5hergf6vcx1vbvghyuo7ythrtg");

        $community2 = new Community();
        $community2->setName("Treviz Community");
        $community2->setDescription("Communauté de MatchMyProject");
        $community2->setLogoUrl("https://matchmyproject.com/assets/images/logo.png");
        $community2->setPublic(true);
        $community2->setOpen(false);
        $community2->setHash("gedhgrsdv68c1xvb68tyjk8t9j4hfgfh");

        // CREATE USERS

        $userAdmin = new User();
        $userAdmin->setUsername("admin");
        $userAdmin->setFirstName("Ad");
        $userAdmin->setLastName("Min");
        $userAdmin->setPlainPassword("admin");
        $userAdmin->setEmail("admin@Treviz.com");
        $userAdmin->addRole("ROLE_ADMIN");
        $userAdmin->setEnabled(true);

        $user1 = new User();
        $user1->setUsername("searev");
        $user1->setFirstName("Bastien");
        $user1->setLastName("Huber");
        $user1->setEmail("searev@centralelille.fr");
        $user1->setPlainPassword("searev");
        $user1->setEnabled(true);


        $user2 = new User();
        $user2->setUsername("neako");
        $user2->setFirstName("Juliette");
        $user2->setLastName("Maës");
        $user2->setEmail("neako@centralelille.fr");
        $user2->setPlainPassword("neako");
        $user2->setEnabled(true);


        $user3 = new User();
        $user3->setUsername("nymous");
        $user3->setFirstName("Thomas");
        $user3->setLastName("Gaudin");
        $user3->setEmail("nymous@centralelille.fr");
        $user3->setPlainPassword("nymous");
        $user3->setEnabled(true);


        $user4 = new User();
        $user4->setUsername("achway");
        $user4->setFirstName("Naoufel");
        $user4->setLastName("Hatim");
        $user4->setEmail("achway@Treviz.com");
        $user4->setPlainPassword("achway");
        $user4->setEnabled(true);


        $user5 = new User();
        $user5->setUsername("jondoe");
        $user5->setFirstName("Jon");
        $user5->setLastName("Doe");
        $user5->setEmail("jondoe@Treviz.com");
        $user5->setPlainPassword("jondoe");
        $user5->setEnabled(true);


        $user6 = new User();
        $user6->setUsername("jonsmith");
        $user6->setFirstName("Jon");
        $user6->setLastName("Smith");
        $user6->setEmail("jonsmith@Treviz.com");
        $user6->setPlainPassword("jonsmith");
        $user6->setEnabled(true);

        // CREATE SKILLS

        $skill1 = new Skill();
        $skill2 = new Skill();
        $skill3 = new Skill();
        $skill4 = new Skill();
        $skill5 = new Skill();
        $skill6 = new Skill();
        $skill7 = new Skill();
        $skill8 = new Skill();
        $skill9 = new Skill();
        $skill10 = new Skill();

        $skill1->setName("Mécanique");
        $skill2->setName("Science des matériaux");
        $skill3->setName("Développement Web");
        $skill4->setName("Développement Mobile");
        $skill5->setName("Big Data");
        $skill6->setName("Machine Learning");
        $skill7->setName("Design");
        $skill8->setName("Pitch");
        $skill9->setName("Comptabilité");
        $skill10->setName("Anglais");


        // CREATE TAGS

        $tag1 = new Tag();
        $tag1->setName('Environnement');

        $tag2 = new Tag();
        $tag2->setName('Energie');

        $tag3 = new Tag();
        $tag3->setName('Numérique');

        $tag4= new Tag();
        $tag4->setName('Transports');

        $tag5 = new Tag();
        $tag5->setName('Nourriture');

        $tag6 = new Tag();
        $tag6->setName('Divertissement');

        $tag7 = new Tag();
        $tag7->setName('Santé');

        // CREATE PROJECTS

        $project1 = new Project();
        $project1->setName("WikiJourney");
        $project1->setShortDescription("Application utilisant les données des projets wikimedia pour créer des parcours touristiques");
        $project1->setDescription("Application utilisant les données des projets wikimedia pour créer des parcours touristiques");
        $project1->addSkill($skill2);
        $project1->addSkill($skill3);
        $project1->addSkill($skill4);
        $project1->addTag($tag3);
        $project1->addTag($tag6);
        $project1->setPublic(true);
        $project1->addCommunity($community);
        $project1->addCommunity($community1);
        $project1->setHash("nuyl5iop45kjhdfvc1v2x1z65etghgf1bhfe3");

        $project2 = new Project();
        $project2->setName("MatchMyProject");
        $project2->setShortDescription("Application mettant en relation des étudiants autour de projets");
        $project2->setDescription("Application mettant en relation des étudiants autour de projets");
        $project2->addSkill($skill2);
        $project2->addSkill($skill3);
        $project2->addSkill($skill4);
        $project2->addTag($tag3);
        $project2->addTag($tag6);
        $project2->addTag($tag1);
        $project2->addTag($tag2);
        $project2->setPublic(true);
        $project2->addCommunity($community);
        $project2->addCommunity($community2);
        $project2->setHash("gtr57hujtyuk61jh5gcsx1cxw21gtr45j6t684651d231596");

        $project3 = new Project();
        $project3->setName("BionicArm");
        $project3->setShortDescription("Création d'une prothèse de bras articulée, contrôlée par la pensée");
        $project3->setDescription("Création d'une prothèse de bras articulée, contrôlée par la pensée");
        $project3->addSkill($skill1);
        $project3->addSkill($skill3);
        $project3->addSkill($skill4);
        $project3->addTag($tag6);
        $project3->addTag($tag7);
        $project3->setPublic(false);
        $project3->addCommunity($community2);
        $project3->setHash("fdz64f1sq6f4rze9g4az6f1gdf5jhrgj8tr");

        // CREATE PROJECT ROLES;

        $roleCreator = new ProjectRole();
        $roleCreator->setName("Creator");
        $roleCreator->setPermissions(ProjectPermissions::getAvailablePermissions());
        $roleCreator->setHash("z7g684h9re4h68464f56g62e1r315");
        $roleCreator->setGlobal(true);
        $roleCreator->setDefaultCreator(true);
        $roleCreator->setDefaultMember(false);

        // CREATE PROJECT MEMBERSHIPS;

        $membership1 = new ProjectMembership();
        $membership1->setRole($roleCreator);
        $membership1->setUser($user1);
        $membership1->setProject($project1);
        $membership1->setHash("5678g68rze4gh86zeg4ze68gze");

        $membership2 = new ProjectMembership();
        $membership2->setRole($roleCreator);
        $membership2->setUser($user2);
        $membership2->setProject($project2);
        $membership2->setHash("h4df984bngyt94lkyu1s65g");

        $membership3 = new ProjectMembership();
        $membership3->setRole($roleCreator);
        $membership3->setUser($user3);
        $membership3->setProject($project3);
        $membership3->setHash("e5jh47trj7rtjtr8j91zef");

        // CREATE COMMUNITY ROLES;

        $cRoleCreator = new CommunityRole();
        $cRoleCreator->setName("Creator");
        $cRoleCreator->setDefaultCreator(true);
        $cRoleCreator->setGlobal(true);
        $cRoleCreator->setHash("nignzeg619ez1gz4eg1fd321");
        $cRoleCreator->setPermissions(CommunityPermissions::getAvailablePermissions());


        $cRoleMember = new CommunityRole();
        $cRoleMember->setName("Member");
        $cRoleMember->setHash("h6ty84ki1k2c1qsf1ze53g1s");
        $cRoleMember->addPermission(CommunityPermissions::MANAGE_POST);
        $cRoleMember->setGlobal(true);
        $cRoleMember->setDefaultMember(true);

        // CREATE COMMUNITY MEMBERSHIPS;

        $cMembership1 = new CommunityMembership();
        $cMembership1->setRole($cRoleCreator);
        $cMembership1->setUser($user1);
        $cMembership1->setCommunity($community);
        $cMembership1->setHash("5678g68rze4gh86zeg4ze68gze");

        $cMembership2 = new CommunityMembership();
        $cMembership2->setRole($cRoleCreator);
        $cMembership2->setUser($user2);
        $cMembership2->setCommunity($community1);
        $cMembership2->setHash("h4df984bngyt94lkyu1s65g");

        $cMembership3 = new CommunityMembership();
        $cMembership3->setRole($cRoleCreator);
        $cMembership3->setUser($user3);
        $cMembership3->setCommunity($community2);
        $cMembership3->setHash("e5jh47trj7rtjtr8j91zef");

        $cMembership4 = new CommunityMembership();
        $cMembership4->setRole($cRoleMember);
        $cMembership4->setUser($user4);
        $cMembership4->setCommunity($community);
        $cMembership4->setHash("j7ty4kl869lk1g6n1ty68jsdbf");

        $cMembership5 = new CommunityMembership();
        $cMembership5->setRole($cRoleMember);
        $cMembership5->setUser($user5);
        $cMembership5->setCommunity($community1);
        $cMembership5->setHash("yuoliu57o7ezaf1sd6vb5df4h9re");

        $cMembership6 = new CommunityMembership();
        $cMembership6->setRole($cRoleMember);
        $cMembership6->setUser($user6);
        $cMembership6->setCommunity($community2);
        $cMembership6->setHash("j687ty4j6h1fa68ze4a5a45e");

        // ADD INTERESTS FOR USERS

        $user1->addSkill($skill3);
        $user1->addSkill($skill4);
        $user2->addSkill($skill2);
        $user2->addSkill($skill3);
        $user3->addSkill($skill2);
        $user3->addSkill($skill4);
        $user4->addSkill($skill2);
        $user4->addSkill($skill3);
        $user5->addSkill($skill1);
        $user5->addSkill($skill3);
        $user6->addSkill($skill1);
        $user6->addSkill($skill4);

        $user1->addInterest($tag1);
        $user1->addInterest($tag2);
        $user1->addInterest($tag3);
        $user2->addInterest($tag4);
        $user2->addInterest($tag5);
        $user2->addInterest($tag3);
        $user3->addInterest($tag4);
        $user3->addInterest($tag6);
        $user3->addInterest($tag7);

        // PERSIS ENTITIES

        $manager->persist($community);
        $manager->persist($community1);
        $manager->persist($community2);
        $manager->persist($tag1);
        $manager->persist($tag2);
        $manager->persist($tag3);
        $manager->persist($tag4);
        $manager->persist($tag5);
        $manager->persist($tag6);
        $manager->persist($tag7);
        $manager->persist($skill1);
        $manager->persist($skill2);
        $manager->persist($skill3);
        $manager->persist($skill4);
        $manager->persist($skill5);
        $manager->persist($skill6);
        $manager->persist($skill7);
        $manager->persist($skill8);
        $manager->persist($skill9);
        $manager->persist($skill10);
        $manager->persist($userAdmin);
        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->persist($user4);
        $manager->persist($user5);
        $manager->persist($user6);
        $manager->persist($project1);
        $manager->persist($project2);
        $manager->persist($project3);
        $manager->persist($roleCreator);
        $manager->persist($membership1);
        $manager->persist($membership2);
        $manager->persist($membership3);
        $manager->persist($cRoleMember);
        $manager->persist($cRoleCreator);
        $manager->persist($cMembership1);
        $manager->persist($cMembership2);
        $manager->persist($cMembership3);
        $manager->persist($cMembership4);
        $manager->persist($cMembership5);
        $manager->persist($cMembership6);

        $manager->flush();
    }
}