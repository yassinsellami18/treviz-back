<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 30/06/2018
 * Time: 23:32
 */

namespace Treviz\CoreBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Treviz\BrainstormingBundle\EventSubscriber\BrainstormingEventSubscriber;
use Treviz\CommunityBundle\EventSubscriber\CommunityEventSubscriber;
use Treviz\CoreBundle\EventSubscriber\CoreEventSubscriber;
use Treviz\DocumentBundle\EventSubscriber\DocumentEventSubscriber;
use Treviz\ProjectBundle\EventSubscriber\ProjectEventSubscriber;

class Dispatcher
{

    private $dispatcher;

    public function __construct(
        CommunityEventSubscriber $communityEventSubscriber,
        CoreEventSubscriber $coreEventSubscriber,
        DocumentEventSubscriber $documentEventSubscriber,
        ProjectEventSubscriber $projectEventSubscriber,
        BrainstormingEventSubscriber $brainstormingEventSubscriber
    )
    {
        $this->dispatcher = new EventDispatcher();

        $this->dispatcher->addSubscriber($communityEventSubscriber);
        $this->dispatcher->addSubscriber($coreEventSubscriber);
        $this->dispatcher->addSubscriber($documentEventSubscriber);
        $this->dispatcher->addSubscriber($projectEventSubscriber);
        $this->dispatcher->addSubscriber($brainstormingEventSubscriber);
    }

    public function dispatch(string $name, Event $event)
    {
        $this->dispatcher->dispatch($name, $event);
    }

}