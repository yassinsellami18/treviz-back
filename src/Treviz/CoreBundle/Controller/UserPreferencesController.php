<?php
/**
 * Created by IntelliJ IDEA.
 * User: Bastien
 * Date: 25/06/2018
 * Time: 23:05
 */

namespace Treviz\CoreBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Treviz\CoreBundle\Entity\User;
use Treviz\CoreBundle\Form\UserPreferencesType;


/**
 * Class UserPreferencesController
 *
 * @package Treviz\CoreBundle\Controller
 *
 * @Security("has_role('ROLE_USER')")
 */
class UserPreferencesController extends FOSRestController
{

    /**
     * Retrieves the preferences of a user.
     *
     * @Rest\Get("users/{username}/preferences)
     *
     * @param $username
     *
     * @return \FOS\RestBundle\View\View
     */
    public function getPreferencesAction($username): \FOS\RestBundle\View\View
    {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")
            ->findOneByUsername($username);

        if ($user !== null) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            if ($currentUser->getUsername() === $username) {
                return $this->view($user->getPreferences(), 200);
            }

            return $this->view('You can only see your own preferences', 403);
        }

        return $this->view("No user was found with username $username", 404);
    }

    /**
     * Updates the preferences of a user.
     *
     * @Rest\Put("users/{username}/preferences)
     *
     * @param Request $request
     * @param         $username
     *
     * @return \FOS\RestBundle\View\View
     */
    public function updatePreferencesAction(Request $request, $username): \FOS\RestBundle\View\View
    {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository('TrevizCoreBundle:User')
            ->findOneByUsername($username);

        if ($user !== null) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            if ($currentUser->getUsername() === $username) {

                $preferences = $user->getPreferences();
                $form = $this->createForm(UserPreferencesType::class, $preferences);
                $form->submit($request);

                if($form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                    return $this->view($preferences, 200);
                }

                return $this->view('Invalid data', 400);
            }

            return $this->view('You can only see your own preferences', 403);
        }

        return $this->view("No user was found with username $username", 404);
    }

}