<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 11/02/2017
 * Time: 17:32
 */

namespace Treviz\CoreBundle\Controller;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CoreBundle\Entity\User;
use Swagger\Annotations as SWG;
use Treviz\CoreBundle\Services\MailerService;


class AccountController extends FOSRestController
{

    /**
     * Confirms the registration of a user
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user was successfully enabled",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Post("/users/{username}/confirm")
     *
     * @RequestParam(name="token", nullable=false)
     *
     * @param ParamFetcher $paramFetcher
     * @param              $username
     *
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function postConfirmationAction(ParamFetcher $paramFetcher, $username){

        $token = $paramFetcher->get("token");

        /** @var User $user */
        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(array("username" => $username));

        if ($user) {

            if(!$user->isEnabled()) {

                if($user->getConfirmationToken() === $token){

                    $today = new \DateTime();

                    /*
                     * If account was generated more than a day ago, ask the user to change his/her password.
                     */
                    if ($user->getPasswordRequestedAt()->add(new \DateInterval("P1D")) > $today) {

                        $user->setEnabled(true);
                        $tokenGenerator = $this->get('fos_user.util.token_generator');
                        $user->setConfirmationToken($tokenGenerator->generateToken());
                        $em = $this->getDoctrine()->getManager();
                        $em->flush();

                        return $this->view("User successfully enabled.", 200);

                    }

                    return $this->view("Token has expired. Please try resetting your password.", 408);

                }

                return $this->view("Invalid confirmation token.", 400);

            }

            return $this->view("User has already been enabled", 409);

        }

        return $this->view("No user was found for username " . $username, 404);

    }

    /**
     * Sends an email to retrieve a password, resets the confirmation token.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user successfully performed a reset password action",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Get("/reset-password")
     *
     * @QueryParam(name="email", nullable=false)
     *
     * @param ParamFetcher $paramFetcher
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getResetPasswordAction(ParamFetcher $paramFetcher){

        $email = $paramFetcher->get("email");

        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(["email" => $email]);

        if($user){

            $tokenGenerator = $this->get('fos_user.util.token_generator');

            $user->setConfirmationToken($tokenGenerator->generateToken());
            $user->setPasswordRequestedAt(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $url = $this->getParameter("frontend_url") . '/password-reset?user=' . $user->getUsername() . '&token=' . $user->getConfirmationToken();

            /** @var MailerService $mailerService */
            $mailerService = $this->get('treviz_core.service.mailer');
            ["result" => $result, "errors" => $errors] = $mailerService->sendResetPasswordMail($user, $url);

            if ($result) {
                return $this->handleView($this->view($result . ' email successfully sent', 200));

            } else {
                return $this->handleView($this->view($errors, 400));
            }

        }

        $view = $this->view("User was not found", 404);
        return $this->handleView($view);
    }

    /**
     * Resets the password of a user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a user successfully reset his or her password",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Post("/users/{username}/reset")
     *
     * @RequestParam(name="token", nullable=false)
     * @RequestParam(name="password", nullable=false)
     *
     * @param ParamFetcher $paramFetcher
     * @param              $username
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function postResetPasswordAction(ParamFetcher $paramFetcher, $username){

        $token = $paramFetcher->get("token");
        $password = $paramFetcher->get("password");

        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(["username" => $username]);

        if($user){

            $requestExpireAt = $user->getPasswordRequestedAt();
            $requestExpireAt->add(new \DateInterval("PT3H"));

            if($user->getPasswordRequestedAt() > new \DateTime()){

                if ($user->getConfirmationToken() === $token) {
                    $user->setPlainPassword($password);

                    $tokenGenerator = $this->get('fos_user.util.token_generator');

                    $user->setConfirmationToken($tokenGenerator->generateToken());
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    $view = $this->view("Password successfully changed", 200);
                    return $this->handleView($view);
                }

                $view = $this->view("Invalid token", 400);
                return $this->handleView($view);

            }

            $view = $this->view("Link expired at " . $requestExpireAt->format("Y-m-d H:i:s"), 408);
            return $this->handleView($view);

        }

        $view = $this->view("User was not found for username " . $username, 404);
        return $this->handleView($view);

    }

}