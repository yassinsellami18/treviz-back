<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 26/12/2017
 * Time: 21:10
 */

namespace Treviz\CoreBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

class DefaultController extends FOSRestController
{

    /**
     * Basic method that handles GET '' requests.
     * Returns the name of the organization, as well as the API version and route for the documentation.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAction()
    {
        $info = [
            "organization" => $this->getParameter('organization_name'),
            "documentation" => "/v1/doc",
            "version" => "1.0"
        ];
        return $this->handleView($this->view($info, 200));
    }

    /**
     * Returns the latest news of the platform (what was updated...)
     */
    public function getNewsAction() {
        $changelog = [];
        return $this->handleView($this->view($changelog, 200));
    }

}