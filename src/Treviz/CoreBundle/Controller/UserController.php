<?php
/**
 * Created by PhpStorm.
 * User: huber
 * Date: 10/02/2017
 * Time: 12:42
 */

namespace Treviz\CoreBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CoreBundle\Entity\User;
use Treviz\CoreBundle\Form\UserType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\FileParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\View;
use Swagger\Annotations as SWG;

/**
 * Class UserController
 * @package Treviz\CoreBundle\Controller
 *
 */
class UserController extends FOSRestController
{

    /**
     * Get the details of a specified user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when the information of a user was successfully fetched",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @View(serializerEnableMaxDepthChecks=true, serializerGroups={"Default", "user"})
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param string $username
     * @return \FOS\RestBundle\View\View
     */
    public function getUserAction($username){
        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(array("username" => $username));

        if ($user !== null
            && $user->isEnabled()) {
            $view = $this->view($user, 200);
            return $view;
        }

        $view = $this->view("No user was found for username", 404);
        return $view;

    }

    /**
     * Get the detail of all the users (or just the ones that match a query).
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned an array of users matching the query",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * [\p{L}\p{N}_\s]+ matches all words ands letters, using extended latin alphabet (ie characters like é,è,ê,ë... are accepted)
     * @QueryParam(name="name", requirements="[\p{L}\p{N}_\s]{1,16}", description="search  by name", nullable=true)
     * @QueryParam(name="tags", description="search by interests", nullable=true)
     * @QueryParam(name="skills", description="search by skills", nullable=true)
     * @QueryParam(name="offset", description="\d+", nullable=true)
     * @QueryParam(name="nb", description="\d+", nullable=true)
     *
     * @View(serializerGroups={"Default"},serializerEnableMaxDepthChecks=true)
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getUsersAction(ParamFetcher $paramFetcher)
    {
        $s_name = $paramFetcher->get("name");
        $s_interests = $paramFetcher->get("tags");
        $s_skills = $paramFetcher->get("skills");
        $s_start = $paramFetcher->get("offset");
        $s_nb = $paramFetcher->get("nb");

        $qb = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->createQueryBuilder("u");

        /*
         * If a name is specified, we look for a user whose first name, last name or username matches.
         */
        if($s_name){

            $qb->andWhere("UPPER(CONCAT(u.firstName, ' ', u.lastName, ' ', u.username)) LIKE :name
                        OR UPPER(CONCAT(u.lastName, ' ', u.firstName, ' ', u.username)) LIKE :name
                        OR UPPER(CONCAT(u.firstName, ' ', u.username, ' ', u.lastName)) LIKE :name
                        OR UPPER(CONCAT(u.firstName, ' ', u.username, ' ', u.lastName)) LIKE :name
                        OR UPPER(CONCAT(u.lastName, ' ', u.username, ' ', u.firstName)) LIKE :name
                        OR UPPER(CONCAT(u.username, ' ', u.firstName, ' ', u.lastName)) LIKE :name
                        OR UPPER(CONCAT(u.username, ' ', u.lastName, ' ', u.firstName)) LIKE :name
                        ");
            $qb->setParameter('name', '%'.strtoupper($s_name).'%');

        }

        if($s_interests){
            foreach ($s_interests as $key=>$s_interest_id){
                $qb->andWhere(":interest_".$key . " MEMBER OF u.tags");
                $qb->setParameter('interest_'.$key, $s_interest_id);
            }
        }

        if($s_skills){
            foreach ($s_skills as $key=>$s_skill_id){
                $qb->andWhere(":skill".$key . " MEMBER OF u.skills");
                $qb->setParameter('skill'.$key, $s_skill_id);
            }
        }

        if(isset($s_nb)){
            $qb->setMaxResults($s_nb);
        }
        else{
            $qb->setMaxResults(10);
        }

        if(isset($s_start)){
            $qb->setFirstResult($s_start);
        }

        $users = $qb->getQuery()->getResult();

        $view = $this->view($users, 200);
        return $view;
    }

    /**
     * Creates a new user in database and sends a confirmation email.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned a user was successfully created",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postUserAction(Request $request){

        $password = $request->get("password");

        $request->request->remove("password");
        $request->request->remove("newsletter");

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->submit($request->request->all());

        if ($password !== null && $form->isValid()) {

            $email = $user->getEmail();

            /*
             * We extract the domain name from the mail thanks to a regex.
             * Then, we check if it matches any blacklisted domain.
             */
            $pattern = '/^[a-zA-Z0-9.]+@/';
            preg_match($pattern, $email, $matches);

            if($matches){

                $domain = substr($email, strlen($matches[0]));

                if($this->getDoctrine()->getRepository("TrevizCoreBundle:BlacklistedMailDomain")->findOneBy(array("domain" => $domain))){

                    $view = $this->view("The email address using '@$domain' are not accepted.", 403);
                    return $this->handleView($view);

                }

            } else{

                $view = $this->view("Incorrect email", 400);
                return $this->handleView($view);

            }

            $userManager = $this->get('fos_user.user_manager');

            if(!$userManager->findUserByEmail($email) && !$userManager->findUserByUsername($user->getUsername())){

                /*
                 * Generate a confirmation token for the current user
                 */
                $tokenGenerator = $this->get('fos_user.util.token_generator');

                $user->setPlainPassword($password)
                    ->setEnabled(false)
                    ->setConfirmationToken($tokenGenerator->generateToken())
                    ->setPasswordRequestedAt(new \DateTime());

                $userManager->updateUser($user);

                $this->getDoctrine()->getManager()->flush();

                $url = $this->getParameter("frontend_url") . '/confirm-registration?user=' . $user->getUsername() . '&token=' . $user->getConfirmationToken();

                /*
                 * Send account confirmation email with swiftmailer.
                 */
                $mailerService = $this->get('treviz_core.service.mailer');
                ["result" => $result, "errors" => $errors] = $mailerService->sendRegistrationMail($user, $url);

                if ($result) {
                    return $this->handleView($this->view("User created. Please see your emails.", 200));
                }

                return $this->handleView($this->view('Unexpected Server error, the mail could not be sent', 500));

            }

            $view = $this->view("Email or Username already exists in database", 409);
            return $this->handleView($view);

        }

        return $this->handleView($this->view("Invalid data", 422));

    }

    /**
     * Updates an existing user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when the information of a user was successfully updated",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @param $username
     * @return \FOS\RestBundle\View\View
     */
    public function putUserAction(Request $request, $username)
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user->getUsername() === $username) {

            $newUsername = $request->get('username');

            if($newUsername == null
            || $newUsername == $username
            || !$this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(array("username" => $newUsername))) {

                /*
                 * We save email as it is constant, and cannot be modified for now.
                 */
                $email = $user->getEmail();

                /*
                 * Set the user avatar an background with the actual files so that the form builder does not crash.
                 */
                if ($avatarFileName = $user->getAvatar()) {
                    $user->setAvatar(new File($this->getParameter("kernel.root_dir") . '/../web' . $this->getParameter('upload_user_avatar_path') . $avatarFileName));
                }

                if ($backgroundLogoName = $user->getBackgroundImage()) {
                    $user->setBackgroundImage(new File($this->getParameter("kernel.root_dir") . '/../web' . $this->getParameter('upload_user_background_path') . $backgroundLogoName));
                }

                $form = $this->createForm(UserType::class, $user);
                $form->submit($request->request->all());

                if ($form->isValid()) {

                    /** @var File $avatar */
                    $avatar = $request->files->get("avatar");

                    if($avatar){

                        $avatarName = md5($avatar->getFilename() . uniqid()) . '.' . $avatar->guessExtension();

                        $avatar->move(
                            $this->getParameter("kernel.root_dir") .
                            '/../web' .
                            $this->getParameter('upload_user_avatar_path'),
                            $avatarName
                        );

                        $user->setAvatar($avatarName);
                        $user->setAvatarUrl(
                            $this->getParameter("backend_url") .
                            $this->getParameter("upload_user_avatar_path") .
                            $avatarName
                        );

                    }

                    /** @var File $logo */
                    $background = $request->files->get("background");

                    if($background){

                        $backgroundName = md5($background->getFilename() . uniqid()) . '.' . $background->guessExtension();

                        $background->move(
                            $this->getParameter("kernel.root_dir") .
                            '/../web' .
                            $this->getParameter('upload_user_background_path'),
                            $backgroundName
                        );

                        $user->setBackgroundImage($backgroundName);
                        $user->setBackgroundImageUrl(
                            $this->getParameter("backend_url") .
                            $this->getParameter("upload_user_background_path") .
                            $backgroundName
                        );

                    }

                    $user->setEmail($email);

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                    return $this->view($user, 200);

                }

                return $this->view($form, 422);

            }

            return $this->view("A user already exists with username " . $newUsername, 409);

        }

        return $this->view("You are not allowed to do this", 403);

    }

    /**
     * Deletes a user
     * 
     * @SWG\Response(
     *      response=204,
     *      description="Deletes a user from the platform, and all his or her personal data"
     * )
     * @SWG\Tag(name="users")
     * 
     * @Delete("/users/{username}")
     * 
     * @Security("has_role('ROLE_USER')")
     * 
     * @param ParamFetcher $paramFetcher
     * @param String $username
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteUserAction(Paramfetcher $paramFetcher, String $username) {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(array("username" => $username));

        if($user == null)  return $this->view("No user was found for this username", 404);

        if ($user != $this->getUser()) {
            $userService = $this->get('treviz_core.service.user');
            $userService->deleteUser($user);
            $this->getDoctrine()->getManager()->remove($user);

            $view = $this->view(null, 204);
            return $view;
        }

        $view = $this->view("You do not have the rights to perform this action", 403);
        return $view;
    }

    /**
     * Updates the avatar of a user.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when the avatar of a user was successfully updated",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Post("/users/{username}/avatar")
     *
     * @FileParam(name="avatar", image=true, nullable=false)
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param ParamFetcher $paramFetcher
     * @param $username
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function postUserAvatarAction(ParamFetcher $paramFetcher, $username){

        /** @var User $user */
        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(array("username" => $username));

        if($user == null) return $this->view("No user was found for this username", 404);

        if($user == $this->getUser()){

            $avatar = $paramFetcher->get("avatar");

            $avatarName = md5($avatar->getFilename() . uniqid()) . '.' . $avatar->guessExtension();

            $avatar->move(
                $this->getParameter("kernel.root_dir") .
                '/../web' .
                $this->getParameter('upload_user_avatar_path'),
                $avatarName
            );

            $user->setAvatar($avatarName);
            $user->setAvatarUrl(
                $this->getParameter("backend_url") .
                $this->getParameter("upload_user_avatar_path") .
                $avatarName
            );

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $view = $this->view($user, 200);
            return $view;
        }

        $view = $this->view("You do not have the rights to perform this action", 403);
        return $view;

    }

    /**
     * Creates a new background picture for a user
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when the background picture of a user was successfully updated",
     * )
     *
     * @SWG\Tag(name="users")
     *
     * @Post("/users/{username}/background")
     *
     * @FileParam(name="background", image=true, nullable=true)
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param ParamFetcher $paramFetcher
     * @param $username
     * @return \FOS\RestBundle\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function postUserBackgroundAction(ParamFetcher $paramFetcher, $username){

        /** @var User $user */
        $user = $this->getDoctrine()->getRepository("TrevizCoreBundle:User")->findOneBy(array("username" => $username));

        if($user == null) return $this->view("No user was found for this username", 404);

        if($user == $this->getUser()){

            $background = $paramFetcher->get("background");

            $backgroundName = md5($background->getFilename() . uniqid()) . '.' . $background->guessExtension();

            $background->move(
                $this->getParameter("kernel.root_dir") .
                '/../web' .
                $this->getParameter('upload_user_background_path'),
                $backgroundName
            );

            $user->setBackgroundImage($backgroundName);
            $user->setBackgroundImageUrl(
                $this->getParameter("backend_url") .
                $this->getParameter("upload_user_background_path") .
                $backgroundName
            );

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $view = $this->view($user, 200);
            return $view;
        }

        $view = $this->view("You do not have the rights to perform this action", 403);
        return $view;

    }

}