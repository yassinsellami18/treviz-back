<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 18/03/2017
 * Time: 14:45
 */

namespace Treviz\CoreBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Treviz\CoreBundle\Entity\Comment;
use Treviz\CoreBundle\Entity\Post;
use Treviz\CoreBundle\Entity\User;
use Treviz\CoreBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;

/**
 * @Security("has_role('ROLE_USER')")
 *
 * Class CommentController
 * @package Treviz\CoreBundle\Controller
 */
class CommentController extends FOSRestController
{

    /**
     * Create a new comment for a specific post
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a comment was successfully created",
     * )
     *
     * @SWG\Tag(name="comment")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param string $hash Hash of the post to which add a comment
     * @return \FOS\RestBundle\View\View
     * @internal param $hash
     */
    public function postPostCommentAction(Request $request, $hash)
    {
        $post = $this->getDoctrine()->getRepository('TrevizCoreBundle:Post')->findOneByHash($hash);

        if ($post) {
            $comment = new Comment();
            $form = $this->createForm(CommentType::class, $comment);
            $form->submit($request->request->all());

            if ($form->isValid()) {

                $hash = hash("sha256", random_bytes(256));
                while ($this->getDoctrine()->getRepository("TrevizCoreBundle:Comment")->findOneBy(array("hash" => $hash))) {
                    $hash = hash("sha256", random_bytes(256));
                }

                $comment->setPost($post);
                $comment->setAuthor($this->getUser());
                $comment->setHash($hash);

                $em = $this->getDoctrine()->getManager();
                $em->persist($comment);
                $em->flush();

                $view = $this->view($comment, 201);
                return $view;

            }

            $view = $this->view($form, 422);
            return $view;
        }

        $view = $this->view('No post was found for hash ' . $hash, 404);
        return $view;

    }

    /**
     * Updates a comment.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when a comment was successfully updated",
     * )
     *
     * @SWG\Tag(name="comment")
     *
     * @Put("/posts/comments/{hash}")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putPostCommentAction(Request $request, $hash)
    {

        /** @var Post $post */
        $comment = $this->getDoctrine()->getRepository("TrevizCoreBundle:Comment")
            ->findOneBy(array("hash" => $hash));
        $post = $comment->getPost();

        if($post){

            $user = $this->getUser();

            /*
             * TODO: Implement role management.
             */
            if ($user == $comment->getAuthor()) {

                $form = $this->createForm(CommentType::class, $comment);
                $form->submit($request->request->all());

                if ($form->isValid() && $comment->getAuthor()) {

                    $comment->setAuthor($user);
                    $comment->setPost($post);

                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    $view = $this->view($comment, 200);
                    return $view;

                }

                $view = $this->view("Invalid data", 422);
                return $view;

            }

            $view = $this->view("You are not authorized to do this", 403);
            return $view;

        }

        $view = $this->view("No post found for hash " .$hash, 404);
        return $view;
    }

    /**
     * Deletes a comment.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when a comment was successfully deleted",
     * )
     *
     * @SWG\Tag(name="comment")
     *
     * @Delete("/posts/comments/{hash}")
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deletePostCommentAction($hash)
    {

        /** @var Post $post */
        $comment = $this->getDoctrine()->getRepository("TrevizCoreBundle:Comment")
            ->findOneBy(array("hash" => $hash));
        $post = $comment->getPost();

        if($post){

            $user = $this->getUser();

            /*
             * TODO: Implement role management.
             */
            if ($user == $comment->getAuthor()) {

                $em = $this->getDoctrine()->getManager();
                $em->remove($comment);
                $em->flush();

                $view = $this->view(null, 204);
                return $view;

            }

            $view = $this->view("You are not authorized to do this", 403);
            return $view;

        }

        $view = $this->view("No post found for hash " .$hash, 404);
        return $view;
    }

}