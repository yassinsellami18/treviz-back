<?php
/**
 * Created by PhpStorm.
 * User: Bastien
 * Date: 18/03/2017
 * Time: 14:44
 */

namespace Treviz\CoreBundle\Controller;

use Doctrine\ORM\Query\Expr\Join;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Treviz\CommunityBundle\Entity\CommunityMembership;
use Treviz\CommunityBundle\Entity\Enums\CommunityPermissions;
use Treviz\CoreBundle\Events\PostEvent;
use Treviz\CoreBundle\Entity\Post;
use Treviz\CoreBundle\Form\PostType;
use Treviz\ProjectBundle\Entity\Enums\ProjectPermissions;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Swagger\Annotations as SWG;
use Treviz\ProjectBundle\Entity\ProjectMembership;

/**
 * Class PostController
 * @package Treviz\CoreBundle\Controller
 *
 * @Security("has_role('ROLE_USER')")
 */
class PostController extends FOSRestController
{

    /**
     * Fetches the posts of a project, a community, according to query parameters
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns an array of posts matching the query",
     * )
     *
     * @SWG\Tag(name="posts")
     *
     * @QueryParam(name="community", description="Hash of a community", nullable=true)
     * @QueryParam(name="project", description="Hash of a project", nullable=true)
     * @QueryParam(name="task", description="Hash of a task", nullable=true)
     * @QueryParam(name="document", description="Hash of a document", nullable=true)
     * @QueryParam(name="job", description="Hash of a job", nullable=true)
     * @QueryParam(name="offset", description="Offset", nullable=true)
     * @QueryParam(name="limit", description="Max number of results", nullable=true)
     *
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     */
    public function getPostsAction(ParamFetcher $paramFetcher)
    {
        $s_project = $paramFetcher->get("project");
        $s_community = $paramFetcher->get("community");
        $s_task = $paramFetcher->get("task");
        $s_document = $paramFetcher->get("document");
        $s_job = $paramFetcher->get("job");
        $s_offset = $paramFetcher->get("offset");
        $s_limit = $paramFetcher->get("limit");

        $qb = $this->getDoctrine()->getRepository("TrevizCoreBundle:Post")->createQueryBuilder("post");

        /*
         * First, apply search filters
         * If a search option is given, make the correct join, and specify the option.
         * Otherwise, make a left join that will be used later for access management.
         *
         * Then apply access management filters:
         *  - only show posts of a project/community if it is either public or the current user is member of the project.
         *  - only show posts of a task is the current user is either the supervisor or assignee, or wants to see its posts specifically.
         *  - only show posts of a job if the current user is either the contact for that job, or holds it, or wants to see its posts specifically.
         */
        if ($s_community) {
            $qb->join("post.community", "community");
            $qb->andWhere("community.hash =  :community_hash");
            $qb->setParameter("community_hash", $s_community);
        } else {
            $qb->leftJoin("post.community", "community", Join::WITH, $qb->expr()->orX(
                $qb->expr()->eq('community.public', 'true'),
                $qb->expr()->exists('SELECT cm
                                     FROM TrevizCommunityBundle:CommunityMembership cm
                                     WHERE cm.community = community
                                     AND cm.user = :user
                                    ')
            ));
        }

        if ($s_project) {
            $qb->join("post.project", "project");
            $qb->andWhere("project.hash =  :project_hash");
            $qb->setParameter("project_hash", $s_project);
        } else {
            $qb->leftJoin("post.project", "project", Join::WITH, $qb->expr()->orX(
                $qb->expr()->eq('project.public', 'true'),
                $qb->expr()->exists('SELECT pm
                                     FROM TrevizProjectBundle:ProjectMembership pm
                                     WHERE pm.project = project
                                     AND pm.user = :user
                                    ')
            ));
        }

        if ($s_task) {
            $qb->join("post.task", "task");
            $qb->andWhere("task.hash =  :task_hash");
            $qb->andWhere("task.assignee = :user OR task.supervisor = :user");
            $qb->setParameter("task_hash", $s_task);
        } else {
            $qb->leftJoin("post.task", "task", Join::WITH, $qb->expr()->orX(
                $qb->expr()->eq('task.assignee', ':user'),
                $qb->expr()->eq('task.supervisor', ':user')
            ));
        }

        if($s_job) {
            $qb->join('post.job', 'job');
            $qb->andWhere('job.hash = :job_hash');
            $qb->setParameter('job_hash', $s_job);
        } else {
            $qb->leftJoin("post.job", "job", Join::WITH, $qb->expr()->orX(
                $qb->expr()->eq('job.holder', ':user'),
                $qb->expr()->eq('job.contact', ':user')
            ));
        }

        if($s_document) {
            $qb->join('post.document', 'document');
            $qb->andWhere('document.hash = :document_hash');
            $qb->setParameter('document_hash', $s_document);
        } else {
            $qb->leftJoin("post.document", "document", Join::WITH, $qb->expr()->eq("document.owner", ':user'));
        }

        if ($s_offset) {
            $qb->setFirstResult($s_offset);
        }

        if ($s_limit) {
            $qb->setMaxResults($s_limit);
        } else {
            $qb->setMaxResults(20);
        }

        $qb->setParameter('user', $this->getUser());

        /*
         * Order by publication date and send the results.
         */
        $s_project = $paramFetcher->get("project");
        $s_community = $paramFetcher->get("community");
        $s_task = $paramFetcher->get("task");
        $s_document = $paramFetcher->get("document");
        $s_job = $paramFetcher->get("job");

        /*
         * Specify the context for serialization.
         * If no search filter was given, adding the "GeneralPosts" group will allow for project, community... serialization.
         */
        $context = new Context();
        $context->enableMaxDepth();
        if (!$s_project && !$s_community && !$s_task && !$s_document && !$s_job) {
            $context->setGroups(['Default', 'GeneralPosts']);
        } else {
            $context->setGroups(['Default']);
        }

        $qb->orderBy("post.publicationDate", "DESC");
        $view = $this->view($qb->getQuery()->getResult(), 200);
        $view->setContext($context);

        return $view;

    }

    /**
     * Creates a new post
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a post was successfully created",
     * )
     *
     * @SWG\Tag(name="posts")
     *
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     * @internal param ParamFetcher $paramFetcher
     */
    public function postPostAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->submit($request->request->all());
        $dispatcher = $this->get('treviz_core.event_dispatcher.dispatcher');

        if ($form->isValid()) {

            $user = $this->getUser();

            $post->setAuthor($user);

            $postHash = hash("sha256", random_bytes(256));
            while ($this->getDoctrine()->getRepository("TrevizCoreBundle:Post")->findOneBy(array("hash" => $postHash))) {
                $postHash = hash("sha256", random_bytes(256));
            }

            $post->setHash($postHash);

            $em = $this->getDoctrine()->getManager();

            if (($project = $post->getProject()) && ($post->getCommunity() == null)) {

                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $project,
                        "user" => $this->getUser()
                    ));

                if($membership !== null) {

                    $em->persist($post);
                    $em->flush();

                    $event = new PostEvent($post);
                    $dispatcher->dispatch(PostEvent::NAME, $event);

                    $view = $this->view($post, 200);
                    return $view;

                }

                return $this->view("You do not have the rights to do this", 403);

            } elseif (($community = $post->getCommunity()) && ($post->getProject() == null)) {

                $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                    ->findOneBy(array(
                        "community" => $community,
                        "user" => $this->getUser()
                    ));

                if($membership !== null) {
                    $em->persist($post);
                    $em->flush();

                    $event = new PostEvent($post);
                    $dispatcher->dispatch(PostEvent::NAME, $event);

                    $view = $this->view($post, 200);
                    return $view;

                }

                return $this->view("You do not have the rights to do this", 403);

            }

            $em->persist($post);
            $em->flush();

            $view = $this->view($post, 200);
            return $view;

        }

        return $this->view($form, 422);

    }

    /**
     * Updates an existing post.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when a post was successfully updated",
     * )
     *
     * @SWG\Tag(name="posts")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function putPostAction(Request $request, $hash)
    {
        $post = $this->getDoctrine()->getRepository("TrevizCoreBundle:Post")->findOneBy(array("hash" => $hash));
        $form = $this->createForm(PostType::class, $post);

        $project = $post->getProject();
        $community = $post->getCommunity();

        $form->submit($request->request->all());

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            /*
             * If the user is the author of the post, update it.
             */
            if ($this->getUser() == $post->getAuthor()) {

                $em->flush();

                $view = $this->view($post, 200);
                return $view;

            }

            /*
             * Otherwise, check if the post is linked to a project or a community the user can administrate.
             */
            if ($project != null) {

                /** @var ProjectMembership $membership */
                $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                    ->findOneBy(array(
                        "project" => $project,
                        "user" => $this->getUser()
                    ));

                if($membership !== null
                    && (in_array(ProjectPermissions::MANAGE_POST, $membership->getRole()->getPermissions())
                    || $post->getAuthor() == $this->getUser())) {
                    $em->flush();

                    $view = $this->view($post, 200);
                    return $view;

                }

            }

            if ($community != null) {

                /** @var CommunityMembership $membership */
                $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                    ->findOneBy(array(
                        "community" => $community,
                        "user" => $this->getUser()
                    ));

                if($membership !== null
                    && (in_array(CommunityPermissions::MANAGE_POST, $membership->getRole()->getPermissions())
                    || $post->getAuthor() == $this->getUser())) {
                    $em->flush();

                    $view = $this->view($post, 200);
                    return $view;

                }

            }

            return $this->view("You do not have the rights to do this", 403);

        }

        $view = $this->view($form, 422);
        return $view;

    }

    /**
     * Deletes a post.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when a post was successfully deleted",
     * )
     *
     * @SWG\Tag(name="posts")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param $hash
     * @return \FOS\RestBundle\View\View
     */
    public function deletePostAction($hash)
    {
        $post = $this->getDoctrine()->getRepository("TrevizCoreBundle:Post")->findOneBy(array("hash" => $hash));

        $project = $post->getProject();
        $community = $post->getCommunity();

        $em = $this->getDoctrine()->getManager();
        $em->remove($post);

        /*
         * If the user is the author of the post, update it.
         */
        if ($this->getUser() == $post->getAuthor()) {

            $em->flush();

            $view = $this->view(null, 204);
            return $view;

        }

        /*
         * Otherwise, check if the post is linked to a project or a community the user can administrate.
         */
        if ($project != null) {

            /** @var ProjectMembership $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizProjectBundle:ProjectMembership")
                ->findOneBy(array(
                    "project" => $project,
                    "user" => $this->getUser()
                ));

            if($membership !== null
                && (in_array(ProjectPermissions::MANAGE_POST, $membership->getRole()->getPermissions())
                    || $post->getAuthor() == $this->getUser())) {
                $em->flush();

                $view = $this->view(null, 204);
                return $view;

            }

        }

        if ($community != null) {

            /** @var CommunityMembership $membership */
            $membership = $this->getDoctrine()->getRepository("TrevizCommunityBundle:CommunityMembership")
                ->findOneBy(array(
                    "community" => $community,
                    "user" => $this->getUser()
                ));

            if($membership !== null
                && (in_array(CommunityPermissions::MANAGE_POST, $membership->getRole()->getPermissions())
                    || $post->getAuthor() == $this->getUser())) {
                $em->flush();

                $view = $this->view(null, 204);
                return $view;

            }

        }

        return $this->view("You do not have the rights to do this", 403);

    }

}